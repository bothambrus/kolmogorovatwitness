#!/usr/bin/env python
#################################################################################
#   KolmogorovAtWitness                                                         #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import sys


def main(argv):
    """
    This is the plotting script of KolmogorovAtWitness.
    It plots the results according to given criteria.


    Execution:
    plot [OPTIONS]

    OPTIONS:
        -h  --help:
                        Print help message.

        -i  --input JSONFILE:
                        Input path to json file.
                        If not provided KolmogorovAtWitnessInputFile.json is used.

        -t  --task TASK:
                        Name of the task to execute:
                        spectrum:          Interactively select a point to plot spectrum.
                        borghi:            Borghi diagram.
                        premixed_peters:   Premixed regime diagram of Peters.
                        pitsch:            Premixed LES regime diagram of Pitsch.

        -q  --quiet:
                        Plot in backend.
    """
    #
    # Import
    #
    import getopt
    import os

    import kolmogorovatwitness.context
    from kolmogorovatwitness.configuration import LocalConfig
    from kolmogorovatwitness.plotting import (
        plotBorghiDiagram,
        plotFilterRegimeDiagram,
        setPltBackend,
        plotSpectrum,
    )

    #
    # Process options and arguments
    #
    short_arguments = "hi:t:q"
    long_arguments = [
        "help",
        "input=",
        "task=",
        "quiet",
    ]
    try:
        opts, args = getopt.gnu_getopt(argv, short_arguments, long_arguments)
    except getopt.GetoptError:
        help(main)

    isHelp = False
    json_path = "KolmogorovAtWitnessInputFile.json"
    task = None
    quiet = False

    for opt, arg in opts:
        if opt in ["-h", "--help"]:
            isHelp = True
        elif opt in ["-i", "--input"]:
            json_path = arg
        elif opt in ["-t", "--task"]:
            task = arg.lower()
        elif opt in ["-q", "--quiet"]:
            quiet = True

    #
    # Print help
    #
    if isHelp or (task is None):
        help(main)
        sys.exit()

    #
    # Read configure
    #
    lc = LocalConfig(json_path_loc=json_path)
    lc.getData(noTables=True)
    if quiet:
        plt = setPltBackend(backend="ps")
    else:
        plt = setPltBackend()

    #
    # Execute task
    #
    if task == "spectrum":
        #
        # Plot Spectrum
        #
        plotSpectrum(lc)

    elif task == "borghi":
        #
        # Plot Borghi Diagram
        #
        plotBorghiDiagram(lc, labelStyle="Borghi")

    elif task == "premixed_peters":
        #
        # Plot premixed regime diagram of Peters
        #
        plotBorghiDiagram(lc, labelStyle="Peters")

    elif task == "pitsch":
        #
        # Plot LES regime diagram of Pitsch
        #
        plotFilterRegimeDiagram(lc)


if __name__ == "__main__":
    main(sys.argv[1:])
