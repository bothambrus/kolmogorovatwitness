#!/usr/bin/env python
#################################################################################
#   KolmogorovAtWitness                                                         #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import sys


def main(argv):
    """
    This script extracts the witness point data from the Alya output to intermediate files.


    Execution:
    extractWitness [OPTIONS]

    OPTIONS:
        -h  --help:
                        Print help message.

        -i  --input JSONFILE:
                        Input path to json file.
                        If not provided KolmogorovAtWitnessInputFile.json is used.

        -o  --overwrite:
                        Overwrite existing files.
    """
    #
    # Import
    #
    import getopt
    import os

    import kolmogorovatwitness.context
    from kolmogorovatwitness.configuration import LocalConfig
    from kolmogorovatwitness.process_data import extractWitnessPointData

    #
    # Process options and arguments
    #
    short_arguments = "hi:o"
    long_arguments = [
        "help",
        "input=",
        "overwrite",
    ]
    try:
        opts, args = getopt.gnu_getopt(argv, short_arguments, long_arguments)
    except getopt.GetoptError:
        help(main)

    isHelp = False
    json_path = "KolmogorovAtWitnessInputFile.json"
    overwrite = False

    for opt, arg in opts:
        if opt in ["-h", "--help"]:
            isHelp = True
        elif opt in ["-i", "--input"]:
            json_path = arg
        elif opt in ["-o", "--overwrite"]:
            overwrite = True

    #
    # Print help
    #
    if isHelp:
        help(main)
        sys.exit()

    #
    # Read configure
    #
    lc = LocalConfig(json_path_loc=json_path)
    lc.getData(noTables=False)

    #
    # Extract selected witness point data
    #
    extractWitnessPointData(lc, overwrite=overwrite)


if __name__ == "__main__":
    main(sys.argv[1:])
