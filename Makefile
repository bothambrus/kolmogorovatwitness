init:
	pip3 install -r alyatab/requirements.txt

reinit:
	pip3 install --upgrade --force-reinstall  -r requirements.txt

checkstyle:
	black --check kolmogorovatwitness  
	black --check tests
	black --check initialize 
	black --check configure 
	black --check extractWitness 
	black --check processWitness 
	black --check mergeWitness 
	black --check deleteIntermediateData 
	black --check plot 
	isort --profile black --diff kolmogorovatwitness
	isort --profile black --diff tests
	isort --profile black --diff initialize
	isort --profile black --diff configure
	isort --profile black --diff extractWitness
	isort --profile black --diff processWitness
	isort --profile black --diff mergeWitness
	isort --profile black --diff deleteIntermediateData
	isort --profile black --diff plot

style:
	isort --profile black kolmogorovatwitness
	black kolmogorovatwitness 
	black tests
	black initialize 
	black configure 
	black extractWitness
	black processWitness
	black mergeWitness
	black deleteIntermediateData
	black plot


test:
	rm -rf tests/tmp_testing/
	pytest --junitxml=junit_report.xml --cov=kolmogorovatwitness --cov-report=html tests
	coverage xml
	mkdir -p dist 
	rm -rf dist/htmlcov
	mv coverage.xml dist/
	mv htmlcov dist/
	echo open dist/htmlcov/index.html to review code coverage
	python alyatab/.cicd_scripts/test_coverage.py 90 dist/coverage.xml



.PHONY: init test

