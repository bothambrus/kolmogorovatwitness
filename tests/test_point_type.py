#################################################################################
#   KolmogorovAtWitness                                                         #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import os
import glob
import json
import shutil
import numpy as np

from .context import kolmogorovatwitness, getTestPath, getTestTmpPath
from kolmogorovatwitness.configuration import GlobalConfig, LocalConfig
from kolmogorovatwitness.point_type import PointType

test_path = getTestPath()
tmp_test_path = getTestTmpPath()


def test_create_PointType():
    """
    Check the creation of a PointType object.
    """
    #
    # Paths
    #
    test_case = os.path.join(tmp_test_path, "point_type_create")
    if not os.path.isdir(test_case):
        os.makedirs(test_case)
    json_path_glob = os.path.join(test_case, "globalConfig.json")
    json_path_loc = os.path.join(test_case, "KolmogorovAtWitnessInputFile.json")

    #
    # Set case data
    #
    settings = {
        "CasePath": test_case,
        "CaseName": "case",
        "WitnessDefinitionPath": os.path.join(
            test_path, "data", "witnessDefinitionExample.dat"
        ),
    }
    with open(json_path_loc, "w") as f:
        json.dump(settings, f)

    #
    # Get configuration
    #
    lc = LocalConfig(json_path_glob=json_path_glob, json_path_loc=json_path_loc)
    lc.getData()

    #
    # Create point
    #
    pointIndexes = lc.getWitnessIndces()

    #
    # Creat PointType
    #
    ii = 11
    point = PointType(index=ii, coords=lc.witnessDefinition[ii, :])

    assert np.abs(point.index - 11) < 1e-3
    assert np.all(np.abs(point.coords - np.array([0.0, 0.05, 0.045])) < 1e-3)

    #
    # Create empty point type
    #
    point2 = PointType()
    assert point2.index is None
    assert np.all(np.isnan(point2.coords))


def test_PointType_extractSinglePointFiles():
    """
    Extract relevant files associated with a single point.
    """
    #
    # Paths
    #
    test_case = os.path.join(tmp_test_path, "point_type_extrac_files")
    if not os.path.isdir(test_case):
        os.makedirs(test_case)
    json_path_glob = os.path.join(test_case, "globalConfig.json")
    json_path_loc = os.path.join(test_case, "KolmogorovAtWitnessInputFile.json")

    #
    # Copy data
    #
    for fn in ["cam.chm.wit", "cam.nsi.wit", "cam.tem.wit", "witness.dat"]:
        shutil.copy(
            os.path.join(test_path, "data", "camb_example", fn),
            os.path.join(test_case, fn),
        )

    #
    # Set case data
    #
    data = {
        "CasePath": test_case,
        "CaseName": "cam",
        "Modules": ["nsi", "tem", "chm"],
        "PropertyDensityMethod": "EoS_H_from_SCONC_to_NASA_poly",
        "PropertyLookupEnthalpyScalingTable": os.path.join(
            test_path, "data", "camb_example", "tab_turbulent_scaling_H.dat"
        ),
        "PropertyLookupTable": os.path.join(
            test_path, "data", "camb_example", "tab_turbulentProp_table.dat"
        ),
        "PropertyMethod": "tabulated",
        "Tasks": ["kolmogorov", "integral", "chemical_scales"],
    }
    with open(json_path_loc, "w") as f:
        json.dump(data, f)

    if ("BSC_MACHINE" in os.environ) and ("mn4" in os.getenv("BSC_MACHINE")):
        globdata = {"AlyaPath": "/gpfs/projects/bsc21/bsc21304/git/master"}
    else:
        globdata = {"AlyaPath": "/usr/local/bin/"}
    with open(json_path_glob, "w") as f:
        json.dump(globdata, f)

    lc = LocalConfig(json_path_glob=json_path_glob, json_path_loc=json_path_loc)
    lc.getData()

    #
    # Create point
    #
    pointIndexes = lc.getWitnessIndces()

    #
    # Check extraction with enthalpy from SCONC
    #
    ii = 11
    point = PointType(index=ii)
    point.extractSinglePointFiles(lc)
    for fn in [
        "tmp_cam.chm.wit_entity12_varSCO01",
        "tmp_cam.chm.wit_entity12_varSCO02",
        "tmp_cam.chm.wit_entity12_varSCO03",
        "tmp_cam.chm.wit_entity12_varSCO04",
        "tmp_cam.nsi.wit_entity12_varS11",
        "tmp_cam.nsi.wit_entity12_varS12",
        "tmp_cam.nsi.wit_entity12_varS13",
        "tmp_cam.nsi.wit_entity12_varS22",
        "tmp_cam.nsi.wit_entity12_varS23",
        "tmp_cam.nsi.wit_entity12_varS33",
        "tmp_cam.nsi.wit_entity12_varTURBU",
        "tmp_cam.nsi.wit_entity12_varVELOX",
        "tmp_cam.nsi.wit_entity12_varVELOY",
        "tmp_cam.nsi.wit_entity12_varVELOZ",
    ]:
        assert os.path.isfile(os.path.join(test_case, fn))

    #
    # Remove files
    #
    for fn in glob.glob(os.path.join(test_case, "tmp_*")):
        os.remove(fn)

    #
    # CALL AGAIN WITH DIFFERENT DENSITY METHOD
    #
    data["PropertyDensityMethod"] = "EoS_T_from_temper"
    with open(json_path_loc, "w") as f:
        json.dump(data, f)
    lc = LocalConfig(json_path_glob=json_path_glob, json_path_loc=json_path_loc)
    lc.getData()

    point.extractSinglePointFiles(lc)
    for fn in [
        "tmp_cam.chm.wit_entity12_varSCO01",
        "tmp_cam.chm.wit_entity12_varSCO02",
        "tmp_cam.chm.wit_entity12_varSCO03",
        "tmp_cam.chm.wit_entity12_varSCO04",
        "tmp_cam.nsi.wit_entity12_varS11",
        "tmp_cam.nsi.wit_entity12_varS12",
        "tmp_cam.nsi.wit_entity12_varS13",
        "tmp_cam.nsi.wit_entity12_varS22",
        "tmp_cam.nsi.wit_entity12_varS23",
        "tmp_cam.nsi.wit_entity12_varS33",
        "tmp_cam.nsi.wit_entity12_varTURBU",
        "tmp_cam.nsi.wit_entity12_varVELOX",
        "tmp_cam.nsi.wit_entity12_varVELOY",
        "tmp_cam.nsi.wit_entity12_varVELOZ",
        "tmp_cam.tem.wit_entity12_varTEMPE",
    ]:
        assert os.path.isfile(os.path.join(test_case, fn))

    #
    # Remove files
    #
    for fn in glob.glob(os.path.join(test_case, "tmp_*")):
        os.remove(fn)

    #
    # CALL AGAIN WITH ANOTHER DIFFERENT DENSITY METHOD
    #
    data["PropertyDensityMethod"] = "EoS_H_from_temper_to_NASA_poly"
    with open(json_path_loc, "w") as f:
        json.dump(data, f)
    lc = LocalConfig(json_path_glob=json_path_glob, json_path_loc=json_path_loc)
    lc.getData()

    point.extractSinglePointFiles(lc)
    for fn in [
        "tmp_cam.chm.wit_entity12_varSCO01",
        "tmp_cam.chm.wit_entity12_varSCO02",
        "tmp_cam.chm.wit_entity12_varSCO03",
        "tmp_cam.chm.wit_entity12_varSCO04",
        "tmp_cam.nsi.wit_entity12_varS11",
        "tmp_cam.nsi.wit_entity12_varS12",
        "tmp_cam.nsi.wit_entity12_varS13",
        "tmp_cam.nsi.wit_entity12_varS22",
        "tmp_cam.nsi.wit_entity12_varS23",
        "tmp_cam.nsi.wit_entity12_varS33",
        "tmp_cam.nsi.wit_entity12_varTURBU",
        "tmp_cam.nsi.wit_entity12_varVELOX",
        "tmp_cam.nsi.wit_entity12_varVELOY",
        "tmp_cam.nsi.wit_entity12_varVELOZ",
        "tmp_cam.tem.wit_entity12_varENTHA",
    ]:
        assert os.path.isfile(os.path.join(test_case, fn))


def test_PointType_processSinglePointFiles_constantProperty():
    """
    Extract and PROCESS relevant files associated with a single point.
    """
    #
    # Paths
    #
    test_case = os.path.join(tmp_test_path, "point_type_process_constantProperty")
    if not os.path.isdir(test_case):
        os.makedirs(test_case)
    json_path_glob = os.path.join(test_case, "globalConfig.json")
    json_path_loc = os.path.join(test_case, "KolmogorovAtWitnessInputFile.json")

    #
    # Copy data
    #
    for fn in [
        "mixedEq_non_prem_box_VarVar_vectorized.nsi.wit",
        "witness.dat",
    ]:
        shutil.copy(
            os.path.join(test_path, "data", "simple_case", fn),
            os.path.join(test_case, fn),
        )

    #
    # Set case data
    #
    data = {
        "CasePath": test_case,
        "CaseName": "mixedEq_non_prem_box_VarVar_vectorized",
        "Modules": ["nsi"],
        "PropertyConstPressure": 101325.0,
        "PropertyMethod": "constant",
        "PropertyConstKinematicViscosity": 1e-5,
        "PropertyConstDensity": 1.1,
        "Tasks": ["kolmogorov", "spectrum", "favre_error"],
        "spectrumDoLombscargle": True,
        "spectrumProperties": {
            "raw": {
                "nsi": [
                    "VELOX",
                    "PRESS",
                ]
            }
        },
    }
    with open(json_path_loc, "w") as f:
        json.dump(data, f)

    if ("BSC_MACHINE" in os.environ) and ("mn4" in os.getenv("BSC_MACHINE")):
        globdata = {"AlyaPath": "/gpfs/projects/bsc21/bsc21304/git/master"}
    else:
        globdata = {"AlyaPath": "/usr/local/bin/"}
    with open(json_path_glob, "w") as f:
        json.dump(globdata, f)

    lc = LocalConfig(json_path_glob=json_path_glob, json_path_loc=json_path_loc)
    lc.getData()

    #
    # Create point
    #
    pointIndexes = lc.getWitnessIndces()

    #
    # Check extraction with enthalpy from SCONC
    #
    ii = 1
    point = PointType(index=ii)
    point.extractSinglePointFiles(lc)

    #
    # Process data
    #
    point.processSinglePointFiles(lc)

    assert np.abs(point.processedData["rho"][0] - 1.1) < 1e-5
    assert np.abs(point.processedData["nu"][0] - 1.0e-5) < 1e-9
    assert point.spectrumData["nequi"] == 31
    assert np.abs(point.spectrumData["equidist_dt"] - 9.67741935483871e-05) < 1e-9
    assert len(point.spectrumData["frequencies"]) == 32
    assert np.abs(point.spectrumData["frequencies"][0] - 0.0) < 1e-9
    assert np.abs(point.spectrumData["frequencies"][1] - 322.91666667) < 1e-5
    assert np.abs(point.spectrumData["nsi"]["VELOX"][0] - 0.007014433860942213) < 1e-7
    assert np.abs(point.spectrumData["nsi"]["PRESS"][0] - 0.14636481229364445) < 1e-5
    assert np.abs(point.spectrumData["nsi"]["VELOX"][1] - 0.0017307518561012116) < 1e-7
    assert np.abs(point.spectrumData["nsi"]["PRESS"][1] - 0.5620527325060585) < 1e-9
    assert len(point.spectrumData["welch_frequencies"]) == 6
    assert len(point.spectrumData["lombscargle_frequencies"]) == 15
    assert (
        np.abs(point.spectrumData["nsi"]["welch_VELOX"][1] - 6.2316936849327674e-06)
        < 1e-7
    )
    assert (
        np.abs(point.spectrumData["nsi"]["lombscargle_VELOX"][1] - 12.58570151947925)
        < 1e-4
    )
    assert (
        np.abs(point.averagedData["Re"]["spectrum"]["nsi_VELOX"] - 1.5472601399) < 1e-9
    )
    assert (
        np.abs(
            point.averagedData["Re"]["spectrum"]["nsi_VELOX_square"]
            - 3.7388629321960307
        )
        < 1e-9
    )


def test_PointType_processSinglePointFiles_EoS_T_from_temper():
    """
    Extract and PROCESS relevant files associated with a single point.
    """
    #
    # Paths
    #
    test_case = os.path.join(tmp_test_path, "point_type_process_EoS_T_from_temper")
    if not os.path.isdir(test_case):
        os.makedirs(test_case)
    json_path_glob = os.path.join(test_case, "globalConfig.json")
    json_path_loc = os.path.join(test_case, "KolmogorovAtWitnessInputFile.json")

    #
    # Copy data
    #
    for fn in [
        "mixedEq_non_prem_box_VarVar_vectorized.chm.wit",
        "mixedEq_non_prem_box_VarVar_vectorized.nsi.wit",
        "mixedEq_non_prem_box_VarVar_vectorized.tem.wit",
        "witness.dat",
    ]:
        shutil.copy(
            os.path.join(test_path, "data", "simple_case", fn),
            os.path.join(test_case, fn),
        )

    #
    # Set case data
    #
    data = {
        "CasePath": test_case,
        "CaseName": "mixedEq_non_prem_box_VarVar_vectorized",
        "Modules": ["nsi", "tem", "chm"],
        "PropertyConstPressure": 101325.0,
        "PropertyDensityMethod": "EoS_T_from_temper",
        "PropertyLookupTable": os.path.join(
            test_path, "data", "simple_case", "cam_diff_turbulent_table.dat_props"
        ),
        "PropertyMethod": "tabulated",
        "Tasks": ["kolmogorov", "integral", "chemical_scales"],
    }
    with open(json_path_loc, "w") as f:
        json.dump(data, f)

    if ("BSC_MACHINE" in os.environ) and ("mn4" in os.getenv("BSC_MACHINE")):
        globdata = {"AlyaPath": "/gpfs/projects/bsc21/bsc21304/git/master"}
    else:
        globdata = {"AlyaPath": "/usr/local/bin/"}
    with open(json_path_glob, "w") as f:
        json.dump(globdata, f)

    lc = LocalConfig(json_path_glob=json_path_glob, json_path_loc=json_path_loc)
    lc.getData()

    #
    # Create point
    #
    pointIndexes = lc.getWitnessIndces()

    #
    # Check extraction with enthalpy from SCONC
    #
    ii = 1
    point = PointType(index=ii)
    point.extractSinglePointFiles(lc)

    for fn in [
        "tmp_mixedEq_non_prem_box_VarVar_vectorized.chm.wit_entity2_varSCO01",
        "tmp_mixedEq_non_prem_box_VarVar_vectorized.chm.wit_entity2_varSCO02",
        "tmp_mixedEq_non_prem_box_VarVar_vectorized.chm.wit_entity2_varSCO03",
        "tmp_mixedEq_non_prem_box_VarVar_vectorized.chm.wit_entity2_varSCO04",
        "tmp_mixedEq_non_prem_box_VarVar_vectorized.nsi.wit_entity2_varS11",
        "tmp_mixedEq_non_prem_box_VarVar_vectorized.nsi.wit_entity2_varS12",
        "tmp_mixedEq_non_prem_box_VarVar_vectorized.nsi.wit_entity2_varS13",
        "tmp_mixedEq_non_prem_box_VarVar_vectorized.nsi.wit_entity2_varS22",
        "tmp_mixedEq_non_prem_box_VarVar_vectorized.nsi.wit_entity2_varS23",
        "tmp_mixedEq_non_prem_box_VarVar_vectorized.nsi.wit_entity2_varS33",
        "tmp_mixedEq_non_prem_box_VarVar_vectorized.nsi.wit_entity2_varTURBU",
        "tmp_mixedEq_non_prem_box_VarVar_vectorized.nsi.wit_entity2_varVELOX",
        "tmp_mixedEq_non_prem_box_VarVar_vectorized.nsi.wit_entity2_varVELOY",
        "tmp_mixedEq_non_prem_box_VarVar_vectorized.nsi.wit_entity2_varVELOZ",
        "tmp_mixedEq_non_prem_box_VarVar_vectorized.tem.wit_entity2_varTEMPE",
    ]:
        assert os.path.isfile(os.path.join(test_case, fn))

    #
    # Process data
    #
    point.processSinglePointFiles(lc)

    assert np.abs(np.max(point.processedData["rho"]) - 0.36241821) < 1e-5
    assert np.abs(np.min(point.processedData["rho"]) - 0.25275453) < 1e-5
    assert np.abs(np.min(point.processedData["nu"]) - 0.00010010241221341828) < 1e-9


def test_PointType_processSinglePointFiles_from_chemic():
    """
    Extract and PROCESS relevant files associated with a single point.
    """
    #
    # Paths
    #
    test_case = os.path.join(tmp_test_path, "point_type_process_from_chemic")
    if not os.path.isdir(test_case):
        os.makedirs(test_case)
    json_path_glob = os.path.join(test_case, "globalConfig.json")
    json_path_loc = os.path.join(test_case, "KolmogorovAtWitnessInputFile.json")

    #
    # Copy data
    #
    for fn in [
        "mixedEq_non_prem_box_VarVar_vectorized.chm.wit",
        "mixedEq_non_prem_box_VarVar_vectorized.nsi.wit",
        "mixedEq_non_prem_box_VarVar_vectorized.tem.wit",
        "witness.dat",
    ]:
        shutil.copy(
            os.path.join(test_path, "data", "simple_case", fn),
            os.path.join(test_case, fn),
        )

    #
    # Set case data
    #
    data = {
        "CasePath": test_case,
        "CaseName": "mixedEq_non_prem_box_VarVar_vectorized",
        "Modules": ["nsi", "tem", "chm"],
        "PropertyDensityMethod": "from_chemic",
        "PropertyLookupTable": os.path.join(
            test_path, "data", "simple_case", "cam_diff_turbulent_table.dat_props"
        ),
        "PropertyMethod": "tabulated",
        "Tasks": ["kolmogorov", "integral", "chemical_scales"],
        "ChemicalScalesList": ["delta_diff", "tau_c"],
        "ChemicalScalesMethod": "constant",
        "ChemicalScalesConst_delta_diff": 0.1e-3,
        "ChemicalScalesConst_tau_c": 0.5e-3,
    }
    with open(json_path_loc, "w") as f:
        json.dump(data, f)

    if ("BSC_MACHINE" in os.environ) and ("mn4" in os.getenv("BSC_MACHINE")):
        globdata = {"AlyaPath": "/gpfs/projects/bsc21/bsc21304/git/master"}
    else:
        globdata = {"AlyaPath": "/usr/local/bin/"}
    with open(json_path_glob, "w") as f:
        json.dump(globdata, f)

    lc = LocalConfig(json_path_glob=json_path_glob, json_path_loc=json_path_loc)
    lc.getData()

    #
    # Create point
    #
    pointIndexes = lc.getWitnessIndces()

    #
    # Check extraction with enthalpy from SCONC
    #
    ii = 1
    point = PointType(index=ii)
    point.extractSinglePointFiles(lc)

    for fn in [
        "tmp_mixedEq_non_prem_box_VarVar_vectorized.chm.wit_entity2_varDENSI",
        "tmp_mixedEq_non_prem_box_VarVar_vectorized.chm.wit_entity2_varSCO01",
        "tmp_mixedEq_non_prem_box_VarVar_vectorized.chm.wit_entity2_varSCO02",
        "tmp_mixedEq_non_prem_box_VarVar_vectorized.chm.wit_entity2_varSCO03",
        "tmp_mixedEq_non_prem_box_VarVar_vectorized.chm.wit_entity2_varSCO04",
        "tmp_mixedEq_non_prem_box_VarVar_vectorized.nsi.wit_entity2_varS11",
        "tmp_mixedEq_non_prem_box_VarVar_vectorized.nsi.wit_entity2_varS12",
        "tmp_mixedEq_non_prem_box_VarVar_vectorized.nsi.wit_entity2_varS13",
        "tmp_mixedEq_non_prem_box_VarVar_vectorized.nsi.wit_entity2_varS22",
        "tmp_mixedEq_non_prem_box_VarVar_vectorized.nsi.wit_entity2_varS23",
        "tmp_mixedEq_non_prem_box_VarVar_vectorized.nsi.wit_entity2_varS33",
        "tmp_mixedEq_non_prem_box_VarVar_vectorized.nsi.wit_entity2_varTURBU",
        "tmp_mixedEq_non_prem_box_VarVar_vectorized.nsi.wit_entity2_varVELOX",
        "tmp_mixedEq_non_prem_box_VarVar_vectorized.nsi.wit_entity2_varVELOY",
        "tmp_mixedEq_non_prem_box_VarVar_vectorized.nsi.wit_entity2_varVELOZ",
    ]:
        assert os.path.isfile(os.path.join(test_case, fn))

    #
    # Process data
    #
    point.processSinglePointFiles(lc)

    assert np.abs(np.max(point.processedData["rho"]) - 0.36275916) < 1e-5
    assert np.abs(np.min(point.processedData["rho"]) - 0.25285705) < 1e-5
    assert np.abs(np.min(point.processedData["nu"]) - 0.00010000832841880006) < 1e-9


def test_PointType_processSinglePointFiles_EoS_H_from_temper_to_NASA_poly():
    """
    Extract and PROCESS relevant files associated with a single point.
    """
    #
    # Paths
    #
    test_case = os.path.join(
        tmp_test_path, "point_type_process_EoS_H_from_temper_to_NASA_poly"
    )
    if not os.path.isdir(test_case):
        os.makedirs(test_case)
    json_path_glob = os.path.join(test_case, "globalConfig.json")
    json_path_loc = os.path.join(test_case, "KolmogorovAtWitnessInputFile.json")

    #
    # Copy data
    #
    for fn in [
        "mixedEq_non_prem_box_VarVar_vectorized.chm.wit",
        "mixedEq_non_prem_box_VarVar_vectorized.nsi.wit",
        "mixedEq_non_prem_box_VarVar_vectorized.tem.wit",
        "witness.dat",
    ]:
        shutil.copy(
            os.path.join(test_path, "data", "simple_case", fn),
            os.path.join(test_case, fn),
        )

    #
    # Set case data
    #
    data = {
        "CasePath": test_case,
        "CaseName": "mixedEq_non_prem_box_VarVar_vectorized",
        "Modules": ["nsi", "tem", "chm"],
        "PropertyConstPressure": 101325.0,
        "PropertyDensityMethod": "EoS_H_from_temper_to_NASA_poly",
        "PropertyLookupTable": os.path.join(
            test_path, "data", "simple_case", "cam_diff_turbulent_table.dat_props"
        ),
        "PropertyMethod": "tabulated",
        "Tasks": ["kolmogorov", "integral", "chemical_scales"],
        "ChemicalScalesList": ["S_L", "tau_c"],
        "ChemicalScalesMethod": "constant",
        "ChemicalScalesConst_S_L": 0.3,
        "ChemicalScalesConst_tau_c": 0.5e-3,
    }
    with open(json_path_loc, "w") as f:
        json.dump(data, f)

    if ("BSC_MACHINE" in os.environ) and ("mn4" in os.getenv("BSC_MACHINE")):
        globdata = {"AlyaPath": "/gpfs/projects/bsc21/bsc21304/git/master"}
    else:
        globdata = {"AlyaPath": "/usr/local/bin/"}
    with open(json_path_glob, "w") as f:
        json.dump(globdata, f)

    lc = LocalConfig(json_path_glob=json_path_glob, json_path_loc=json_path_loc)
    lc.getData()

    #
    # Create point
    #
    pointIndexes = lc.getWitnessIndces()

    #
    # Check extraction with enthalpy from SCONC
    #
    ii = 1
    point = PointType(index=ii)
    point.extractSinglePointFiles(lc)

    for fn in [
        "tmp_mixedEq_non_prem_box_VarVar_vectorized.chm.wit_entity2_varSCO01",
        "tmp_mixedEq_non_prem_box_VarVar_vectorized.chm.wit_entity2_varSCO02",
        "tmp_mixedEq_non_prem_box_VarVar_vectorized.chm.wit_entity2_varSCO03",
        "tmp_mixedEq_non_prem_box_VarVar_vectorized.chm.wit_entity2_varSCO04",
        "tmp_mixedEq_non_prem_box_VarVar_vectorized.nsi.wit_entity2_varS11",
        "tmp_mixedEq_non_prem_box_VarVar_vectorized.nsi.wit_entity2_varS12",
        "tmp_mixedEq_non_prem_box_VarVar_vectorized.nsi.wit_entity2_varS13",
        "tmp_mixedEq_non_prem_box_VarVar_vectorized.nsi.wit_entity2_varS22",
        "tmp_mixedEq_non_prem_box_VarVar_vectorized.nsi.wit_entity2_varS23",
        "tmp_mixedEq_non_prem_box_VarVar_vectorized.nsi.wit_entity2_varS33",
        "tmp_mixedEq_non_prem_box_VarVar_vectorized.nsi.wit_entity2_varTURBU",
        "tmp_mixedEq_non_prem_box_VarVar_vectorized.nsi.wit_entity2_varVELOX",
        "tmp_mixedEq_non_prem_box_VarVar_vectorized.nsi.wit_entity2_varVELOY",
        "tmp_mixedEq_non_prem_box_VarVar_vectorized.nsi.wit_entity2_varVELOZ",
        "tmp_mixedEq_non_prem_box_VarVar_vectorized.tem.wit_entity2_varENTHA",
    ]:
        assert os.path.isfile(os.path.join(test_case, fn))

    #
    # Process data
    #
    point.processSinglePointFiles(lc)

    assert os.path.isfile(os.path.join(test_case, "witness_data_entity2.json"))
    assert not os.path.isfile(
        os.path.join(test_case, "witness_temporal_data_entity2.json")
    )

    assert np.abs(np.max(point.processedData["rho"]) - 0.3627582341049219) < 1e-5
    assert np.abs(np.min(point.processedData["rho"]) - 0.2528633065208047) < 1e-5
    assert np.abs(np.min(point.processedData["nu"]) - 0.00010000832841880006) < 1e-9


def test_PointType_processSinglePointFiles_EoS_H_from_SCONC_to_NASA_poly():
    """
    Extract and PROCESS relevant files associated with a single point.
    """
    #
    # Paths
    #
    test_case = os.path.join(
        tmp_test_path, "point_type_process_EoS_H_from_SCONC_to_NASA_poly"
    )
    if not os.path.isdir(test_case):
        os.makedirs(test_case)
    json_path_glob = os.path.join(test_case, "globalConfig.json")
    json_path_loc = os.path.join(test_case, "KolmogorovAtWitnessInputFile.json")

    #
    # Copy data
    #
    for fn in [
        "cam.chm.wit",
        "cam.nsi.wit",
        "cam.tem.wit",
        "witness.dat",
    ]:
        shutil.copy(
            os.path.join(test_path, "data", "camb_example", fn),
            os.path.join(test_case, fn),
        )

    #
    # Set case data
    #
    data = {
        "CasePath": test_case,
        "CaseName": "cam",
        "Modules": ["nsi", "tem", "chm"],
        "TimeInitial": 0.0,
        "TimeFinal": 1000.0,
        "PropertyConstPressure": 101325.0,
        "PropertyDensityMethod": "EoS_H_from_SCONC_to_NASA_poly",
        "PropertyLookupEnthalpyScalingTable": os.path.join(
            test_path, "data", "camb_example", "tab_turbulent_scaling_H.dat"
        ),
        "PropertyLookupTable": os.path.join(
            test_path, "data", "camb_example", "tab_turbulentProp_table.dat"
        ),
        "PropertyMethod": "tabulated",
        "SaveTemporalData": True,
        "Tasks": ["kolmogorov", "integral", "chemical_scales", "cutoff", "favre_error"],
        "ChemicalScalesList": ["S_L", "delta_diff"],
        "ChemicalScalesMethod": "constant",
        "ChemicalScalesConst_S_L": 0.3,
        "ChemicalScalesConst_delta_diff": 0.1e-3,
        "FavreDensityLookupTable": os.path.join(
            test_path, "data", "camb_example", "tab_turbulentPost_table.dat"
        ),
        "FilterSizeMethod": "constant",
        "FilterSizeConstant": 0.5e-3,
    }
    with open(json_path_loc, "w") as f:
        json.dump(data, f)

    if ("BSC_MACHINE" in os.environ) and ("mn4" in os.getenv("BSC_MACHINE")):
        globdata = {"AlyaPath": "/gpfs/projects/bsc21/bsc21304/git/master"}
    else:
        globdata = {"AlyaPath": "/usr/local/bin/"}
    with open(json_path_glob, "w") as f:
        json.dump(globdata, f)

    lc = LocalConfig(json_path_glob=json_path_glob, json_path_loc=json_path_loc)
    lc.getData()

    #
    # Create point
    #
    pointIndexes = lc.getWitnessIndces()

    #
    # Check extraction with enthalpy from SCONC
    #
    ii = 1
    point = PointType(index=ii, filterSize=data["FilterSizeConstant"])
    point.extractSinglePointFiles(lc)

    for fn in [
        "tmp_cam.chm.wit_entity2_varSCO01",
        "tmp_cam.chm.wit_entity2_varSCO02",
        "tmp_cam.chm.wit_entity2_varSCO03",
        "tmp_cam.chm.wit_entity2_varSCO04",
        "tmp_cam.nsi.wit_entity2_varS11",
        "tmp_cam.nsi.wit_entity2_varS12",
        "tmp_cam.nsi.wit_entity2_varS13",
        "tmp_cam.nsi.wit_entity2_varS22",
        "tmp_cam.nsi.wit_entity2_varS23",
        "tmp_cam.nsi.wit_entity2_varS33",
        "tmp_cam.nsi.wit_entity2_varTURBU",
        "tmp_cam.nsi.wit_entity2_varVELOX",
        "tmp_cam.nsi.wit_entity2_varVELOY",
        "tmp_cam.nsi.wit_entity2_varVELOZ",
    ]:
        assert os.path.isfile(os.path.join(test_case, fn))

    #
    # Process data
    #
    point.processSinglePointFiles(lc)

    assert os.path.isfile(os.path.join(test_case, "witness_data_entity2.json"))
    assert os.path.isfile(os.path.join(test_case, "witness_temporal_data_entity2.json"))

    with open(os.path.join(test_case, "witness_data_entity2.json"), "r") as f:
        data = json.load(f)

    with open(os.path.join(test_case, "witness_temporal_data_entity2.json"), "r") as f:
        temp_data = json.load(f)

    for ii, dic in enumerate([point.processedData, temp_data["processed"]]):
        print("Assessing temporal data in prrocessed dic #{}".format(ii))
        assert np.abs(np.max(dic["rho"]) - 0.20385554452071153) < 1e-5
        assert np.abs(np.min(dic["rho"]) - 0.18315007611360218) < 1e-5
        assert np.abs(np.min(dic["nu"]) - 0.0002623735211377407) < 1e-9

    for ii, dic in enumerate([point.rawModuleData["nsi"], temp_data["nsi"]]):
        print("Assessing temporal data in nsi dic #{}".format(ii))
        assert np.abs(np.max(dic["VELOX"]) - -1.3386182) < 1e-5
        assert np.abs(np.min(dic["VELOX"]) - -3.834797) < 1e-5

    for ii, dic in enumerate([point.averagedData, data]):
        print("Assessing data in dic #{}".format(ii))

        #
        # Averaged material properties
        #
        assert np.abs(dic["Re"]["processed"]["nu"] - 0.0003207247373876762) < 1e-9
        assert np.abs(dic["Fv"]["processed"]["nu"] - 0.00032022675420187875) < 1e-9
        assert np.abs(dic["Re"]["processed"]["mu"] - 6.05564677587578e-05) < 1e-9
        assert np.abs(dic["Fv"]["processed"]["mu"] - 6.051725020510538e-05) < 1e-9
        assert np.abs(dic["Re"]["processed"]["rho"] - 0.18910496066978066) < 1e-6
        assert np.abs(dic["Fv"]["processed"]["rho"] - 0.18928416913603108) < 1e-6

        #
        # Mean of strain rate
        #
        assert (
            np.abs(dic["Re"]["dissipationRate"]["Sij"][0] - -1899.982553831364) < 1e-5
        )
        assert (
            np.abs(dic["Fv"]["dissipationRate"]["Sij"][0] - -1893.8587407833597) < 1e-5
        )
        assert (
            np.abs(dic["Re"]["dissipationRate"]["Sij"][1] - 19.650879478027797) < 1e-5
        )
        assert (
            np.abs(dic["Fv"]["dissipationRate"]["Sij"][1] - 19.674891291647377) < 1e-5
        )
        assert np.abs(dic["Re"]["dissipationRate"]["Sij"][2] - 720.7174837474682) < 1e-5
        assert np.abs(dic["Fv"]["dissipationRate"]["Sij"][2] - 717.6573164591133) < 1e-5
        assert (
            np.abs(dic["Re"]["dissipationRate"]["Sij"][4] - -366.86428326638855) < 1e-5
        )
        assert (
            np.abs(dic["Fv"]["dissipationRate"]["Sij"][4] - -361.62702184458084) < 1e-5
        )

        #
        # Velocity divergence
        #
        assert np.abs(dic["Re"]["dissipationRate"]["divU"] - -1159.6141906058674) < 1e-5
        assert np.abs(dic["Fv"]["dissipationRate"]["divU"] - -1156.5265330325994) < 1e-5

        #
        # Mean of deviatoric part of strain rate
        #
        assert (
            np.abs(dic["Re"]["dissipationRate"]["SijD"][0] - -1513.4444902960745) < 1e-5
        )
        assert (
            np.abs(dic["Fv"]["dissipationRate"]["SijD"][0] - -1508.3498964391604) < 1e-5
        )
        assert (
            np.abs(dic["Re"]["dissipationRate"]["SijD"][1] - 406.1889430133169) < 1e-5
        )
        assert (
            np.abs(dic["Fv"]["dissipationRate"]["SijD"][1] - 405.18373563584714) < 1e-5
        )
        assert (
            np.abs(dic["Re"]["dissipationRate"]["SijD"][2] - 1107.2555472827573) < 1e-5
        )
        assert (
            np.abs(dic["Fv"]["dissipationRate"]["SijD"][2] - 1103.1661608033123) < 1e-5
        )
        assert (
            np.abs(dic["Re"]["dissipationRate"]["SijD"][4] - -366.86428326638855) < 1e-5
        )
        assert (
            np.abs(dic["Fv"]["dissipationRate"]["SijD"][4] - -361.62702184458084) < 1e-5
        )

        #
        # Dissipation rate
        #
        assert np.abs(dic["Re"]["dissipationRate"]["eps"] - 1498.386081942177) < 1e-5
        assert np.abs(dic["Fv"]["dissipationRate"]["eps"] - 1501.74145123107) < 1e-5
        assert (
            np.abs(dic["Fv"]["dissipationRate"]["eps_res"] - 446.3211712138184) < 1e-5
        )
        assert (
            np.abs(dic["Fv"]["dissipationRate"]["eps_sgs"] - 1055.4202800172518) < 1e-5
        )

        assert np.abs(dic["kolmogorov"]["l_K_inco"] - 0.0003852063263624567) < 1e-9
        assert np.abs(dic["kolmogorov"]["t_K_inco"] - 0.00046265191477980823) < 1e-9
        assert np.abs(dic["kolmogorov"]["v_K_inco"] - 0.8326050623734899) < 1e-9

        assert np.abs(dic["kolmogorov"]["l_K"] - 0.0003845425642179253) < 1e-9
        assert np.abs(dic["kolmogorov"]["t_K"] - 0.00046177585649846876) < 1e-9
        assert np.abs(dic["kolmogorov"]["v_K"] - 0.8327472274835148) < 1e-9

        assert np.abs(dic["integral"]["l_t"] - 6.246663003820475e-05) < 1e-9
        assert np.abs(dic["integral"]["t_t"] - 0.00013747814450816272) < 1e-9
        assert np.abs(dic["integral"]["v_t"] - 0.45437498637825896) < 1e-9
        assert np.abs(dic["integral"]["Re_t"] - 0.08863492447233662) < 1e-9
        assert np.abs(dic["integral"]["Re_t_+sgs"] - 0.41385066142990573) < 1e-8

        assert np.abs(dic["cutoff"]["l_filter"] - 0.0005) < 1e-9
        assert np.abs(dic["cutoff"]["rati_l_cutoff_l_K"] - 2.6004923591066684) < 1e-4
        assert np.abs(dic["cutoff"]["rati_l_cutoff_l_t"] - 16.008547273774774) < 1e-9

        assert (
            np.abs(
                dic["favre_error"]["error_veloc_Fv_Re_Yoshizawa"] - 0.05304261655087742
            )
            < 1e-7
        )


def test_PointType_processSinglePointFiles_PremixedScaleCONCE():
    """
    Extract and PROCESS relevant files associated with a single point.
    """
    #
    # Paths
    #
    test_case = os.path.join(tmp_test_path, "point_type_process_PremixedScaleCONCE")
    if not os.path.isdir(test_case):
        os.makedirs(test_case)
    json_path_glob = os.path.join(test_case, "globalConfig.json")
    json_path_loc = os.path.join(test_case, "KolmogorovAtWitnessInputFile.json")

    #
    # Copy data
    #
    for fn in [
        "mixedEq_pr_box_Var_vec.chm.wit",
        "mixedEq_pr_box_Var_vec.nsi.wit",
        "mixedEq_pr_box_Var_vec.tem.wit",
        "witness.dat",
    ]:
        shutil.copy(
            os.path.join(test_path, "data", "simple_prem_case", fn),
            os.path.join(test_case, fn),
        )

    #
    # Set case data
    #
    data = {
        "CasePath": test_case,
        "CaseName": "mixedEq_pr_box_Var_vec",
        "Modules": ["nsi", "tem", "chm"],
        "PropertyConstPressure": 101325.0,
        "PropertyDensityMethod": "EoS_T_from_temper",
        "PropertyLookupTable": os.path.join(
            test_path, "data", "simple_prem_case", "kaust_prem_turbulentProp_table.dat"
        ),
        "PropertyMethod": "tabulated",
        "PropertyControlVariableMethod": "CONCE_perfectly_premixed",
        "PropertyControlVariableSconcIndexToConceIndex": [1, 2],
        "Tasks": ["kolmogorov", "integral", "chemical_scales"],
    }
    with open(json_path_loc, "w") as f:
        json.dump(data, f)

    if ("BSC_MACHINE" in os.environ) and ("mn4" in os.getenv("BSC_MACHINE")):
        globdata = {"AlyaPath": "/gpfs/projects/bsc21/bsc21304/git/master"}
    else:
        globdata = {"AlyaPath": "/usr/local/bin/"}
    with open(json_path_glob, "w") as f:
        json.dump(globdata, f)

    lc = LocalConfig(json_path_glob=json_path_glob, json_path_loc=json_path_loc)
    lc.getData()

    #
    # Create point
    #
    pointIndexes = lc.getWitnessIndces()

    #
    # Check extraction with enthalpy from SCONC
    #
    ii = 0
    point = PointType(index=ii)
    point.extractSinglePointFiles(lc)

    for fn in [
        "tmp_mixedEq_pr_box_Var_vec.chm.wit_entity1_varCON01",
        "tmp_mixedEq_pr_box_Var_vec.chm.wit_entity1_varCON02",
        "tmp_mixedEq_pr_box_Var_vec.nsi.wit_entity1_varS11",
        "tmp_mixedEq_pr_box_Var_vec.nsi.wit_entity1_varS12",
        "tmp_mixedEq_pr_box_Var_vec.nsi.wit_entity1_varS13",
        "tmp_mixedEq_pr_box_Var_vec.nsi.wit_entity1_varS22",
        "tmp_mixedEq_pr_box_Var_vec.nsi.wit_entity1_varS23",
        "tmp_mixedEq_pr_box_Var_vec.nsi.wit_entity1_varS33",
        "tmp_mixedEq_pr_box_Var_vec.nsi.wit_entity1_varTURBU",
        "tmp_mixedEq_pr_box_Var_vec.nsi.wit_entity1_varVELOX",
        "tmp_mixedEq_pr_box_Var_vec.nsi.wit_entity1_varVELOY",
        "tmp_mixedEq_pr_box_Var_vec.nsi.wit_entity1_varVELOZ",
        "tmp_mixedEq_pr_box_Var_vec.tem.wit_entity1_varTEMPE",
    ]:
        assert os.path.isfile(os.path.join(test_case, fn))

    #
    # Process data
    #
    point.processSinglePointFiles(lc)

    assert np.abs(np.max(point.processedData["rho"]) - 0.3777586213395184) < 1e-5
    assert np.abs(np.min(point.processedData["rho"]) - 0.3193533082260547) < 1e-5
    assert np.abs(np.min(point.processedData["nu"]) - 0.00010387151880286756) < 1e-9

    assert np.abs(point.averagedData["Fv"]["chm"]["CON01"] - 0.3504759815458336) < 1e-7
    assert np.abs(point.averagedData["Fv"]["chm"]["CON02"] - 0.04059044080601005) < 1e-7
    assert np.abs(point.averagedData["Fv"]["chm"]["SCO01"] - 0.3504759815458336) < 1e-7
    assert np.abs(point.averagedData["Fv"]["chm"]["SCO02"] - 0.17533628269573734) < 1e-7

    assert np.abs(point.averagedData["integral"]["l_t"] - 0.0011310545955687632) < 1e-9
    assert np.abs(point.averagedData["integral"]["t_t"] - 0.0032461063956689728) < 1e-9
    assert np.abs(point.averagedData["integral"]["v_t"] - 0.34843423403430074) < 1e-9

    assert (
        np.abs(point.averagedData["kolmogorov"]["l_K"] - 0.0004724838112962818) < 1e-9
    )
    assert np.abs(point.averagedData["kolmogorov"]["t_K"] - 0.001813976513863439) < 1e-9
    assert np.abs(point.averagedData["kolmogorov"]["v_K"] - 0.2604685384211384) < 1e-9


def test_PointType_deleteSinglePointFiles():
    """
    Delete files associated with a single point.
    """

    test_case = os.path.join(tmp_test_path, "point_type_delete_files")
    if not os.path.isdir(test_case):
        os.makedirs(test_case)

    file_list = [
        "tmp_cam.chm.wit_entity12_varSCO01",
        "tmp_cam.chm.wit_entity12_varSCO02",
        "tmp_cam.chm.wit_entity12_varSCO03",
        "tmp_cam.chm.wit_entity12_varSCO04",
        "tmp_cam.nsi.wit_entity12_varS11",
        "tmp_cam.nsi.wit_entity12_varS12",
        "tmp_cam.nsi.wit_entity12_varS13",
        "tmp_cam.nsi.wit_entity12_varS22",
        "tmp_cam.nsi.wit_entity12_varS23",
        "tmp_cam.nsi.wit_entity12_varS33",
        "tmp_cam.nsi.wit_entity12_varTURBU",
        "tmp_cam.nsi.wit_entity12_varVELOX",
        "tmp_cam.nsi.wit_entity12_varVELOY",
        "tmp_cam.nsi.wit_entity12_varVELOZ",
        "tmp_cam.tem.wit_entity12_varENTHA",
    ]

    other_files = [
        "tmp_cam.chm.wit_entity122_varSCO01",
        "tmp_cam.chm.wit_entity122_varSCO02",
        "tmp_cam.chm.wit_entity122_varSCO03",
        "tmp_cam.chm.wit_entity122_varSCO04",
        "tmp_cam.nsi.wit_entity122_varS11",
        "tmp_cam.nsi.wit_entity122_varS12",
        "tmp_cam.nsi.wit_entity122_varS13",
        "tmp_cam.nsi.wit_entity122_varS22",
        "tmp_cam.nsi.wit_entity122_varS23",
        "tmp_cam.nsi.wit_entity122_varS33",
        "tmp_cam.nsi.wit_entity122_varTURBU",
        "tmp_cam.nsi.wit_entity122_varVELOX",
        "tmp_cam.nsi.wit_entity122_varVELOY",
        "tmp_cam.nsi.wit_entity122_varVELOZ",
        "tmp_cam.tem.wit_entity122_varENTHA",
    ]

    #
    # Create files
    #
    for fn in file_list:
        f = open(os.path.join(test_case, fn), "w")
        f.close()
    for fn in other_files:
        f = open(os.path.join(test_case, fn), "w")
        f.close()

    #
    # Test existence
    #
    for fn in file_list:
        assert os.path.isfile(os.path.join(test_case, fn))
    for fn in other_files:
        assert os.path.isfile(os.path.join(test_case, fn))

    #
    # Create point
    #
    ii = 11
    point = PointType(index=ii)
    lc = LocalConfig()
    lc.locdata["CasePath"] = test_case

    #
    # Remove points
    #
    point.deleteSinglePointFiles(lc)

    #
    # Test non-existence
    #
    for fn in file_list:
        assert not os.path.isfile(os.path.join(test_case, fn))
    for fn in other_files:
        assert os.path.isfile(os.path.join(test_case, fn))
