#################################################################################
#   KolmogorovAtWitness                                                         #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import os
import json
import shutil
import numpy as np

from .context import kolmogorovatwitness, getTestPath, getTestTmpPath
from kolmogorovatwitness.configuration import GlobalConfig, LocalConfig
from kolmogorovatwitness.plotting import (
    plotBorghiDiagram,
    setPltBackend,
    plotFilterRegimeDiagram,
    plotWilliamsRegimeDiagram,
    plotSpectrum,
    plotMixtureFractionRegimeDiagram,
)

test_path = getTestPath()
tmp_test_path = getTestTmpPath()


def test_plotting_Borghi_diagram():
    """
    Check plotting of the Borghi diagram.
    """
    #
    # Paths
    #
    test_case = os.path.join(tmp_test_path, "plot_Borghi")
    if not os.path.isdir(test_case):
        os.makedirs(test_case)
    json_path_glob = os.path.join(test_case, "globalConfig.json")
    json_path_loc = os.path.join(test_case, "KolmogorovAtWitnessInputFile.json")

    #
    # Copy data to plot
    #
    shutil.copy(
        os.path.join(test_path, "data", "dataBorghi", "witness_data_merged.json"),
        os.path.join(test_case, "witness_data_merged.json"),
    )

    #
    # Set case data
    #
    data = {
        "CasePath": test_case,
        "CaseName": "case",
        "WitnessDefinitionPath": os.path.join(
            test_path, "data", "witnessDefinitionExample.dat"
        ),
    }
    with open(json_path_loc, "w") as f:
        json.dump(data, f)

    lc = LocalConfig(json_path_glob=json_path_glob, json_path_loc=json_path_loc)

    #
    # Read extra data
    #
    lc.getData()

    #
    # Test plotting
    #
    plt = setPltBackend(backend="ps")

    #
    # Default
    #
    plotBorghiDiagram(lc)
    fn = os.path.join(test_case, "PetersDiagram.eps")
    assert os.path.isfile(fn)

    plotBorghiDiagram(lc, labelStyle="Borghi")
    fn = os.path.join(test_case, "BorghiDiagram.eps")
    assert os.path.isfile(fn)


def test_plotting_LES_Regime_diagram():
    """
    Check plotting of the LES regime diagram of Pitsch.
    """
    #
    # Paths
    #
    test_case = os.path.join(tmp_test_path, "plot_Pitsch")
    if not os.path.isdir(test_case):
        os.makedirs(test_case)
    json_path_glob = os.path.join(test_case, "globalConfig.json")
    json_path_loc = os.path.join(test_case, "KolmogorovAtWitnessInputFile.json")

    #
    # Copy data to plot
    #
    shutil.copy(
        os.path.join(test_path, "data", "dataBorghi", "witness_data_merged.json"),
        os.path.join(test_case, "witness_data_merged.json"),
    )

    #
    # Set case data
    #
    data = {
        "CasePath": test_case,
        "CaseName": "case",
        "WitnessDefinitionPath": os.path.join(
            test_path, "data", "witnessDefinitionExample.dat"
        ),
    }
    with open(json_path_loc, "w") as f:
        json.dump(data, f)

    lc = LocalConfig(json_path_glob=json_path_glob, json_path_loc=json_path_loc)

    #
    # Read extra data
    #
    lc.getData()

    #
    # Test plotting
    #
    plt = setPltBackend(backend="ps")

    #
    # Default
    #
    plotFilterRegimeDiagram(lc)
    fn = os.path.join(test_case, "LESRegimeDiagram.eps")
    assert os.path.isfile(fn)


def test_plotting_Williams_Regime_diagram():
    """
    Check plotting of the regime diagram of Williams.
    """
    #
    # Paths
    #
    test_case = os.path.join(tmp_test_path, "plot_Williams")
    if not os.path.isdir(test_case):
        os.makedirs(test_case)
    json_path_glob = os.path.join(test_case, "globalConfig.json")
    json_path_loc = os.path.join(test_case, "KolmogorovAtWitnessInputFile.json")

    #
    # Copy data to plot
    #
    shutil.copy(
        os.path.join(test_path, "data", "dataBorghi", "witness_data_merged.json"),
        os.path.join(test_case, "witness_data_merged.json"),
    )

    #
    # Set case data
    #
    data = {
        "CasePath": test_case,
        "CaseName": "case",
        "WitnessDefinitionPath": os.path.join(
            test_path, "data", "witnessDefinitionExample.dat"
        ),
    }
    with open(json_path_loc, "w") as f:
        json.dump(data, f)

    lc = LocalConfig(json_path_glob=json_path_glob, json_path_loc=json_path_loc)

    #
    # Read extra data
    #
    lc.getData()

    #
    # Test plotting
    #
    plt = setPltBackend(backend="ps")

    #
    # Default
    #
    plotWilliamsRegimeDiagram(lc)
    fn = os.path.join(test_case, "WilliamsDiagram.eps")
    assert os.path.isfile(fn)


def test_interctive_spectrum_plotting(mocker):
    """
    Check plotting of spectrum file interactively.
    """
    #
    # Paths
    #
    test_case = os.path.join(tmp_test_path, "plot_spectrum")
    if not os.path.isdir(test_case):
        os.makedirs(test_case)
    json_path_glob = os.path.join(test_case, "globalConfig.json")
    json_path_loc = os.path.join(test_case, "KolmogorovAtWitnessInputFile.json")

    #
    # Copy data to plot
    #
    shutil.copy(
        os.path.join(
            test_path, "data", "camb_example", "witness_spectrum_data_entity84.json"
        ),
        os.path.join(test_case, "witness_spectrum_data_entity84.json"),
    )

    #
    # Set case data
    #
    data = {
        "CasePath": test_case,
        "CaseName": "case",
        "WitnessDefinitionPath": os.path.join(
            test_path, "data", "witnessDefinitionExample.dat"
        ),
    }
    with open(json_path_loc, "w") as f:
        json.dump(data, f)

    lc = LocalConfig(json_path_glob=json_path_glob, json_path_loc=json_path_loc)

    #
    # Read extra data
    #
    lc.getData()

    #
    # Test plotting
    #
    plt = setPltBackend(backend="ps")

    #
    # Mock user input
    #
    list_loc = [
        "84",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
    ]

    def mock_input(s):
        return list_loc.pop(0)

    mocker.patch("six.moves.input", mock_input)

    #
    # Default
    #
    plotSpectrum(lc)
    fn = os.path.join(test_case, "tmp_fig.eps")
    assert os.path.isfile(fn)
    os.remove(fn)

    ######################
    # Try other spectrum #
    ######################
    list_loc = [
        "84",
        "",
        "welch",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
    ]

    def mock_input(s):
        return list_loc.pop(0)

    mocker.patch("six.moves.input", mock_input)

    #
    # Default
    #
    plotSpectrum(lc)
    fn = os.path.join(test_case, "tmp_fig.eps")
    assert os.path.isfile(fn)
    os.remove(fn)

    ####################################
    # Try spectrum that is not present #
    ####################################
    list_loc = [
        "84",
        "",
        "lombscargle",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
    ]

    def mock_input(s):
        return list_loc.pop(0)

    mocker.patch("six.moves.input", mock_input)

    #
    # Default
    #
    plotSpectrum(lc)
    fn = os.path.join(test_case, "tmp_fig.eps")
    assert not os.path.isfile(fn)


def test_plotting_Mixture_Fraction_Regime_diagram():
    """
    Check plotting of the regime diagram of Peters for mixture fraction variations.
    """
    #
    # Paths
    #
    test_case = os.path.join(tmp_test_path, "plot_MixFrac")
    if not os.path.isdir(test_case):
        os.makedirs(test_case)
    json_path_glob = os.path.join(test_case, "globalConfig.json")
    json_path_loc = os.path.join(test_case, "KolmogorovAtWitnessInputFile.json")

    #
    # Copy data to plot
    #
    shutil.copy(
        os.path.join(test_path, "data", "dataBorghi", "witness_data_merged.json"),
        os.path.join(test_case, "witness_data_merged.json"),
    )

    #
    # Set case data
    #
    data = {
        "CasePath": test_case,
        "CaseName": "case",
        "WitnessDefinitionPath": os.path.join(
            test_path, "data", "witnessDefinitionExample.dat"
        ),
    }
    with open(json_path_loc, "w") as f:
        json.dump(data, f)

    lc = LocalConfig(json_path_glob=json_path_glob, json_path_loc=json_path_loc)

    #
    # Read extra data
    #
    lc.getData()

    #
    # Test plotting
    #
    plt = setPltBackend(backend="ps")

    #
    # Default
    #
    plotMixtureFractionRegimeDiagram(lc)
    fn = os.path.join(test_case, "mixtureFractionRegimeDiagram.eps")
    assert os.path.isfile(fn)
