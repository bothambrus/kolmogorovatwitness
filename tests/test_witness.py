#################################################################################
#   KolmogorovAtWitness                                                         #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import os
import shutil

import numpy as np

from .context import kolmogorovatwitness, getTestPath, getTestTmpPath
from kolmogorovatwitness.witness import (
    extractSinglePointFileWithAlyaUtils,
    extractHeaderInfo,
    getTimeArrayFromAlyaTimeSetFile,
    getDataArrayFromAlyaTimeSetFile,
)

test_path = getTestPath()
tmp_test_path = getTestTmpPath()


def test_calling_extractSinglePointFileWithAlyaUtils():
    """
    Check calling the extractSinglePointFileWithAlyaUtils function.
    """

    #
    # Paths
    #
    test_case = os.path.join(tmp_test_path, "call_extractSinglePointFileWithAlyaUtils")
    if not os.path.isdir(test_case):
        os.makedirs(test_case)

    #
    # Copy data
    #
    for fn in ["cam.chm.wit"]:
        shutil.copy(
            os.path.join(test_path, "data", "camb_example", fn),
            os.path.join(test_case, fn),
        )

    if ("BSC_MACHINE" in os.environ) and ("mn4" in os.getenv("BSC_MACHINE")):
        AlyaPath = "/gpfs/projects/bsc21/bsc21304/git/master"
    else:
        AlyaPath = "/usr/local/bin/"

    #
    # Test function
    #
    extractSinglePointFileWithAlyaUtils(
        path=test_case,
        AlyaPath=AlyaPath,
        timeSeriesFile=os.path.join(test_case, "cam.chm.wit"),
        entityIndex=2,
        column=2,
        newName=os.path.join(test_case, "output.dat"),
    )

    #
    # Check existence of file
    #
    fn = os.path.join(test_case, "output.dat")
    assert os.path.isfile(fn)

    #
    # Check contents of file
    #
    data = np.loadtxt(fn)

    assert np.abs(np.max(data[:, 0]) - 0.00093758098) < 1e-7
    assert np.abs(np.max(data[:, 1]) - 0.12590382) < 1e-7
    assert np.abs(np.max(data[:, 2]) - 2.2119068e-06) < 1e-10


def test_calling_extractHeaderInfo():
    """
    Check calling the extractHeaderInfo function.
    """

    #
    # Test chemic
    #
    headerInfo = extractHeaderInfo(
        path=os.path.join(test_path, "data", "camb_example"),
        timeSeriesFile="cam.chm.wit",
    )

    for k in [
        "ISET",
        "CON01",
        "CON02",
        "CON03",
        "CON04",
        "CON05",
        "CON06",
        "CON07",
        "CON08",
        "SCO01",
        "SCO02",
        "SCO03",
        "SCO04",
        "SCO05",
        "SCO06",
        "SCO07",
        "SCO08",
        "SCO09",
    ]:
        assert k in headerInfo["Vars"]

    assert headerInfo["VarCols"]["CON01"] == 2
    assert headerInfo["VarCols"]["SCO01"] == 10

    assert headerInfo["nLines"] == 23
    assert headerInfo["nVar"] == 18
    assert headerInfo["nEntity"] == 130

    #
    # Test nastin
    #
    headerInfo = extractHeaderInfo(
        path=os.path.join(test_path, "data", "camb_example"),
        timeSeriesFile="cam.nsi.wit",
    )

    for k in [
        "ISET",
        "VELOX",
        "VELOY",
        "VELOZ",
        "PRESS",
        "S11",
        "S22",
        "S12",
        "S33",
        "S13",
        "S23",
        "TURBU",
    ]:
        assert k in headerInfo["Vars"]

    assert headerInfo["VarCols"]["VELOX"] == 2
    assert headerInfo["VarCols"]["S11"] == 6
    assert headerInfo["VarCols"]["TURBU"] == 12

    assert headerInfo["nLines"] == 17
    assert headerInfo["nVar"] == 12
    assert headerInfo["nEntity"] == 130

    #
    # Test temper
    #
    headerInfo = extractHeaderInfo(
        path=os.path.join(test_path, "data", "camb_example"),
        timeSeriesFile="cam.tem.wit",
    )

    print(headerInfo)
    for k in ["ISET", "TEMPE", "ENTHA"]:
        assert k in headerInfo["Vars"]

    assert headerInfo["VarCols"]["TEMPE"] == 2
    assert headerInfo["VarCols"]["ENTHA"] == 3

    assert headerInfo["nLines"] == 8
    assert headerInfo["nVar"] == 3
    assert headerInfo["nEntity"] == 130


def test_calling_getTimeArrayFromAlyaTimeSetFile():
    """
    Check calling the getTimeArrayFromAlyaTimeSetFile function.
    """

    #
    # Test chemic
    #
    time, mask = getTimeArrayFromAlyaTimeSetFile(
        path=os.path.join(test_path, "data", "simple_case"),
        extractedTimeSeriesFile="tmp_mixedEq_non_prem_box_VarVar_vectorized.chm.wit_entity2_varDENSI",
    )

    assert len(time) == 31
    assert len(mask) == 37
    assert not all(mask[7:10])


def test_calling_getDataArrayFromAlyaTimeSetFile():
    """
    Check calling the getDataArrayFromAlyaTimeSetFile function.
    """

    #
    # Test chemic
    #
    time, mask = getTimeArrayFromAlyaTimeSetFile(
        path=os.path.join(test_path, "data", "simple_case"),
        extractedTimeSeriesFile="tmp_mixedEq_non_prem_box_VarVar_vectorized.chm.wit_entity2_varDENSI",
    )

    data = getDataArrayFromAlyaTimeSetFile(
        path=os.path.join(test_path, "data", "simple_case"),
        extractedTimeSeriesFile="tmp_mixedEq_non_prem_box_VarVar_vectorized.chm.wit_entity2_varDENSI",
        mask=mask,
    )

    assert len(time) == 31
    assert len(mask) == 37
    assert len(data) == 31

    assert np.abs(np.max(data) - 0.36275916) < 1e-6
    assert np.abs(np.min(data) - 0.25285705) < 1e-6
