#################################################################################
#   KolmogorovAtWitness                                                         #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import os
import json
import numpy as np

from .context import kolmogorovatwitness, getTestPath, getTestTmpPath
from kolmogorovatwitness.configuration import GlobalConfig, LocalConfig

test_path = getTestPath()
tmp_test_path = getTestTmpPath()


def test_globalConfig():
    """
    Check global config.
    """
    #
    # Initialize from scratch
    #
    test_case = os.path.join(tmp_test_path, "globconf")
    if not os.path.isdir(test_case):
        os.makedirs(test_case)
    json_path_glob = os.path.join(test_case, "globalConfig.json")
    gc = GlobalConfig(json_path_glob=json_path_glob)

    assert os.path.isfile(os.path.join(json_path_glob))
    with open(json_path_glob, "r") as f:
        data = json.load(f)
    assert "AlyaPath" in data.keys()
    gc.globdata["AlyaPath"] = "mypath"
    gc.writeGlob()

    #
    # Read again
    #
    gc2 = GlobalConfig(json_path_glob=json_path_glob)
    assert gc2.globdata["AlyaPath"] == "mypath"


def test_localConfig():
    """
    Check local config.
    """
    #
    # Initialize from scratch
    #
    test_case = os.path.join(tmp_test_path, "locconf")
    if not os.path.isdir(test_case):
        os.makedirs(test_case)
    json_path_glob = os.path.join(test_case, "globalConfig.json")
    json_path_loc = os.path.join(test_case, "KolmogorovAtWitnessInputFile.json")
    lc = LocalConfig(json_path_glob=json_path_glob, json_path_loc=json_path_loc)

    assert os.path.isfile(os.path.join(json_path_glob))
    assert not os.path.isfile(os.path.join(json_path_loc))
    assert "AlyaPath" in lc.globdata.keys()

    #
    # Add some data
    #
    lc.locdata["CasePath"] = test_case
    lc.locdata["WitnessDefinitionPath"] = os.path.join(test_case, "witness.dat")

    #
    # Write
    #
    lc.writeLoc()
    assert os.path.isfile(os.path.join(json_path_loc))
    with open(json_path_loc, "r") as f:
        data = json.load(f)
    assert "CasePath" in data.keys()

    #
    # Read again
    #
    lc2 = LocalConfig(json_path_glob=json_path_glob, json_path_loc=json_path_loc)
    assert lc2.locdata["CasePath"] == test_case


def test_localConfig_read_witness_defintioins():
    """
    Check reading witness file.
    """
    #
    # Paths
    #
    test_case = os.path.join(tmp_test_path, "locconf_witdef")
    if not os.path.isdir(test_case):
        os.makedirs(test_case)
    json_path_glob = os.path.join(test_case, "globalConfig.json")
    json_path_loc = os.path.join(test_case, "KolmogorovAtWitnessInputFile.json")

    #
    # Set case data
    #
    data = {
        "CasePath": test_case,
        "WitnessDefinitionPath": os.path.join(
            test_path, "data", "witnessDefinitionExample.dat"
        ),
        "FilterSizeMethod": "list_file",
        "FilterSizePath": os.path.join(test_path, "data", "witnessFilterExample.dat"),
    }
    with open(json_path_loc, "w") as f:
        json.dump(data, f)

    lc = LocalConfig(json_path_glob=json_path_glob, json_path_loc=json_path_loc)

    #
    # Derive extra data
    #
    lc.getData()

    assert lc.witnessDefinition[5, 0] == 0
    assert lc.witnessDefinition[5, 1] == 0
    assert lc.witnessDefinition[5, 2] == 0.045
    assert lc.witnessDefinition[10, 0] == 0
    assert lc.witnessDefinition[10, 1] == 0.05
    assert lc.witnessDefinition[10, 2] == 0.04
    assert lc.filterSize[5] == 0.00056
    assert lc.filterSize[6] == 0.00061

    #
    # Try other setting
    #
    lc.locdata["FilterSizeMethod"] = "constant"
    lc.locdata["FilterSizeConstant"] = 0.001

    #
    # Derive extra data
    #
    lc.getData()
    assert lc.filterSize[5] == 0.001
    assert lc.filterSize[6] == 0.001

    #
    # Try Cambridge
    #
    lc.locdata["WitnessDefinitionPath"] = os.path.join(
        test_path, "data", "camb_example", "witness.dat"
    )
    lc.getData()
    assert lc.ndim == 3
    assert lc.nwit == 130
    assert np.all(
        np.abs(lc.witnessDefinition[5, :] - np.array([0.1502, 0.01, 0.0])) < 1e-3
    )
    assert np.all(
        np.abs(lc.witnessDefinition[10, :] - np.array([0.1502, 0.02, 0.0])) < 1e-3
    )

    #
    # Try with bad values
    #
    lc.locdata["WitnessDefinitionPath"] = "."
    lc.getData()
    assert lc.witnessDefinition is None


def test_localConfig_get_specific_witness_index():
    """
    Check getting specific witness points.
    """
    #
    # Paths
    #
    test_case = os.path.join(tmp_test_path, "locconf_witdef")
    if not os.path.isdir(test_case):
        os.makedirs(test_case)
    json_path_glob = os.path.join(test_case, "globalConfig.json")
    json_path_loc = os.path.join(test_case, "KolmogorovAtWitnessInputFile.json")

    #
    # Set case data
    #
    data = {
        "CasePath": test_case,
        "WitnessDefinitionPath": os.path.join(
            test_path, "data", "witnessDefinitionExample.dat"
        ),
    }
    with open(json_path_loc, "w") as f:
        json.dump(data, f)

    lc = LocalConfig(json_path_glob=json_path_glob, json_path_loc=json_path_loc)

    #
    # Read extra data
    #
    lc.getData()

    #
    # Test getting indeces
    #
    indeces = lc.getWitnessIndces(zcrit="equal", z=0.03, xcrit="range", xlow=0.05)
    assert len(indeces) == 2
    assert np.all(lc.witnessDefinition[indeces, 2] == 0.03)
    assert np.all(lc.witnessDefinition[indeces, 0] >= 0.05)

    #
    # Test using getWitnessIndcesFromLocData
    #
    indeces = lc.getWitnessIndcesFromLocData()
    assert len(indeces) == 30
    lc.locdata["WitnessFilterZcrit"] = "equal"
    lc.locdata["WitnessFilterZvalue"] = 0.03
    lc.locdata["WitnessFilterXcrit"] = "range"
    lc.locdata["WitnessFilterXrange"] = [0.05, 1e10]

    indeces = lc.getWitnessIndcesFromLocData()
    assert len(indeces) == 2
    assert np.all(lc.witnessDefinition[indeces, 2] == 0.03)
    assert np.all(lc.witnessDefinition[indeces, 0] >= 0.05)
