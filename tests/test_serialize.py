#################################################################################
#   KolmogorovAtWitness                                                         #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import os
import shutil

import numpy as np

from .context import kolmogorovatwitness, getTestPath, getTestTmpPath
from kolmogorovatwitness.serialize import serializeDictionary


def test_calling_serializeDictionary():
    """
    Check calling the serializeDictionary function.
    """

    inp = {}
    inp["a"] = 1.0
    inp["b"] = [1.0, 2.0, 3.0]
    inp["c"] = np.linspace(0, 1, 5)
    inp["d"] = np.linspace(0, 1, 20)
    inp["e"] = np.linspace(0, 1, 9).reshape([3, 3])
    inp["f"] = np.linspace(0, 1, 27).reshape([3, 3, 3])
    inp["g"] = []
    inp["h"] = {}
    inp["i"] = {}
    inp["i"]["a"] = np.linspace(0, 1, 5)
    inp["i"]["b"] = np.linspace(0, 1, 20)

    out = {}
    out["preexisting_data"] = 123.0

    #
    # Call serialization with default array size limit
    #
    out = serializeDictionary(out, inp)

    #
    # Small arrays here
    #
    for k in ["preexisting_data", "a", "b", "c", "e", "g", "h", "i"]:
        assert k in out.keys()

    #
    # Large arrays absent
    #
    for k in ["d", "f"]:
        assert not k in out.keys()

    #
    # Contents are the same
    #
    for k in ["a", "b", "c", "e", "g", "h"]:
        assert np.all(inp[k] == out[k])

    for k in ["a"]:
        assert np.all(inp["i"][k] == out["i"][k])

    #
    # Call serialization with infinite limit
    #
    out = {}
    out["preexisting_data"] = 123.0
    out = serializeDictionary(out, inp, elementLimit=np.inf)

    #
    # Small arrays here
    #
    for k in ["preexisting_data", "a", "b", "c", "d", "e", "f", "g", "h", "i"]:
        assert k in out.keys()

    #
    # Contents are the same
    #
    for k in ["a", "b", "c", "e", "g", "h"]:
        assert np.all(inp[k] == out[k])

    for k in ["a", "b"]:
        assert np.all(inp["i"][k] == out["i"][k])
