#################################################################################
#   KolmogorovAtWitness                                                         #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import copy
import os
import json

import numpy as np

from .context import kolmogorovatwitness, getTestPath, getTestTmpPath
from kolmogorovatwitness.configuration import LocalConfig
from kolmogorovatwitness.input import (
    inputCaseData,
    inputWitnessPointData,
    inputTaskData,
    inputMaterialPropertyData,
    inputChemicalScalesData,
)

testPath = getTestPath()

emptylist = []
for i in range(1000):
    emptylist.append("")

#
# Case inputs
#
cdlist = [
    os.path.abspath(os.path.dirname(__file__)),
    "case",
]

#
# Witness inputs
#
wdlist1 = [
    os.path.join(testPath, "data", "witnessDefinitionExample.dat"),
    "range",
    "0.05,1e10",
    "any",
    "equal",
    "0.03",
    "list_file",
    os.path.join(testPath, "data", "witnessFilterExample.dat"),
]

wdlist2 = [
    os.path.join(testPath, "data", "witnessDefinitionExample.dat"),
    "range",
    "0.05,1e10",
    "any",
    "equal",
    "0.03",
    "constant",
    "hex",
    "0.001",
]

wdlist3 = [
    os.path.join(testPath, "data", "witnessDefinitionExample.dat"),
    "range",
    "0.05,1e10",
    "any",
    "equal",
    "0.03",
    "constant",
    "tet",
    "0.001",
]


tdlist = [
    "0.00001",
    "1000",
    "nsi, tem, chm",
    "'kolmogorov', 'cutoff', 'integral', 'favre_error', 'spectrum', 'chemical_scales'",
    "y",
    "y",
    "'VELOX', 'VELOY', 'VELOZ'",
    "'TEMPE'",
    "'CON01'",
    "'rho'",
    "",
]

mdlist1 = ["constant", "1.1", "1.2e-5"]


mdlist2 = [
    "tabulated",
    "CONCE",
    [1, 2, 3],
    os.path.join(testPath, "data", "camb_example", "tab_turbulentProp_table.dat"),
    "EoS_H_from_SCONC_to_NASA_poly",
    "10000.0",
    os.path.join(testPath, "data", "camb_example", "tab_turbulent_scaling_H.dat"),
]

chdlist = ["delta_diff, tau_c, a", "", "", "", "", ""]


def test_defaults(mocker):
    """
    Test all default values.
    """
    lc = LocalConfig()

    #
    # Mock user input
    #
    def mock_input(s):
        return emptylist.pop(0)

    mocker.patch("six.moves.input", mock_input)

    #
    # Call inputs
    #
    lc = inputCaseData(lc)
    lc = inputWitnessPointData(lc)
    lc = inputTaskData(lc)
    lc = inputMaterialPropertyData(lc)
    lc = inputChemicalScalesData(lc)

    print(lc.locdata)

    assert lc.locdata["CaseName"] == "c"
    for c in ["X", "Y", "Z"]:
        assert lc.locdata["WitnessFilter{}crit".format(c)] == "any"
    assert lc.locdata["FilterSizeMethod"] == "constant"
    assert lc.locdata["FilterSizeConstant"] == 0.001
    assert lc.locdata["Modules"] == ["nsi", "tem", "chm"]
    assert lc.locdata["Tasks"] == [
        "kolmogorov",
        "cutoff",
        "integral",
        "favre_error",
        "spectrum",
        "chemical_scales",
    ]
    assert lc.locdata["SaveTemporalData"] == False
    assert lc.locdata["PropertyMethod"] == "tabulated"
    assert lc.locdata["PropertyDensityMethod"] == "EoS_T_from_temper"
    assert lc.locdata["PropertyConstPressure"] == 101325.0
    assert lc.locdata["ChemicalScalesList"] == ["S_L", "delta_diff"]
    assert lc.locdata["ChemicalScalesMethod"] == "constant"
    assert lc.locdata["ChemicalScalesConst_S_L"] == 0.3
    assert lc.locdata["ChemicalScalesConst_delta_diff"] == 0.1e-3

    #
    # Call inputs again to pass through the options where
    # data is already available
    #
    lc = inputCaseData(lc)
    lc = inputWitnessPointData(lc)
    lc = inputTaskData(lc)
    lc = inputMaterialPropertyData(lc)
    lc = inputChemicalScalesData(lc)


def test_inputCaseData(mocker):
    """
    Test cd.
    """
    lc = LocalConfig()

    #
    # Mock user input
    #
    cdlist_loc = copy.deepcopy(cdlist)

    def mock_input(s):
        return cdlist_loc.pop(0)

    mocker.patch("six.moves.input", mock_input)

    #
    # Call inputs
    #
    lc = inputCaseData(lc)

    print(lc.locdata)

    assert lc.locdata["CasePath"] == os.path.abspath(os.path.dirname(__file__))
    assert lc.locdata["CaseName"] == "case"

    #
    # Call inputs again to pass through the options where
    # data is already available
    #
    cdlist_loc = copy.deepcopy(cdlist)

    def mock_input(s):
        return cdlist_loc.pop(0)

    lc = inputCaseData(lc)


def test_inputWitnessData(mocker):
    """
    Test wd.
    """
    lc = LocalConfig()

    #
    # Mock user input
    #
    list_loc = copy.deepcopy(wdlist1)

    def mock_input(s):
        return list_loc.pop(0)

    mocker.patch("six.moves.input", mock_input)

    #
    # Call inputs
    #
    lc = inputWitnessPointData(lc)

    print(lc.locdata)

    assert lc.locdata["WitnessDefinitionPath"] == os.path.abspath(
        os.path.join(testPath, "data", "witnessDefinitionExample.dat")
    )
    assert lc.locdata["WitnessFilterXcrit"] == "range"
    assert lc.locdata["WitnessFilterXrange"] == [0.05, 10000000000.0]
    assert lc.locdata["WitnessFilterYcrit"] == "any"
    assert lc.locdata["WitnessFilterZcrit"] == "equal"
    assert lc.locdata["WitnessFilterZvalue"] == 0.03
    assert lc.locdata["FilterSizeMethod"] == "list_file"
    assert lc.locdata["FilterSizePath"] == os.path.abspath(
        os.path.join(testPath, "data", "witnessFilterExample.dat")
    )

    #
    # Call again
    #
    list_loc = copy.deepcopy(wdlist1)
    lc = inputWitnessPointData(lc)

    #
    # Call with hex filter size
    #
    list_loc = copy.deepcopy(wdlist2)

    def mock_input(s):
        return list_loc.pop(0)

    lc = inputWitnessPointData(lc)
    assert lc.locdata["FilterSizeMethod"] == "constant"
    assert lc.locdata["FilterSizeConstant"] == 0.001

    #
    # Call with tet filter size
    #
    list_loc = copy.deepcopy(wdlist3)

    def mock_input(s):
        return list_loc.pop(0)

    lc = inputWitnessPointData(lc)
    assert lc.locdata["FilterSizeMethod"] == "constant"
    assert np.abs(lc.locdata["FilterSizeConstant"] - 0.0004902804589054803) < 1e-7


def test_inputTaskData(mocker):
    """
    Test td.
    """
    lc = LocalConfig()

    #
    # Mock user input
    #
    list_loc = copy.deepcopy(tdlist)

    def mock_input(s):
        return list_loc.pop(0)

    mocker.patch("six.moves.input", mock_input)

    #
    # Call inputs
    #
    lc = inputTaskData(lc)

    print(lc.locdata)

    assert lc.locdata["Modules"] == ["nsi", "tem", "chm"]
    assert lc.locdata["Tasks"] == [
        "kolmogorov",
        "cutoff",
        "integral",
        "favre_error",
        "spectrum",
        "chemical_scales",
    ]
    assert lc.locdata["SaveTemporalData"] == True


def test_inputMaterialData(mocker):
    """
    Test md.
    """
    lc = LocalConfig()

    #
    # Mock user input
    #
    list_loc = copy.deepcopy(mdlist1)

    def mock_input(s):
        return list_loc.pop(0)

    mocker.patch("six.moves.input", mock_input)

    #
    # Call inputs
    #
    lc = inputMaterialPropertyData(lc)

    print(lc.locdata)

    assert lc.locdata["PropertyMethod"] == "constant"
    assert lc.locdata["PropertyConstDensity"] == 1.1
    assert lc.locdata["PropertyConstKinematicViscosity"] == 1.2e-05

    #
    # Call again
    #
    list_loc = copy.deepcopy(mdlist1)
    lc = inputMaterialPropertyData(lc)

    #
    # Call with tabulated properties
    #
    list_loc = copy.deepcopy(mdlist2)
    lc = inputMaterialPropertyData(lc)

    print(lc.locdata)

    assert lc.locdata["PropertyMethod"] == "tabulated"
    assert lc.locdata["PropertyLookupTable"] == os.path.join(
        testPath, "data", "camb_example", "tab_turbulentProp_table.dat"
    )
    assert lc.locdata["PropertyDensityMethod"] == "EoS_H_from_SCONC_to_NASA_poly"
    assert lc.locdata["PropertyConstPressure"] == 10000.0
    assert lc.locdata["PropertyLookupEnthalpyScalingTable"] == os.path.join(
        testPath, "data", "camb_example", "tab_turbulent_scaling_H.dat"
    )

    #
    # Call again
    #
    list_loc = copy.deepcopy(mdlist2)
    lc = inputMaterialPropertyData(lc)


def test_inputChemicalScalesData(mocker):
    """
    Test td.
    """
    lc = LocalConfig()

    #
    # Mock user input
    #
    list_loc = copy.deepcopy(chdlist)

    def mock_input(s):
        return list_loc.pop(0)

    mocker.patch("six.moves.input", mock_input)

    #
    # Call inputs
    #
    lc = inputChemicalScalesData(lc)

    print(lc.locdata)

    assert lc.locdata["ChemicalScalesList"] == ["delta_diff", "tau_c", "a"]
    assert lc.locdata["ChemicalScalesMethod"] == "constant"
    assert lc.locdata["ChemicalScalesConst_delta_diff"] == 0.1e-3
    assert lc.locdata["ChemicalScalesConst_tau_c"] == 0.5e-3
    assert lc.locdata["ChemicalScalesConst_a"] == 0.0
