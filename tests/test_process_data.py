#################################################################################
#   KolmogorovAtWitness                                                         #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import os
import json
import shutil
import numpy as np

from .context import kolmogorovatwitness, getTestPath, getTestTmpPath
from kolmogorovatwitness.configuration import GlobalConfig, LocalConfig
from kolmogorovatwitness.process_data import (
    processWitnessPoints,
    extractWitnessPointData,
    deleteIntermediateWitnessPointData,
    mergeWitnessPointData,
)

test_path = getTestPath()
tmp_test_path = getTestTmpPath()


def test_calling_processWitnessPoints():
    """
    Check calling the processWitnessPoints function.
    """

    #
    # Paths
    #
    test_case = os.path.join(tmp_test_path, "call_processWitnessPoints")
    if not os.path.isdir(test_case):
        os.makedirs(test_case)
    json_path_glob = os.path.join(test_case, "globalConfig.json")
    json_path_loc = os.path.join(test_case, "KolmogorovAtWitnessInputFile.json")

    #
    # Copy data
    #
    for fn in ["cam.chm.wit", "cam.nsi.wit", "cam.tem.wit", "witness.dat"]:
        shutil.copy(
            os.path.join(test_path, "data", "camb_example", fn),
            os.path.join(test_case, fn),
        )

    #
    # Set case data
    #
    data = {
        "CasePath": test_case,
        "CaseName": "cam",
        "Modules": ["nsi", "tem", "chm"],
        "PropertyConstPressure": 101325.0,
        "PropertyDensityMethod": "EoS_H_from_SCONC_to_NASA_poly",
        "PropertyLookupEnthalpyScalingTable": os.path.join(
            test_path, "data", "camb_example", "tab_turbulent_scaling_H.dat"
        ),
        "PropertyLookupTable": os.path.join(
            test_path, "data", "camb_example", "tab_turbulentProp_table.dat"
        ),
        "PropertyMethod": "tabulated",
        "Tasks": ["kolmogorov", "cutoff", "integral", "spectrum", "chemical_scales"],
        "WitnessDefinitionPath": os.path.join(test_case, "witness.dat"),
        "WitnessFilterXcrit": "equal",
        "WitnessFilterXvalue": 0.1752,
        "WitnessFilterYcrit": "range",
        "WitnessFilterYrange": [0.01, 0.012],
        "WitnessFilterZcrit": "any",
        "ChemicalScalesList": ["S_L", "delta_diff"],
        "ChemicalScalesMethod": "constant",
        "ChemicalScalesConst_S_L": 0.3,
        "ChemicalScalesConst_delta_diff": 0.1e-3,
        "spectrumProperties": {
            "processed": [],
            "raw": {"chm": ["SCO01"], "nsi": ["VELOX", "VELOY", "VELOZ"], "tem": []},
        },
    }

    with open(json_path_loc, "w") as f:
        json.dump(data, f)

    if ("BSC_MACHINE" in os.environ) and ("mn4" in os.getenv("BSC_MACHINE")):
        data = {"AlyaPath": "/gpfs/projects/bsc21/bsc21304/git/master"}
    else:
        data = {"AlyaPath": "/usr/local/bin/"}
    with open(json_path_glob, "w") as f:
        json.dump(data, f)

    lc = LocalConfig(json_path_glob=json_path_glob, json_path_loc=json_path_loc)

    #
    # Read extra data
    #
    lc.getData()

    #
    # Chack if required properties are identified correctly
    #
    assert lc.locdata["RequiredProperties"]["general"] == [
        "time",
        "viscosity",
        "density",
        "sgs_viscosity",
    ]
    assert lc.locdata["RequiredProperties"]["nsi"] == [
        "S11",
        "S22",
        "S12",
        "S33",
        "S13",
        "S23",
        "TURBU",
        "VELOX",
        "VELOY",
        "VELOZ",
    ]
    assert lc.locdata["RequiredProperties"]["tem"] == []
    assert lc.locdata["RequiredProperties"]["chm"] == [
        "SCO01",
        "SCO02",
        "SCO03",
        "SCO04",
    ]

    #
    # Check if header info is correctly extracted
    #
    assert lc.locdata["ModulesWitnessHeaderData"]["chm"]["VarCols"]["CON01"] == 2
    assert lc.locdata["ModulesWitnessHeaderData"]["chm"]["VarCols"]["SCO01"] == 10

    assert lc.locdata["ModulesWitnessHeaderData"]["chm"]["nLines"] == 23
    assert lc.locdata["ModulesWitnessHeaderData"]["chm"]["nVar"] == 18
    assert lc.locdata["ModulesWitnessHeaderData"]["chm"]["nEntity"] == 130

    #
    # Check if correct number of witness points is being processed
    #
    indeces = lc.getWitnessIndcesFromLocData()
    assert len(indeces) == 2

    #
    # Call extraction
    #
    extractWitnessPointData(lc, overwrite=True)

    #
    # Call processing function
    #
    processWitnessPoints(lc)

    #
    # Existence of output files
    #
    assert os.path.isfile(os.path.join(test_case, "witness_data_entity84.json"))
    assert os.path.isfile(os.path.join(test_case, "witness_data_entity85.json"))

    #
    # Check data
    #
    with open(os.path.join(test_case, "witness_data_entity84.json"), "r") as f:
        data = json.load(f)

    assert np.abs(data["Re"]["dissipationRate"]["eps"] - 378.47979496519935) < 1e-5
    assert np.abs(data["Fv"]["dissipationRate"]["eps"] - 332.84296596194037) < 1e-5
    assert np.abs(data["Fv"]["dissipationRate"]["eps_res"] - 188.61338943188423) < 1e-5
    assert np.abs(data["Fv"]["dissipationRate"]["eps_sgs"] - 144.22957653005614) < 1e-5

    assert np.abs(data["Re"]["processed"]["nu"] - 0.00030766106171613473) < 1e-9
    assert np.abs(data["Fv"]["processed"]["nu"] - 0.0003051435042384822) < 1e-9

    assert np.abs(data["Re"]["processed"]["rho"] - 0.19925512981002355) < 1e-6
    assert np.abs(data["Fv"]["processed"]["rho"] - 0.20026586094394777) < 1e-6

    assert np.abs(data["Re"]["processed"]["mu"] - 6.0801408547724245e-05) < 1e-9
    assert np.abs(data["Fv"]["processed"]["mu"] - 6.0605781985770266e-05) < 1e-9

    assert np.abs(data["Re"]["dissipationRate"]["Sij"][0] - 21.884689755500826) < 1e-6
    assert np.abs(data["Fv"]["dissipationRate"]["Sij"][0] - 16.923618827797398) < 1e-6

    assert np.abs(data["kolmogorov"]["l_K"] - 0.0005405280688320805) < 1e-9
    assert np.abs(data["kolmogorov"]["t_K"] - 0.0009574858685734795) < 1e-9
    assert np.abs(data["kolmogorov"]["v_K"] - 0.5645285080158854) < 1e-9

    assert np.abs(data["kolmogorov"]["l_K_inco"] - 0.0005266762393346074) < 1e-9
    assert np.abs(data["kolmogorov"]["t_K_inco"] - 0.0009016021056820578) < 1e-9
    assert np.abs(data["kolmogorov"]["v_K_inco"] - 0.5841559552882579) < 1e-9

    assert np.abs(data["Re"]["integral"]["veloc"][0] - -2.490476946328124) < 1e-5
    assert np.abs(data["Re"]["integral"]["veloc"][1] - -1.39742818950711) < 1e-5
    assert np.abs(data["Re"]["integral"]["veloc"][2] - -4.929804108834905) < 1e-5

    assert np.abs(data["integral"]["l_t"] - 0.007750938318300714) < 1e-9
    assert np.abs(data["integral"]["t_t"] - 0.005651404495544539) < 1e-9
    assert np.abs(data["integral"]["v_t"] - 1.371506556363362) < 1e-9
    assert np.abs(data["integral"]["Re_t"] - 34.837584853876805) < 1e-9

    #
    # Delete intermediate data
    #
    assert os.path.isfile(os.path.join(test_case, "tmp_cam.chm.wit_entity84_varSCO01"))
    deleteIntermediateWitnessPointData(lc)
    assert not os.path.isfile(
        os.path.join(test_case, "tmp_cam.chm.wit_entity84_varSCO01")
    )

    #
    # Merge results
    #
    mergeWitnessPointData(lc)
    assert os.path.isfile(os.path.join(test_case, "witness_data_merged.json"))

    with open(os.path.join(test_case, "witness_data_merged.json"), "r") as f:
        data = json.load(f)

    assert np.abs(np.nanmin(data["integral"]["Re_t"]) - 18.606805177216035) < 1e-9
    assert np.abs(np.nanmax(data["integral"]["Re_t"]) - 34.837584853876805) < 1e-9


def test_calling_extractWitnessPointData():
    """
    Check calling the extractWitnessPointData function.
    """

    #
    # Paths
    #
    test_case = os.path.join(tmp_test_path, "call_extractWitnessPointData")
    if not os.path.isdir(test_case):
        os.makedirs(test_case)
    json_path_glob = os.path.join(test_case, "globalConfig.json")
    json_path_loc = os.path.join(test_case, "KolmogorovAtWitnessInputFile.json")

    #
    # Copy data
    #
    for fn in ["cam.chm.wit", "cam.nsi.wit", "cam.tem.wit", "witness.dat"]:
        shutil.copy(
            os.path.join(test_path, "data", "camb_example", fn),
            os.path.join(test_case, fn),
        )

    #
    # Set case data
    #
    data = {
        "CasePath": test_case,
        "CaseName": "cam",
        "Modules": ["nsi", "tem", "chm"],
        "PropertyConstPressure": 101325.0,
        "PropertyDensityMethod": "EoS_T_from_temper",
        "PropertyLookupTable": os.path.join(
            test_path, "data", "camb_example", "tab_turbulentProp_table.dat"
        ),
        "PropertyMethod": "tabulated",
        "PropertyControlVariableMethod": "CONCE",
        "PropertyControlVariableSconcIndexToConceIndex": [1, 2, 3, 4],
        "Tasks": ["kolmogorov"],
        "WitnessDefinitionPath": os.path.join(test_case, "witness.dat"),
        "WitnessFilterXcrit": "equal",
        "WitnessFilterXvalue": 0.1752,
        "WitnessFilterYcrit": "range",
        "WitnessFilterYrange": [0.01, 0.012],
        "WitnessFilterZcrit": "any",
    }

    with open(json_path_loc, "w") as f:
        json.dump(data, f)

    if ("BSC_MACHINE" in os.environ) and ("mn4" in os.getenv("BSC_MACHINE")):
        data = {"AlyaPath": "/gpfs/projects/bsc21/bsc21304/git/master"}
    else:
        data = {"AlyaPath": "/usr/local/bin/"}
    with open(json_path_glob, "w") as f:
        json.dump(data, f)

    lc = LocalConfig(json_path_glob=json_path_glob, json_path_loc=json_path_loc)

    #
    # Read extra data
    #
    lc.getData()

    #
    # Chack if required properties are identified correctly
    #
    assert lc.locdata["RequiredProperties"]["general"] == [
        "time",
        "viscosity",
        "density",
        "sgs_viscosity",
    ]
    assert lc.locdata["RequiredProperties"]["nsi"] == [
        "S11",
        "S22",
        "S12",
        "S33",
        "S13",
        "S23",
        "TURBU",
    ]
    assert lc.locdata["RequiredProperties"]["tem"] == ["TEMPE"]
    assert lc.locdata["RequiredProperties"]["chm"] == [
        "CON01",
        "CON02",
        "CON03",
        "CON04",
    ]

    #
    # Check if header info is correctly extracted
    #
    assert lc.locdata["ModulesWitnessHeaderData"]["chm"]["VarCols"]["CON01"] == 2
    assert lc.locdata["ModulesWitnessHeaderData"]["chm"]["VarCols"]["SCO01"] == 10

    assert lc.locdata["ModulesWitnessHeaderData"]["chm"]["nLines"] == 23
    assert lc.locdata["ModulesWitnessHeaderData"]["chm"]["nVar"] == 18
    assert lc.locdata["ModulesWitnessHeaderData"]["chm"]["nEntity"] == 130

    #
    # Check if correct number of witness points is being processed
    #
    indeces = lc.getWitnessIndcesFromLocData()
    assert len(indeces) == 2

    #
    # Call extraction
    #
    extractWitnessPointData(lc, overwrite=True)

    #
    # Existence of output files
    #
    assert os.path.isfile(os.path.join(test_case, "tmp_cam.chm.wit_entity84_varCON01"))
    assert os.path.isfile(os.path.join(test_case, "tmp_cam.chm.wit_entity84_varCON02"))
    assert os.path.isfile(os.path.join(test_case, "tmp_cam.chm.wit_entity84_varCON03"))
    assert os.path.isfile(os.path.join(test_case, "tmp_cam.chm.wit_entity84_varCON04"))
