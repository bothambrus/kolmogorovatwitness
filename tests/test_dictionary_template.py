#################################################################################
#   KolmogorovAtWitness                                                         #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import os
import json
import numpy as np

from .context import kolmogorovatwitness, getTestPath, getTestTmpPath
from kolmogorovatwitness.dictionary_template import (
    getExpandedDictionary,
    insertToExpandedDictionary,
)


dict_template = {
    "a": "a",
    "b": ["m", "n", "o"],
    "c": [3, 4, 5],
    "d": ["a", "1", 3, 5],
    "e": np.array([1, 2, 3, 4, 5, 6], dtype=int),
    "f": np.linspace(0, 1, 4),
    "g": np.linspace(0, 4, 12).reshape([3, 4]),
    "h": np.linspace(0, 7, 24).reshape([2, 3, 4]),
    "i": 3.14,
    "j": [],
    "k": {"l": [1, 2, 3]},
}


def test_getExpandedDictionary():
    """
    Test using a tamplate dictioanry to create one with arrays
    """
    exp = {}
    exp = getExpandedDictionary(exp, dict_template, 12)

    print(exp)

    assert len(exp["a"]) == 12
    assert np.prod(exp["b"].size) == 36
    assert np.prod(exp["c"].size) == 36
    assert np.abs(np.min(exp["c"]) - 0.0) < 1e-6
    assert np.abs(np.max(exp["c"]) - 0.0) < 1e-6
    assert np.prod(exp["d"].size) == 48
    assert np.prod(exp["e"].size) == 72
    assert np.abs(np.min(exp["e"]) - 0.0) < 1e-6
    assert np.abs(np.max(exp["e"]) - 0.0) < 1e-6
    assert np.prod(exp["f"].size) == 48
    assert np.all(np.isnan(exp["f"]))
    assert np.prod(exp["g"].size) == 144
    assert np.all(np.isnan(exp["g"]))
    assert np.prod(exp["h"].size) == 288
    assert np.all(np.isnan(exp["h"]))
    assert len(exp["i"]) == 12
    assert np.all(np.isnan(exp["i"]))
    assert len(exp["j"]) == 12
    assert np.all(np.isnan(exp["j"]))
    assert np.prod(exp["k"]["l"].size) == 36


def test_insertToExpandedDictionary():
    """
    Test using a tamplate dictioanry to create one with arrays
    """
    exp = {}
    exp = getExpandedDictionary(exp, dict_template, 12)

    print(exp)

    assert len(exp["a"]) == 12
    assert np.prod(exp["b"].size) == 36
    assert np.prod(exp["c"].size) == 36
    assert np.abs(np.min(exp["c"]) - 0.0) < 1e-6
    assert np.abs(np.max(exp["c"]) - 0.0) < 1e-6
    assert np.prod(exp["d"].size) == 48
    assert np.prod(exp["e"].size) == 72
    assert np.abs(np.min(exp["e"]) - 0.0) < 1e-6
    assert np.abs(np.max(exp["e"]) - 0.0) < 1e-6
    assert np.prod(exp["f"].size) == 48
    assert np.all(np.isnan(exp["f"]))
    assert np.prod(exp["g"].size) == 144
    assert np.all(np.isnan(exp["g"]))
    assert np.prod(exp["h"].size) == 288
    assert np.all(np.isnan(exp["h"]))
    assert len(exp["i"]) == 12
    assert np.all(np.isnan(exp["i"]))
    assert len(exp["j"]) == 12
    assert np.all(np.isnan(exp["j"]))
    assert np.prod(exp["k"]["l"].size) == 36

    exp = insertToExpandedDictionary(exp, dict_template, 8)

    assert np.abs(np.min(exp["c"]) - 0.0) < 1e-6
    assert np.abs(np.max(exp["c"]) - 5.0) < 1e-6
    assert np.abs(np.min(exp["e"]) - 0.0) < 1e-6
    assert np.abs(np.max(exp["e"]) - 6.0) < 1e-6
    assert not np.all(np.isnan(exp["f"]))
    assert np.abs(np.nanmin(exp["f"]) - 0.0) < 1e-6
    assert np.abs(np.nanmax(exp["f"]) - 1.0) < 1e-6
    assert not np.all(np.isnan(exp["g"]))
    assert np.abs(np.nanmin(exp["g"]) - 0.0) < 1e-6
    assert np.abs(np.nanmax(exp["g"]) - 4.0) < 1e-6
    assert not np.all(np.isnan(exp["h"]))
    assert np.abs(np.nanmin(exp["h"]) - 0.0) < 1e-6
    assert np.abs(np.nanmax(exp["h"]) - 7.0) < 1e-6
    assert np.abs(np.nanmin(exp["i"]) - 3.14) < 1e-6
    assert np.abs(np.nanmax(exp["i"]) - 3.14) < 1e-6
    assert np.abs(np.nanmin(exp["k"]["l"]) - 0) < 1e-6
    assert np.abs(np.nanmax(exp["k"]["l"]) - 3) < 1e-6
