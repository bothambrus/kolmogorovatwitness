#################################################################################
#   KolmogorovAtWitness                                                         #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import os
import json
import numpy as np

from .context import kolmogorovatwitness, getTestPath, getTestTmpPath
from kolmogorovatwitness.nasapoly import T2hcp, h2Tcp


cpcoeffsLT = np.array(
    [
        9.80027581e02,
        1.39145377e-01,
        -3.06020284e-04,
        6.98175695e-07,
        -3.60195091e-10,
        -2.98188943e05,
    ]
)
cpcoeffsHT = np.array(
    [
        8.65027691e02,
        4.28585162e-01,
        -1.75288492e-04,
        3.56573120e-08,
        -2.84807148e-12,
        -2.77326162e05,
    ]
)

cpLT = np.array(
    [
        cpcoeffsLT,
        cpcoeffsLT,
        cpcoeffsLT,
    ]
)
cpHT = np.array(
    [
        cpcoeffsHT,
        cpcoeffsHT,
        cpcoeffsHT,
    ]
)


def test_T2H():
    """
    Test calculating enthalpy and specific heat from teperature
    """
    h, cp = T2hcp(cpcoeffsLT, cpcoeffsHT, 300.0)
    assert np.abs(h - 565.4416771489778) < 1e-8
    assert np.abs(cp - 1010.1625320679001) < 1e-8

    h, cp = T2hcp(cpcoeffsLT, cpcoeffsHT, 700.0)
    assert np.abs(h - 416733.06023116765) < 1e-8
    assert np.abs(cp - 1080.4708277759) < 1e-8

    h, cp = T2hcp(cpcoeffsLT, cpcoeffsHT, 1200.0)
    assert np.abs(h - 985389.5803437773) < 1e-8
    assert np.abs(cp - 1182.624531035072) < 1e-8


def test_H2T():
    """
    Test calculating temperature and specific heat from enthalpy
    """
    T, cp = h2Tcp(cpcoeffsLT, cpcoeffsHT, 565.4416771489778)
    assert np.abs(T - 300.0) < 1e-8
    assert np.abs(cp - 1010.1625320679001) < 1e-8

    T, cp = h2Tcp(cpcoeffsLT, cpcoeffsHT, 416733.06023116765)
    assert np.abs(T - 700.0) < 1e-8
    assert np.abs(cp - 1080.4708277759) < 1e-8

    T, cp = h2Tcp(cpcoeffsLT, cpcoeffsHT, 985389.5803437773)
    assert np.abs(T - 1200.0) < 1e-8
    assert np.abs(cp - 1182.624531035072) < 1e-8


def test_T2H_array():
    """
    Test calculating enthalpy and specific heat from teperature array
    """

    T = np.array([300.0, 700.0, 1200.0])

    h, cp = T2hcp(cpLT, cpHT, T)

    assert np.abs(h[0] - 565.4416771489778) < 1e-8
    assert np.abs(cp[0] - 1010.1625320679001) < 1e-8

    assert np.abs(h[1] - 416733.06023116765) < 1e-8
    assert np.abs(cp[1] - 1080.4708277759) < 1e-8

    assert np.abs(h[2] - 985389.5803437773) < 1e-8
    assert np.abs(cp[2] - 1182.624531035072) < 1e-8


def test_H2T_array():
    """
    Test calculating temperature and specific heat from enthalpy array
    """

    h = np.array([565.4416771489778, 416733.06023116765, 985389.5803437773])
    T, cp = h2Tcp(cpLT, cpHT, h)

    assert np.abs(T[0] - 300.0) < 1e-8
    assert np.abs(cp[0] - 1010.1625320679001) < 1e-8
    assert np.abs(T[1] - 700.0) < 1e-8
    assert np.abs(cp[1] - 1080.4708277759) < 1e-8
    assert np.abs(T[2] - 1200.0) < 1e-8
    assert np.abs(cp[2] - 1182.624531035072) < 1e-8
