# KolmogorovAtWitness

This python tool includes scripts to get the relevant turbulent scales from witness point data given by Alya. 

For details on installation and usage, see the [wiki](https://gitlab.com/bothambrus/kolmogorovatwitness/-/wikis/home).

