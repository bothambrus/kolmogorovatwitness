#################################################################################
#   KolmogorovAtWitness                                                         #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import os

import numpy as np

import kolmogorovatwitness.context  # isort:skip
from lectio.input import userInputPrompt  # isort:skip


def inputCaseData(lc):
    """
    Ask for user input regarding case data.
    """
    print("\n#############################")
    print("####      CASE DATA      ####")
    print("#############################")
    #
    # Input Case path
    #
    key = "CasePath"
    message = "Path to processed case:"
    if key in lc.locdata.keys():
        default = lc.locdata[key]
    else:
        default = "."
    lc.locdata[key] = os.path.abspath(userInputPrompt(message, default=default))

    #
    # Input Case name
    #
    key = "CaseName"
    message = "Case name:"
    if key in lc.locdata.keys():
        default = lc.locdata[key]
    else:
        default = "c"
    lc.locdata[key] = userInputPrompt(message, default=default)

    return lc


def inputWitnessPointData(lc):
    """
    Ask for user input regarding witness point data.
    """
    print("\n##############################")
    print("####  WITNESS POINT DATA  ####")
    print("##############################")
    #
    # Witness point definition path
    #
    key = "WitnessDefinitionPath"
    message = "Path to file defining the witness points:"
    if key in lc.locdata.keys():
        default = lc.locdata[key]
    else:
        default = "./witness.dat"
    lc.locdata[key] = os.path.abspath(userInputPrompt(message, default=default))

    #
    # Filter witness points
    #
    for coord in ["x", "y", "z"]:
        key = "WitnessFilter{}crit".format(coord.upper())
        message = "Filtering criteria of {} coordinate of witness points:".format(coord)

        options = ["any", "equal", "range"]
        optDescr = [
            "{} coordinate is not constrained".format(coord),
            "{} coordinate must be equal to a given value".format(coord),
            "{} coordinate must be within a given range of".format(coord),
        ]

        if key in lc.locdata.keys():
            default = lc.locdata[key]
        else:
            default = options[0]
        lc.locdata[key] = userInputPrompt(
            message, default=default, options=options, optDescr=optDescr
        )

        #
        # Get equal criterion
        #
        if lc.locdata["WitnessFilter{}crit".format(coord.upper())] == "equal":
            key = "WitnessFilter{}value".format(coord.upper())
            message = "Fixed {} value:".format(coord)

            if key in lc.locdata.keys():
                default = lc.locdata[key]
            else:
                default = 0.0
            lc.locdata[key] = userInputPrompt(message, typ="float", default=default)

        elif lc.locdata["WitnessFilter{}crit".format(coord.upper())] == "range":
            key = "WitnessFilter{}range".format(coord.upper())
            message = "Range of {} given by minimum and maximum:".format(coord)

            if key in lc.locdata.keys():
                default = lc.locdata[key]
            else:
                default = [0.0, 1.0]
            lc.locdata[key] = userInputPrompt(message, typ="floatList", default=default)

    #
    # Mesh size at location
    #
    key = "FilterSizeMethod"
    message = "Method to determine LES filter size at the witness points:"

    options = ["constant", "list_file"]
    optDescr = [
        "Use constant filter size at all witness points",
        "Read filter size from a text file",
    ]

    if key in lc.locdata.keys():
        default = lc.locdata[key]
    else:
        default = "constant"
    lc.locdata[key] = userInputPrompt(
        message, default=default, options=options, optDescr=optDescr
    ).strip()

    #
    # Mesh size from list
    #
    if lc.locdata["FilterSizeMethod"] == "list_file":
        key = "FilterSizePath"
        message = "Path to file defining the LES filter size at the witness points:"
        if key in lc.locdata.keys():
            default = lc.locdata[key]
        else:
            default = "./witnessFilterSize.dat"
        lc.locdata[key] = os.path.abspath(userInputPrompt(message, default=default))

    #
    # Mesh size is considered constant
    #
    elif lc.locdata["FilterSizeMethod"] == "constant":
        #
        # Read method
        #
        message = "Give filter width manually, or calculate from regular elements?"

        options = ["manual", "hex", "tet"]
        optDescr = [
            "Manual filter width",
            "Calculate from regular hexahedron",
            "Calculate from regular tetrahedron",
        ]
        default = "manual"
        task = userInputPrompt(
            message, default=default, options=options, optDescr=optDescr
        )

        #
        # Read size
        #
        if "FilterSizeConstant" in lc.locdata.keys():
            default = lc.locdata["FilterSizeConstant"]
        else:
            default = 1e-3
        size = userInputPrompt("Size:", typ="float", default=default)

        #
        # Calculate
        #
        if task == "manual":
            lc.locdata["FilterSizeConstant"] = size
        elif task == "hex":
            lc.locdata["FilterSizeConstant"] = size
        elif task == "tet":
            vol = np.sqrt(2.0) / 12.0 * size**3
            lc.locdata["FilterSizeConstant"] = vol ** (1.0 / 3.0)
        print("Filter size: {} m".format(lc.locdata["FilterSizeConstant"]))

    return lc


def inputTaskData(lc):
    """
    Ask for user input regarding tasks to perform.
    """
    print("\n#############################")
    print("####      TASK DATA      ####")
    print("#############################")

    #
    # Processed time frame
    #
    key = "TimeInitial"
    message = "Initial time instance to be analized:"
    if key in lc.locdata.keys():
        default = lc.locdata[key]
    else:
        default = 0.0
    lc.locdata[key] = userInputPrompt(message, typ="float", default=default)

    key = "TimeFinal"
    message = "Final time instance to be analized:"
    if key in lc.locdata.keys():
        default = lc.locdata[key]
    else:
        default = 1.0e9
    lc.locdata[key] = userInputPrompt(message, typ="float", default=default)

    #
    # Alya modules to process
    #
    key = "Modules"
    message = "List of Alya modules to process:"

    options = ["nsi", "tem", "chm"]
    optDescr = [
        "Nastin",
        "Temper",
        "Chemic",
    ]
    if key in lc.locdata.keys():
        default = lc.locdata[key]
    else:
        default = ["nsi", "tem", "chm"]
    lc.locdata[key] = userInputPrompt(
        message, typ="strList", default=default, options=options, optDescr=optDescr
    )

    #
    # Tasks
    #
    key = "Tasks"
    message = "List of tasks that KolmogorovAtWitness executes:"

    options = [
        "kolmogorov",
        "cutoff",
        "integral",
        "favre_error",
        "spectrum",
        "chemical_scales",
    ]
    optDescr = [
        "Kolmogorov length scale at witness points",
        "Scales at cut-off",
        "Integral lenght scale",
        "Error made by ensemble averaging the Favre-filtered velocities",
        "Temporal power spectral density (PSD) of selected properties",
        "Chemical lenght and time scales",
    ]
    if key in lc.locdata.keys():
        default = lc.locdata[key]
    else:
        default = [
            "kolmogorov",
            "cutoff",
            "integral",
            "favre_error",
            "spectrum",
            "chemical_scales",
        ]
    lc.locdata[key] = userInputPrompt(
        message, typ="strList", default=default, options=options, optDescr=optDescr
    )

    #
    # Treatment of temporal data
    #
    key = "SaveTemporalData"
    message = "Do you want to save the full temporal data in a json file \n(PLEASE DO NOT DO THIS FOR LARGE DATA SETS!):"

    if key in lc.locdata.keys():
        default = lc.locdata[key]
    else:
        default = False
    lc.locdata[key] = userInputPrompt(message, typ="bool", default=default)

    #
    # Inputs about spectrum
    #
    if "spectrum" in lc.locdata["Tasks"]:
        #
        # Do Lomb-Scargle?
        #
        key = "spectrumDoLombscargle"
        message = "Do you want to compute the spectrum using the Lomb-Scargle variable time step method?:"

        if key in lc.locdata.keys():
            default = lc.locdata[key]
        else:
            default = False
        lc.locdata[key] = userInputPrompt(message, typ="bool", default=default)

        #
        # Properties
        #
        key = "spectrumProperties"
        if not key in lc.locdata.keys():
            lc.locdata[key] = {}

        #
        # Raw data from modules
        #
        if not "raw" in lc.locdata[key].keys():
            lc.locdata[key]["raw"] = {}

        for mod in ["nsi", "tem", "chm"]:
            message = "Power spectral density of variables from case.{}.wit:".format(
                mod
            )

            if mod == "nsi":
                options = [
                    "VELOX",
                    "VELOY",
                    "VELOZ",
                    "PRESS",
                    "S11",
                    "S22",
                    "S33",
                    "S12",
                    "S23",
                    "TURBU",
                ]
            elif mod == "tem":
                options = ["TEMPE", "ENTHA"]
            elif mod == "chm":
                options = [
                    "CON01",
                    "CON02",
                    "CON03",
                    "CON04",
                    "XZR",
                    "XZS",
                    "XYR",
                    "XYS",
                    "DENSI",
                ]

            if mod in lc.locdata[key]["raw"].keys():
                default = lc.locdata[key]["raw"][mod]
            else:
                default = [options[0]]

            lc.locdata[key]["raw"][mod] = userInputPrompt(
                message,
                typ="strList",
                default=default,
                options=options,
                compactOptions=True,
            )

        #
        # Processed
        #
        message = "Power spectral density of processed quantities:"
        options = ["rho", "mu", "nu", "mu_sgs", "nu_sgs"]
        if "processed" in lc.locdata[key].keys():
            default = lc.locdata[key]["processed"]
        else:
            default = [options[0]]
        lc.locdata[key]["processed"] = userInputPrompt(
            message,
            typ="strList",
            default=default,
            options=options,
            compactOptions=True,
        )

    #
    # Inputs about favre_error
    #
    if "favre_error" in lc.locdata["Tasks"]:
        #
        # Define path to table, that has the Favre-filtered density
        #
        key = "FavreDensityLookupTable"
        message = (
            "Path to lookup table that contains the Favre-filtered density ('rho_Fv')"
        )
        if key in lc.locdata.keys():
            default = lc.locdata[key]
        else:
            default = "./tab_turbulentPost_table.dat"
        lc.locdata[key] = os.path.abspath(userInputPrompt(message, default=default))

    return lc


def inputMaterialPropertyData(lc):
    """
    Ask for user input regarding material properties.
    """
    print("\n##################################")
    print("####  MATERIAL PROPERTY DATA  ####")
    print("##################################")
    #
    # Material properties
    #
    key = "PropertyMethod"
    message = "Method to assess local fluid properties:"

    options = ["constant", "tabulated"]
    optDescr = [
        "Assume constant properties",
        "Look up tabulated properties",
    ]

    if key in lc.locdata.keys():
        default = lc.locdata[key]
    else:
        default = "tabulated"
    lc.locdata[key] = userInputPrompt(
        message, default=default, options=options, optDescr=optDescr
    )

    #
    # Constant properties
    #
    if lc.locdata["PropertyMethod"] == "constant":
        #
        # Density
        #
        key = "PropertyConstDensity"
        message = "Constant fluid density:"

        if key in lc.locdata.keys():
            default = lc.locdata[key]
        else:
            default = 1.0
        lc.locdata[key] = userInputPrompt(message, typ="float", default=default)

        #
        # Viscosity
        #
        key = "PropertyConstKinematicViscosity"
        message = "Constant fluid kinematic viscosity:"

        if key in lc.locdata.keys():
            default = lc.locdata[key]
        else:
            default = 1.0e-5
        lc.locdata[key] = userInputPrompt(message, typ="float", default=default)

    #
    # Tabulated properties
    #
    elif lc.locdata["PropertyMethod"] == "tabulated":
        #
        # Control variable method for property lookup
        #
        key = "PropertyControlVariableMethod"
        message = "Method to obtain control variables for lookup:"

        options = ["SCONC", "CONCE_perfectly_premixed"]
        optDescr = [
            "Scaled control variables are post-processed on the witness points directly.",
            "Get unscaled control variables from chemic, and scale them.",
        ]

        if key in lc.locdata.keys():
            default = lc.locdata[key]
        else:
            default = "SCONC"
        lc.locdata[key] = userInputPrompt(
            message, default=default, options=options, optDescr=optDescr
        )

        if lc.locdata["PropertyControlVariableMethod"][:5] == "CONCE":
            #
            # Correspondance of SCONC and CONCE
            #
            key = "PropertyControlVariableSconcIndexToConceIndex"
            message = "CONCE indeces in order of corresponding SCONC indeces:"

            if key in lc.locdata.keys():
                default = lc.locdata[key]
            else:
                default = [1, 2]

            lc.locdata[key] = userInputPrompt(message, default=default, typ="intList")

        #
        # Path of lookup table
        #
        key = "PropertyLookupTable"
        message = "Path to proprty lookup table to use with SCONC values of .chm.wit:"
        if key in lc.locdata.keys():
            default = lc.locdata[key]
        else:
            default = "./tab_turbulentProp_table.dat"
        lc.locdata[key] = os.path.abspath(userInputPrompt(message, default=default))

        #
        # Do we need enthalpy scaling
        #
        key = "PropertyDensityMethod"
        message = "Method to obtain instantaneous fluid density:"

        options = [
            "EoS_T_from_temper",
            "EoS_H_from_temper_to_NASA_poly",
            "EoS_H_from_SCONC_to_NASA_poly",
            "from_chemic",
        ]
        optDescr = [
            "Use the ideal gas law with temperature from .tem.wit.",
            "Use the ideal gas law with temperature calculated from the NASA polynomials using enthalpy from .tem.wit.",
            "Use the ideal gas law with temperature calculated from the NASA polynomials using enthalpy from the last SCONC using the enthalpy scaling table.",
            "Use density from the .chm.wit file.",
        ]

        if key in lc.locdata.keys():
            default = lc.locdata[key]
        else:
            default = "EoS_T_from_temper"
        lc.locdata[key] = userInputPrompt(
            message, default=default, options=options, optDescr=optDescr
        )

        #
        # Thermodynamic pressure
        #
        if lc.locdata["PropertyDensityMethod"] != "from_chemic":
            key = "PropertyConstPressure"
            message = "Constant thermodynamic pressure:"

            if key in lc.locdata.keys():
                default = lc.locdata[key]
            else:
                default = 101325.0
            lc.locdata[key] = userInputPrompt(message, typ="float", default=default)

        #
        # Path of enthalpy scaling table
        #
        if lc.locdata["PropertyDensityMethod"] == "EoS_H_from_SCONC_to_NASA_poly":
            key = "PropertyLookupEnthalpyScalingTable"
            message = "Path to enthalpy scaling table:"
            if key in lc.locdata.keys():
                default = lc.locdata[key]
            else:
                default = "./tab_turbulent_scaling_H.dat"
            lc.locdata[key] = os.path.abspath(userInputPrompt(message, default=default))

    return lc


def inputChemicalScalesData(lc):
    """
    Ask for user input regarding chemical scales.
    """
    print("\n###############################")
    print("####  CHEMICAL SCALES DATA  ####")
    print("################################")

    #
    # List of necessary properties
    #
    key = "ChemicalScalesList"
    message = "List of necessary chemical scales, supply either S_L or tau_c:"

    options = ["S_L", "delta_diff", "tau_c"]
    optDescr = [
        "Laminar flame speed",
        "Diffusive flame thickness",
        "Chemical time scale",
    ]

    if key in lc.locdata.keys():
        default = lc.locdata[key]
    else:
        default = ["S_L", "delta_diff"]

    lc.locdata[key] = userInputPrompt(
        message, typ="strList", default=default, options=options, optDescr=optDescr
    )

    #
    # Chemical scales
    #
    key = "ChemicalScalesMethod"
    message = "Method to assess chemical scales:"

    options = [
        "constant",
    ]
    optDescr = [
        "Assume constant chemical scales",
    ]

    if key in lc.locdata.keys():
        default = lc.locdata[key]
    else:
        default = "constant"
    lc.locdata[key] = userInputPrompt(
        message, default=default, options=options, optDescr=optDescr
    )

    #
    # Get constant chemical scales
    #
    if lc.locdata["ChemicalScalesMethod"] == "constant":
        #
        # Flame speed
        #
        for prop in lc.locdata["ChemicalScalesList"]:
            key = "ChemicalScalesConst_{}".format(prop)

            if prop == "S_L":
                name = "flame speed"
            elif prop == "delta_diff":
                name = "diffusive flame thickness"
            elif prop == "tau_c":
                name = "chemical time scale"
            else:
                name = prop
            message = "Constant reference {}:".format(name)

            if key in lc.locdata.keys():
                default = lc.locdata[key]
            else:
                if prop == "S_L":
                    default = 0.3
                elif prop == "delta_diff":
                    default = 0.1e-3
                elif prop == "tau_c":
                    default = 0.5e-3
                else:
                    default = 0.0

            lc.locdata[key] = userInputPrompt(message, typ="float", default=default)

    return lc
