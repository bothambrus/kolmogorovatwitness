#################################################################################
#   KolmogorovAtWitness                                                         #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import os
import shutil
import subprocess
import sys

import numpy as np

import kolmogorovatwitness.context


def extractSinglePointFileWithAlyaUtils(
    path,
    AlyaPath,
    timeSeriesFile,
    entityIndex,
    column,
    newName,
    startTime=0,
    endTime=10000,
    rank=0,
    overwrite=False,
):
    #
    # Check if file already exists
    #
    newPath = os.path.join(path, newName)
    if (os.path.isfile(newPath)) and (not overwrite):
        return

    #
    # Make temporary directory
    #
    tempdir = os.path.join(path, "tmp_{}".format(rank))
    if os.path.isdir(tempdir):
        shutil.rmtree(tempdir, ignore_errors=True)
    os.makedirs(tempdir)

    #
    # Create symbolic link to time series file inside temporary directory
    #
    src = os.path.join(path, os.path.basename(timeSeriesFile))
    dst = os.path.join(tempdir, os.path.basename(timeSeriesFile))
    os.symlink(src, dst)

    success = False
    try:
        #
        # Script name
        #
        script = os.path.join(AlyaPath, "Utils", "user", "alya-time-set")

        #
        # Try extraction
        #
        command = ""
        command += "cd {} && ".format(tempdir)
        command += "{} {} {} {} {:7d} {:7d}".format(
            script, timeSeriesFile, entityIndex, column, startTime, endTime
        )

        extractionOutput = subprocess.check_output(
            command, shell=True, stderr=subprocess.STDOUT
        ).decode()

        success = True

    except subprocess.CalledProcessError as e:
        #
        # Print failure
        #
        print(
            "Data extraction for entity {} failed!: {}".format(
                entityIndex, e.output.decode()
            )
        )
        print(e.output.decode())
        success = False

    #
    # Copy files
    #
    if success:
        shutil.move(
            os.path.join(tempdir, "alya-time-set.out"), os.path.join(path, newName)
        )

    #
    # Delete temporal directory anyway
    #
    shutil.rmtree(tempdir, ignore_errors=True)


def extractHeaderInfo(
    path,
    timeSeriesFile,
):
    """
    Extract the information of the witness file header.
    """
    #
    # Initialize structure
    #
    headerInfo = {}
    headerInfo["Vars"] = []
    headerInfo["VarCols"] = {}
    headerInfo["nLines"] = 0
    headerInfo["nVar"] = 0
    headerInfo["nEntity"] = 0

    with open(os.path.join(path, timeSeriesFile), "r") as f:
        inHeader = False
        line = True

        while line:
            #
            # Read a new line
            #
            line = f.readline()
            data = line.split()
            headerInfo["nLines"] += 1

            #
            # Check if end of the header is reached
            #
            if len(data) >= 4 and data[1] == "NUMVARIABLES":
                inHeader = False
                headerInfo["nVar"] = int(data[3])

            #
            # Within header:
            #
            if (inHeader) and (len(data) >= 2):
                #
                # Special treatment of some variables
                #
                if "CONCE" in data[1]:
                    #
                    # Array of properties marked by the same key
                    #
                    iord = 1
                    for k in headerInfo["Vars"]:
                        if k[0:3] == "CON":
                            iord += 1
                    headerInfo["Vars"].append("CON{:02d}".format(iord))

                elif "SCONC" in data[1]:
                    #
                    # Array of properties marked by the same key
                    #
                    iord = 1
                    for k in headerInfo["Vars"]:
                        if k[0:3] == "SCO":
                            iord += 1
                    headerInfo["Vars"].append("SCO{:02d}".format(iord))

                elif "MOMEN" in data[1]:
                    #
                    # Array of properties marked by the same key
                    #
                    iord = 1
                    for k in headerInfo["Vars"]:
                        if k[0:4] == "MOME":
                            iord += 1
                    if iord == 1:
                        headerInfo["Vars"].append("MOMEX")
                    elif iord == 2:
                        headerInfo["Vars"].append("MOMEY")
                    elif iord == 3:
                        headerInfo["Vars"].append("MOMEZ")

                else:
                    #
                    # Anything else
                    #
                    headerInfo["Vars"].append(data[1])

                #
                # Column index (With +1 as Alya expects)
                #
                headerInfo["VarCols"][headerInfo["Vars"][-1]] = int(data[5])

            #
            # Detect the start of the Header
            #
            if len(data) >= 2 and data[1] == "HEADER":
                inHeader = True

            #
            # Detect number of sets
            #
            if len(data) >= 4 and data[1] == "NUMSETS":
                headerInfo["nEntity"] = int(data[3])

            #
            # Detect first occurance of "Iterations"
            #
            if len(data) >= 4 and data[1] == "Iterations":
                firstit = int(data[3])
                headerInfo["nLines"] -= 2
                line = False
                break

    return headerInfo


def getTimeArrayFromAlyaTimeSetFile(
    path,
    extractedTimeSeriesFile,
    t_min=0.0,
    t_max=np.inf,
):
    """
    Get the time array, and the list of unique indeces.
    """
    #
    # Get time
    #
    data = np.loadtxt(os.path.join(path, extractedTimeSeriesFile))
    time = data[:, 0]

    ####################################
    # MARK REGION WITH NON-UNIQUE TIME #
    ####################################
    dt = np.diff(time)

    #
    # Turning points
    #
    tps = np.where(dt < 0)[0]

    #
    # Find unique points
    #
    mask = np.ones_like(time, dtype=bool)
    indeces = range(len(time))

    #
    # Cut out overlaps
    #
    for tp in tps:
        overlap = np.where(np.logical_and(indeces <= tp, time >= time[tp + 1]))

        mask[overlap] = False

    #
    # Mark unwanted time ranges
    #
    mask = np.where(time < t_min, False, mask)
    mask = np.where(time > t_max, False, mask)

    #
    # Mask non-unique time and unvanted time ranges
    #
    time = time[mask]

    return time, mask


def getDataArrayFromAlyaTimeSetFile(
    path,
    extractedTimeSeriesFile,
    mask,
):
    """
    Get the data array.
    """
    #
    # Get data
    #
    data = np.loadtxt(os.path.join(path, extractedTimeSeriesFile))
    return data[:, 1][mask]
