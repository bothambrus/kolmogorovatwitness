#################################################################################
#   KolmogorovAtWitness                                                         #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################

import copy
import json
import os

import numpy as np
from matplotlib import style, ticker

import kolmogorovatwitness.context  # isort:skip
from lectio.input import userInputPrompt  # isort:skip


def getStyles():
    """
    Get styles for plotting.
    """
    style.use("default")
    font_style = dict(family="Sans", color="black", weight="normal", size=12)

    default_line_style = dict(
        marker=".",
        linestyle="-",
        fillstyle="none",
        color="black",
        linewidth=1.0,
        markersize=0,
    )

    return font_style, default_line_style


def setPltBackend(backend="default"):
    import matplotlib

    if backend != "default":
        matplotlib.use(backend)
        print("Setting matplotlib backend to {}".format(backend))
    import matplotlib.pyplot as plt

    return plt


def plotSpectrum(lc):
    """
    Ask for user input and plot the spectrum of a single point.
    """
    ##################
    # Ask for inputs #
    ##################
    message = "Point index:"

    options = ["{}-{}".format(1, lc.nwit)]
    optDescr = [
        "Index of witness point corresponding to file name: witness_spectrum_data_entity???.json",
    ]

    default = 1
    index = userInputPrompt(
        message, typ="int", default=default, options=options, optDescr=optDescr
    )

    #
    # Open file
    #
    fn = os.path.join(
        lc.locdata["CasePath"], "witness_spectrum_data_entity{}.json".format(index)
    )

    #
    # Data
    #
    with open(fn, "r") as f:
        data = json.load(f)

    #
    # Source
    #
    message = "Source of data:"

    options = list(data.keys())
    for k in [
        "equidist_dt",
        "sampling_freq",
        "frequencies",
        "coords",
        "welch_frequencies",
        "lombscargle_angular_frequencies",
        "lombscargle_frequencies",
    ]:
        if k in options:
            del options[options.index(k)]

    default = options[0]
    key_source = userInputPrompt(message, default=default, options=options)

    #
    # Use welch
    #
    message = "Type of spectrum to use:"
    default = "FFT"
    options = [
        "FFT",
        "welch",
        "lombscargle",
    ]
    optDescr = [
        "Fast Fourier Transform with the data resampled equidistantly in time",
        "Welch's method of smoothing the spectrum",
        "Lomb-Scargle periodogram",
    ]
    type_of_spectrum = userInputPrompt(
        message, typ="str", default=default, options=options, optDescr=optDescr
    ).strip()

    if type_of_spectrum == "FFT":
        prepend = ""
    else:
        prepend = "{}_".format(type_of_spectrum)

    #
    # Key
    #
    message = "Key of data:"

    options = list(data[key_source].keys())
    if type_of_spectrum == "FFT":
        for k in options[::-1]:
            if "welch_" in k:
                del options[options.index(k)]
            if "lombscargle_" in k:
                del options[options.index(k)]
    else:
        for k in options[::-1]:
            if not prepend in k:
                del options[options.index(k)]

    if len(options) > 0:
        default = options[0]
    else:
        print("There are no spectra to plot")
        return
    prop = userInputPrompt(message, default=default, options=options)

    #
    # Create figure
    #
    try:
        plt = setPltBackend()
        fig, (ax1) = plt.subplots(1, 1)
        font_style, default_line_style = getStyles()
        fig.set_size_inches(6, 4)
        fig.subplots_adjust(top=0.95, right=0.96, bottom=0.13, left=0.13)

    except Exception as e:
        print("Cannot plot, {}".format(e))
        return

    #
    # Data
    #
    freqs = np.array(data["{}frequencies".format(prepend)])
    ps = np.array(data[key_source][prop])
    idx = np.argsort(freqs)

    #
    # Plot
    #
    ax1.plot(freqs[idx], ps[idx])

    #
    # Format
    #
    ax1.set_xlabel("$f \ [Hz]$", fontdict=font_style)
    ax1.set_ylabel("$PSD_{{{}}}$".format(prop), fontdict=font_style)

    ax1.set_xscale("log")
    ax1.set_yscale("log")

    #
    # Save figure
    #
    fig.savefig(
        os.path.join(lc.locdata["CasePath"], "tmp_fig.eps"),
        format="eps",
        dpi=1200,
    )

    #
    # Show
    #
    plt.show()


def plotBorghiDiagram(lc=None, fig=None, ax1=None, saveFig=True, labelStyle="Peters"):
    """
    Prepare standar plot of Borghi diagram with all the
    points from witness_data_merged.json.
    """

    #
    # Create figure
    #
    try:
        font_style, default_line_style = getStyles()
        plt = setPltBackend()

        if fig is None or ax1 is None:
            fig, (ax1) = plt.subplots(1, 1)
            #
            # Format size
            #
            fig.set_size_inches(6, 4)
            fig.subplots_adjust(top=0.95, right=0.96, bottom=0.13, left=0.13)

            #
            # Format fig
            #
            ax1.set_aspect(4.0 / 5.0)
            aspect_ratio = ax1.get_aspect()

    except Exception as e:
        print("Cannot plot, {}".format(e))
        return

    #
    # Format standard Borghi diagram
    #
    ax1.set_xlabel("$\ell_t/\delta_{diff}$", fontdict=font_style)
    ax1.set_ylabel("$v^\prime/S_L$", fontdict=font_style)

    xmin = 0.1
    ymin = 0.1
    xmax = 1e4
    ymax = 1e3
    ax1.set_xlim([xmin, xmax])
    ax1.set_ylim([ymin, ymax])

    dlogx = np.log10(xmax) - np.log10(xmin)
    dlogy = np.log10(ymax) - np.log10(ymin)

    ax1.set_xscale("log")
    ax1.set_yscale("log")

    # ax1.xaxis.set_major_locator(ticker.MaxNLocator(6))
    # locs = ax1.get_xticks()
    # ax1.xaxis.set_major_locator(ticker.FixedLocator(locs))
    # ax1.set_xticklabels(locs, fontdict=font_style)
    # ax1.xaxis.set_major_formatter(ticker.StrMethodFormatter("{x:g}"))

    # ax1.yaxis.set_major_locator(ticker.MaxNLocator(5))
    # locs = ax1.get_yticks()
    # ax1.yaxis.set_major_locator(ticker.FixedLocator(locs))
    # ax1.set_yticklabels(locs, fontdict=font_style)
    # ax1.yaxis.set_major_formatter(ticker.StrMethodFormatter("{x:g}"))

    secondary_line_style = copy.deepcopy(default_line_style)
    secondary_line_style["linewidth"] = 0.5
    secondary_line_style["color"] = (0.5, 0.5, 0.5)

    secondary_font_style = copy.deepcopy(font_style)
    secondary_font_style["size"] = 9
    secondary_font_style["color"] = (0.5, 0.5, 0.5)

    #
    # Plot standard Borghi lines
    #
    x = np.logspace(-1, 4, 100)

    #
    # Unit Reynold
    #
    y = np.power(x, -1.0)
    ax1.plot(x, y, **default_line_style)
    ax1.text(
        0.2,
        2.3,
        "$Re_t=1$",
        rotation=180.0 / np.pi * np.arctan(-1.0 * aspect_ratio),
        fontdict=font_style,
        horizontalalignment="left",
        verticalalignment="bottom",
    )

    #
    # 100 Reynold
    #
    y = 100 * np.power(x, -1.0)
    ax1.plot(x, y, **secondary_line_style)
    ax1.text(
        0.18,
        210,
        "$Re_t=100$",
        rotation=180.0 / np.pi * np.arctan(-1.0 * aspect_ratio),
        fontdict=secondary_font_style,
        horizontalalignment="left",
        verticalalignment="bottom",
    )

    #
    # 10000 Reynold
    #
    y = 10000 * np.power(x, -1.0)
    ax1.plot(x, y, **secondary_line_style)
    ax1.text(
        18,
        210,
        "$Re_t=10^4$",
        rotation=180.0 / np.pi * np.arctan(-1.0 * aspect_ratio),
        fontdict=secondary_font_style,
        horizontalalignment="left",
        verticalalignment="bottom",
    )

    #
    # v' = S_L
    #
    y = np.where(x >= 1, 1.0, np.nan)
    ax1.plot(x, y, **default_line_style)

    #
    # Unit Karlovitz (Based on delta_diff)
    #
    y = np.where(x >= 1, np.power(x, 1.0 / 3.0), np.nan)
    ax1.plot(x, y, **default_line_style)
    ax1.text(
        2000,
        15,
        "$Ka=1$",
        rotation=180.0 / np.pi * np.arctan(1 / 3 * aspect_ratio),
        fontdict=font_style,
        horizontalalignment="left",
        verticalalignment="bottom",
    )

    #
    # 100 Karlovitz (Based on delta_diff)
    #
    if labelStyle == "Peters":
        loc_line_style = default_line_style
        loc_font_style = font_style
    else:
        loc_line_style = secondary_line_style
        loc_font_style = secondary_font_style

    y = np.where(x >= 0.1, 100 ** (2.0 / 3.0) * np.power(x, 1.0 / 3.0), np.nan)
    ax1.plot(x, y, **loc_line_style)
    ax1.text(
        1800,
        320,
        "$Ka=100$",
        rotation=180.0 / np.pi * np.arctan(1 / 3 * aspect_ratio),
        fontdict=loc_font_style,
        horizontalalignment="left",
        verticalalignment="bottom",
    )

    #
    # Unit Damköhler (Based on delta_diff)
    #
    if labelStyle == "Borghi":
        loc_line_style = default_line_style
        loc_font_style = font_style
    else:
        loc_line_style = secondary_line_style
        loc_font_style = secondary_font_style

    y = np.where(x >= 1, x, np.nan)
    ax1.plot(x, y, **loc_line_style)
    ax1.text(
        150,
        220,
        "$Da=1$",
        rotation=180.0 / np.pi * np.arctan(aspect_ratio),
        fontdict=loc_font_style,
        horizontalalignment="left",
        verticalalignment="bottom",
    )

    #
    # Regimes
    #
    if labelStyle == "Peters":
        ax1.text(
            0.3,
            80,
            "Broken reaction zones",
            rotation=180.0 / np.pi * np.arctan(1 / 3 * aspect_ratio),
            fontdict=font_style,
            horizontalalignment="left",
            verticalalignment="bottom",
        )
    elif labelStyle == "Borghi":
        ax1.text(
            1.0,
            15,
            "Well-stirred reactor",
            rotation=180.0 / np.pi * np.arctan(1 * aspect_ratio),
            fontdict=font_style,
            horizontalalignment="left",
            verticalalignment="bottom",
        )

    if labelStyle == "Peters":
        ax1.text(
            200,
            22,
            "Thin reaction zones",
            rotation=180.0 / np.pi * np.arctan(1 / 3 * aspect_ratio),
            fontdict=font_style,
            horizontalalignment="left",
            verticalalignment="bottom",
        )

    elif labelStyle == "Borghi":
        ax1.text(
            50,
            10,
            "Distributed reaction zones",
            rotation=180.0 / np.pi * np.arctan(2 / 3 * aspect_ratio),
            fontdict=font_style,
            horizontalalignment="left",
            verticalalignment="bottom",
        )
    ax1.text(
        80,
        2,
        "Corrugated flamelets",
        fontdict=font_style,
        horizontalalignment="left",
        verticalalignment="bottom",
    )
    ax1.text(
        60,
        0.3,
        "Wrinkled flamelets",
        fontdict=font_style,
        horizontalalignment="left",
        verticalalignment="bottom",
    )
    ax1.text(
        0.15,
        0.15,
        "Laminar flames",
        rotation=180.0 / np.pi * np.arctan(-1.0 * aspect_ratio),
        fontdict=font_style,
        horizontalalignment="left",
        verticalalignment="bottom",
    )

    #
    # Plot case data
    #
    if not lc is None:
        fn = os.path.join(lc.locdata["CasePath"], "witness_data_merged.json")
        with open(fn, "r") as f:
            data = json.load(f)

        pointStyle = dict(
            marker="s",
            linestyle="none",
            fillstyle="none",
            color="red",
            linewidth=1.0,
            markersize=4.0,
        )

        ax1.plot(
            np.array(data["integral"]["l_t"])
            / np.array(data["Fv"]["processed"]["delta_diff"]),
            np.array(data["integral"]["v_t"])
            / np.array(data["Fv"]["processed"]["S_L"]),
            zorder=5,
            **pointStyle
        )

    #
    # Save figure
    #
    if saveFig:
        fig.savefig(
            os.path.join(lc.locdata["CasePath"], "{}Diagram.eps".format(labelStyle)),
            format="eps",
            dpi=1200,
        )

    return fig, ax1


def plotFilterRegimeDiagram(lc=None, fig=None, ax1=None, saveFig=True):
    """
    Prepare regime diagram of Pitsch and De Langeneste (2002) with all the
    points from witness_data_merged.json.
    """

    #
    # Create figure
    #
    try:
        font_style, default_line_style = getStyles()
        plt = setPltBackend()

        if fig is None or ax1 is None:
            fig, (ax1) = plt.subplots(1, 1)
            #
            # Format size
            #
            fig.set_size_inches(6, 4)
            fig.subplots_adjust(top=0.95, right=0.96, bottom=0.13, left=0.13)

            #
            # Format fig
            #
            ax1.set_aspect(4.0 / 6.0)
            aspect_ratio = ax1.get_aspect()

    except Exception as e:
        print("Cannot plot, {}".format(e))
        return

    #
    # Format standard Borghi diagram
    #
    ax1.set_xlabel("$Ka$", fontdict=font_style)
    ax1.set_ylabel("$\Delta/\delta_{diff}$", fontdict=font_style)

    xmin = 0.01
    ymin = 0.01
    xmax = 1e3
    ymax = 1e3
    ax1.set_xlim([xmin, xmax])
    ax1.set_ylim([ymin, ymax])

    ax1.set_xscale("log")
    ax1.set_yscale("log")

    # ax1.xaxis.set_major_locator(ticker.MaxNLocator(6))
    # locs = ax1.get_xticks()
    # ax1.xaxis.set_major_locator(ticker.FixedLocator(locs))
    # ax1.set_xticklabels(locs, fontdict=font_style)
    # ax1.xaxis.set_major_formatter(ticker.StrMethodFormatter("{x:g}"))

    # ax1.yaxis.set_major_locator(ticker.MaxNLocator(5))
    # locs = ax1.get_yticks()
    # ax1.yaxis.set_major_locator(ticker.FixedLocator(locs))
    # ax1.set_yticklabels(locs, fontdict=font_style)
    # ax1.yaxis.set_major_formatter(ticker.StrMethodFormatter("{x:g}"))

    secondary_line_style = copy.deepcopy(default_line_style)
    secondary_line_style["linewidth"] = 0.5
    secondary_line_style["color"] = (0.5, 0.5, 0.5)

    secondary_font_style = copy.deepcopy(font_style)
    secondary_font_style["size"] = 9
    secondary_font_style["color"] = (0.5, 0.5, 0.5)

    #
    # Plot standard lines
    #
    x = np.logspace(np.log10(xmin), np.log10(xmax), 200)

    #
    # Unit filter Reynolds
    #
    y = np.power(x, -0.5)
    ax1.plot(x, y, **default_line_style)
    ax1.text(
        2.0,
        0.22,
        "$Re_\Delta=1$ ($\Delta=\eta$)",
        rotation=180.0 / np.pi * np.arctan(-0.5 * aspect_ratio),
        fontdict=font_style,
        horizontalalignment="left",
        verticalalignment="bottom",
    )

    #
    # Karlovitz limits
    #
    ax1.plot([1, 1], [1, ymax], **default_line_style)
    ax1.text(
        0.7,
        70,
        "$Ka=1$",
        rotation=90.0,
        fontdict=font_style,
        horizontalalignment="left",
        verticalalignment="bottom",
    )

    ax1.plot([100, 100], [0.1, ymax], **default_line_style)
    ax1.text(
        70.0,
        70.0,
        "$Ka=100$",
        rotation=90.0,
        fontdict=font_style,
        horizontalalignment="left",
        verticalalignment="bottom",
    )

    #
    # Resolved reaction zone
    #
    y = np.where(x < 100, 0.1, np.nan)
    ax1.plot(x, y, **default_line_style)
    ax1.text(
        0.1,
        0.12,
        "$\Delta=\delta_R (\\approx0.1\delta_{diff})$",
        rotation=180.0 / np.pi * np.arctan(0.0 * aspect_ratio),
        fontdict=font_style,
        horizontalalignment="left",
        verticalalignment="bottom",
    )

    #
    # Gibson scale
    #
    y = np.where(x <= 1, x**-2, np.nan)
    ax1.plot(x, y, **default_line_style)
    ax1.text(
        0.025,
        150.0,
        "$\Delta=\ell_G$",
        rotation=180.0 / np.pi * np.arctan(-2 * aspect_ratio),
        fontdict=font_style,
        horizontalalignment="left",
        verticalalignment="bottom",
    )

    #
    # Da_Delta 1
    #
    y = np.where(x <= 100, np.where(x >= 1, x, np.nan), np.nan)
    ax1.plot(x, y, **secondary_line_style)
    ax1.text(
        1.8,
        2.8,
        "$Da_\Delta=1$",
        rotation=180.0 / np.pi * np.arctan(1.0 * aspect_ratio),
        fontdict=secondary_font_style,
        horizontalalignment="left",
        verticalalignment="bottom",
    )

    ##########
    # Labels #
    ##########
    ax1.text(
        330,
        1,
        "Broken reaction zones",
        rotation=90.0,
        fontdict=font_style,
        horizontalalignment="left",
        verticalalignment="bottom",
    )

    ax1.text(
        10,
        2.5,
        "Thin reaction zones",
        rotation=90.0,
        fontdict=font_style,
        horizontalalignment="left",
        verticalalignment="bottom",
    )

    ax1.text(
        0.1,
        30,
        "Corrugated\nflamelets",
        rotation=180.0 / np.pi * np.arctan(-4 * aspect_ratio),
        fontdict=font_style,
        horizontalalignment="left",
        verticalalignment="bottom",
    )

    ax1.text(
        0.02,
        4,
        "Resolved\nflame surface",
        rotation=180.0 / np.pi * np.arctan(-1 * aspect_ratio),
        fontdict=font_style,
        horizontalalignment="left",
        verticalalignment="bottom",
    )

    ax1.text(
        0.02,
        0.3,
        "Resolved turbulence",
        rotation=180.0 / np.pi * np.arctan(0 * aspect_ratio),
        fontdict=font_style,
        horizontalalignment="left",
        verticalalignment="bottom",
    )

    ax1.text(
        1.2,
        0.024,
        "DNS",
        rotation=180.0 / np.pi * np.arctan(0 * aspect_ratio),
        fontdict=font_style,
        horizontalalignment="left",
        verticalalignment="bottom",
    )

    #
    # Plot case data
    #
    if not lc is None:
        fn = os.path.join(lc.locdata["CasePath"], "witness_data_merged.json")
        with open(fn, "r") as f:
            data = json.load(f)

        pointStyle = dict(
            marker="s",
            linestyle="none",
            fillstyle="none",
            color="red",
            linewidth=1.0,
            markersize=4.0,
        )

        ax1.plot(
            np.array(data["Fv"]["processed"]["tau_c"])
            / np.array(data["kolmogorov"]["t_K"]),
            np.array(data["cutoff"]["l_filter"])
            / np.array(data["Fv"]["processed"]["delta_diff"]),
            zorder=5,
            **pointStyle
        )

    #
    # Save figure
    #
    if saveFig:
        fig.savefig(
            os.path.join(lc.locdata["CasePath"], "LESRegimeDiagram.eps"),
            format="eps",
            dpi=1200,
        )

    return fig, ax1


def plotWilliamsRegimeDiagram(lc=None, fig=None, ax1=None, saveFig=True):
    """
    Prepare regime diagram of Balakrishnan and Williams (1994) with all the
    points from witness_data_merged.json.
    """

    #
    # Create figure
    #
    try:
        font_style, default_line_style = getStyles()
        plt = setPltBackend()

        if fig is None or ax1 is None:
            fig, (ax1) = plt.subplots(1, 1)
            #
            # Format size
            #
            fig.set_size_inches(6, 4)
            fig.subplots_adjust(top=0.95, right=0.96, bottom=0.13, left=0.13)

            #
            # Format fig
            #
            ax1.set_aspect(5.2 / 6.0)
            aspect_ratio = ax1.get_aspect()

    except Exception as e:
        print("Cannot plot, {}".format(e))
        return

    #
    # Format standard Borghi diagram
    #
    ax1.set_xlabel("$Re_t$", fontdict=font_style)
    ax1.set_ylabel("$Da_t$", fontdict=font_style)

    xmin = 1e-2
    ymin = 1e-2
    xmax = 1e6
    ymax = 1e4
    ax1.set_xlim([xmin, xmax])
    ax1.set_ylim([ymin, ymax])

    ax1.set_xscale("log")
    ax1.set_yscale("log")

    # ax1.xaxis.set_major_locator(ticker.MaxNLocator(6))
    # locs = ax1.get_xticks()
    # ax1.xaxis.set_major_locator(ticker.FixedLocator(locs))
    # ax1.set_xticklabels(locs, fontdict=font_style)
    # ax1.xaxis.set_major_formatter(ticker.StrMethodFormatter("{x:g}"))

    # ax1.yaxis.set_major_locator(ticker.MaxNLocator(5))
    # locs = ax1.get_yticks()
    # ax1.yaxis.set_major_locator(ticker.FixedLocator(locs))
    # ax1.set_yticklabels(locs, fontdict=font_style)
    # ax1.yaxis.set_major_formatter(ticker.StrMethodFormatter("{x:g}"))

    secondary_line_style = copy.deepcopy(default_line_style)
    secondary_line_style["linewidth"] = 0.5
    secondary_line_style["color"] = (0.5, 0.5, 0.5)

    secondary_font_style = copy.deepcopy(font_style)
    secondary_font_style["size"] = 9
    secondary_font_style["color"] = (0.5, 0.5, 0.5)

    #
    # Plot standard lines
    #
    x = np.logspace(np.log10(xmin), np.log10(xmax), 300)

    #
    # Unit Reynolds
    #
    ax1.plot([1.0, 1.0], [ymin, ymax], **default_line_style)
    ax1.text(
        0.5,
        1e3,
        "$Re_t=1$",
        rotation=90.0,
        fontdict=font_style,
        horizontalalignment="left",
        verticalalignment="bottom",
    )

    #
    # Unit Kolmogorov Damköhler
    #
    y = np.where(x >= 1, x**0.5, np.nan)
    ax1.plot(x, y, **default_line_style)
    ax1.text(
        8e4,
        4e2,
        "$Da_\eta=1$",
        rotation=180.0 / np.pi * np.arctan(0.5 * aspect_ratio),
        fontdict=font_style,
        horizontalalignment="left",
        verticalalignment="bottom",
    )

    #
    # 1/100 Kolmogorov Damköhler
    #
    y = np.where(x >= 1, 1e-2 * x**0.5, np.nan)
    ax1.plot(x, y, **secondary_line_style)
    ax1.text(
        8e4,
        4e0,
        "$Da_\eta=0.01$",
        rotation=180.0 / np.pi * np.arctan(0.5 * aspect_ratio),
        fontdict=secondary_font_style,
        horizontalalignment="left",
        verticalalignment="bottom",
    )

    #
    # Unit Damköhler
    #
    y = np.where(x >= 1, 1, np.nan)
    ax1.plot(x, y, **default_line_style)
    ax1.text(
        8e4,
        1.3,
        "$Da_t=1$",
        rotation=180.0 / np.pi * np.arctan(0.0 * aspect_ratio),
        fontdict=font_style,
        horizontalalignment="left",
        verticalalignment="bottom",
    )

    #
    # Equal speeds
    #
    y = np.where(x >= 1, x**1, np.nan)
    ax1.plot(x, y, **secondary_line_style)
    ax1.text(
        700,
        1100,
        "$v^\prime/v_c=1$",
        rotation=180.0 / np.pi * np.arctan(1.0 * aspect_ratio),
        fontdict=secondary_font_style,
        horizontalalignment="left",
        verticalalignment="bottom",
    )

    y = np.where(x >= 1, 1e2 * x**1, np.nan)
    ax1.plot(x, y, **secondary_line_style)
    ax1.text(
        7,
        1100,
        "$v^\prime/v_c=0.1$",
        rotation=180.0 / np.pi * np.arctan(1.0 * aspect_ratio),
        fontdict=secondary_font_style,
        horizontalalignment="left",
        verticalalignment="bottom",
    )

    y = np.where(x >= 1, 1e-2 * x**1, np.nan)
    ax1.plot(x, y, **secondary_line_style)
    ax1.text(
        70000,
        1100,
        "$v^\prime/v_c=10$",
        rotation=180.0 / np.pi * np.arctan(1.0 * aspect_ratio),
        fontdict=secondary_font_style,
        horizontalalignment="left",
        verticalalignment="bottom",
    )

    y = np.where(x >= 1, 1e-4 * x**1, np.nan)
    ax1.plot(x, y, **secondary_line_style)
    ax1.text(
        100000,
        7,
        "$v^\prime/v_c=100$",
        rotation=180.0 / np.pi * np.arctan(1.0 * aspect_ratio),
        fontdict=secondary_font_style,
        horizontalalignment="left",
        verticalalignment="bottom",
    )

    y = np.where(x >= 1, 1e-6 * x**1, np.nan)
    ax1.plot(x, y, **secondary_line_style)
    ax1.text(
        100000,
        0.07,
        "$v^\prime/v_c=1000$",
        rotation=180.0 / np.pi * np.arctan(1.0 * aspect_ratio),
        fontdict=secondary_font_style,
        horizontalalignment="left",
        verticalalignment="bottom",
    )

    #
    # Equal lengths
    #
    y = np.where(x >= 1, x ** (-1), np.nan)
    ax1.plot(x, y, **secondary_line_style)
    ax1.text(
        10,
        0.032,
        "$\ell_t/\delta=1$",
        rotation=180.0 / np.pi * np.arctan(-1.0 * aspect_ratio),
        fontdict=secondary_font_style,
        horizontalalignment="left",
        verticalalignment="bottom",
    )

    ##########
    # Labels #
    ##########
    ax1.text(
        0.1,
        1,
        "Laminar flames",
        rotation=90.0,
        fontdict=font_style,
        horizontalalignment="left",
        verticalalignment="bottom",
    )

    ax1.text(
        20,
        200,
        "Flamelets",
        rotation=180.0 / np.pi * np.arctan(0.5 * aspect_ratio),
        fontdict=font_style,
        horizontalalignment="left",
        verticalalignment="bottom",
    )

    ax1.text(
        2000,
        6,
        "Broken flamelets",
        rotation=180.0 / np.pi * np.arctan(0.5 * aspect_ratio),
        fontdict=font_style,
        horizontalalignment="left",
        verticalalignment="bottom",
    )

    ax1.text(
        100,
        0.03,
        "Distributed reactions\nor\nFlame extinction",
        rotation=180.0 / np.pi * np.arctan(0.0 * aspect_ratio),
        multialignment="center",
        fontdict=font_style,
        horizontalalignment="left",
        verticalalignment="bottom",
    )

    #
    # Plot case data
    #
    if not lc is None:
        fn = os.path.join(
            lc.locdata["CasePath"],
            "witness_data_merged.json".format(lc.locdata["CaseName"]),
        )
        with open(fn, "r") as f:
            data = json.load(f)

        pointStyle = dict(
            marker="s",
            linestyle="none",
            fillstyle="none",
            color="red",
            linewidth=1.0,
            markersize=4.0,
        )

        ax1.plot(
            np.array(data["integral"]["Re_t"]),
            np.array(data["Fv"]["processed"]["tau_c"])
            / np.array(data["integral"]["t_t"]),
            zorder=5,
            **pointStyle
        )

    #
    # Save figure
    #
    if saveFig:
        fig.savefig(
            os.path.join(lc.locdata["CasePath"], "WilliamsDiagram.eps"),
            format="eps",
            dpi=1200,
        )

    return fig, ax1


def plotMixtureFractionRegimeDiagram(lc=None, fig=None, ax1=None, saveFig=True):
    """
    Prepare regime diagram of Peters in mixture fraction space
    with all the points from <caseName>-all-scales.json.
    """

    #
    # Create figure
    #
    try:
        font_style, default_line_style = getStyles()
        plt = setPltBackend()

        if fig is None or ax1 is None:
            fig, (ax1) = plt.subplots(1, 1)
            #
            # Format size
            #
            fig.set_size_inches(6, 4)
            fig.subplots_adjust(top=0.95, right=0.96, bottom=0.15, left=0.13)

            #
            # Format fig
            #
            ax1.set_aspect(4.2 / 6.0)
            aspect_ratio = ax1.get_aspect()

    except Exception as e:
        print("Cannot plot, {}".format(e))
        return

    #
    # Format standard Borghi diagram
    #
    ax1.set_xlabel(
        "${\chi_{st}^{ext}}/{ \left\{ \chi \\right\}_{st} }$", fontdict=font_style
    )
    ax1.set_ylabel("${Z^\prime_{st}}/{\left(\Delta Z\\right)_F}$", fontdict=font_style)

    xmin = 1e-1
    ymin = 1e-2
    xmax = 1e3
    ymax = 4e1
    ax1.set_xlim([xmin, xmax])
    ax1.set_ylim([ymin, ymax])

    ax1.set_xscale("log")
    ax1.set_yscale("log")

    # ax1.xaxis.set_major_locator(ticker.MaxNLocator(6))
    # locs = ax1.get_xticks()
    # ax1.xaxis.set_major_locator(ticker.FixedLocator(locs))
    # ax1.set_xticklabels(locs, fontdict=font_style)
    # ax1.xaxis.set_major_formatter(ticker.StrMethodFormatter("{x:g}"))

    # ax1.yaxis.set_major_locator(ticker.MaxNLocator(5))
    # locs = ax1.get_yticks()
    # ax1.yaxis.set_major_locator(ticker.FixedLocator(locs))
    # ax1.set_yticklabels(locs, fontdict=font_style)
    # ax1.yaxis.set_major_formatter(ticker.StrMethodFormatter("{x:g}"))

    secondary_line_style = copy.deepcopy(default_line_style)
    secondary_line_style["linewidth"] = 0.5
    secondary_line_style["color"] = (0.5, 0.5, 0.5)

    secondary_font_style = copy.deepcopy(font_style)
    secondary_font_style["size"] = 9
    secondary_font_style["color"] = (0.5, 0.5, 0.5)

    #
    # Plot standard lines
    #
    x = np.logspace(np.log10(xmin), np.log10(xmax), 300)

    #
    # Quenching
    #
    ax1.plot([1.0, 1.0], [ymin, ymax], **default_line_style)
    ax1.text(
        0.64,
        3e0,
        "$\left\{ \chi \\right\}_{st} = \chi_{st}^{ext}$",
        rotation=90.0,
        fontdict=font_style,
    )

    #
    # Fluctuations are diffusion width
    #
    y = np.where(x >= 1, 1.0, np.nan)
    ax1.plot(x, y, **default_line_style)
    ax1.text(
        7e1,
        1.2e0,
        "$Z^\prime_{st} = \left(\Delta Z\\right)_F = 2 Z_{st}$",
        rotation=180.0 / np.pi * np.arctan(0.0 * aspect_ratio),
        fontdict=font_style,
    )

    #
    # Fluctuations are reaction width
    #
    y = np.where(x >= 1, 0.1 * x**-0.25, np.nan)
    ax1.plot(x, y, **default_line_style)
    ax1.text(
        5e1,
        2.2e-2,
        "$Z^\prime_{st} = \left(\Delta Z\\right)_R = \epsilon \left(\Delta Z\\right)_F$",
        rotation=180.0 / np.pi * np.arctan(-0.25 * aspect_ratio),
        fontdict=font_style,
    )

    ##########
    # Labels #
    ##########
    ax1.text(
        0.3,
        0.7,
        "Flame extinction",
        rotation=90.0,
        fontdict=font_style,
    )

    ax1.text(
        3,
        6,
        "Separated flamelets",
        rotation=180.0 / np.pi * np.arctan(0.0 * aspect_ratio),
        fontdict=font_style,
    )

    ax1.text(
        3,
        0.16,
        "Connected flame zones",
        rotation=180.0 / np.pi * np.arctan(-0.125 * aspect_ratio),
        fontdict=font_style,
    )

    ax1.text(
        3,
        0.013,
        "Connected reaction zones",
        rotation=180.0 / np.pi * np.arctan(-0.25 * aspect_ratio),
        fontdict=font_style,
    )

    #
    # Plot case data
    #
    if not lc is None:
        fn = os.path.join(
            lc.locdata["CasePath"],
            "witness_data_merged.json".format(lc.locdata["CaseName"]),
        )
        with open(fn, "r") as f:
            data = json.load(f)

        pointStyle = dict(
            marker="s",
            linestyle="none",
            fillstyle="none",
            color="red",
            linewidth=1.0,
            markersize=4.0,
        )

        ax1.plot(
            np.array(data["integral"]["Re_t"]),
            np.array(data["Fv"]["processed"]["tau_c"])
            / np.array(data["integral"]["t_t"]),
            zorder=5,
            **pointStyle
        )

    #
    # Save figure
    #
    if saveFig:
        fig.savefig(
            os.path.join(lc.locdata["CasePath"], "mixtureFractionRegimeDiagram.eps"),
            format="eps",
            dpi=1200,
        )

    return fig, ax1
