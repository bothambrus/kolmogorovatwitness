#################################################################################
#   KolmogorovAtWitness                                                         #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import copy
import json
import os

import numpy as np

from kolmogorovatwitness.dictionary_template import (
    getExpandedDictionary,
    insertToExpandedDictionary,
)
from kolmogorovatwitness.parallel import Barrier, getMpiData
from kolmogorovatwitness.point_type import PointType
from kolmogorovatwitness.serialize import serializeDictionary


def extractWitnessPointData(lc, noMPI=False, overwrite=False):
    """
    Extract the required data of witness points.
    """

    #
    # Deal with MPI paralleleism
    #
    size, rank = getMpiData(noMPI=noMPI)

    #
    # Apply point filtering
    #
    allIndeces = lc.getWitnessIndcesFromLocData()

    #
    # Get local indeces per MPI
    #
    indeces = []
    for jj, ii in enumerate(allIndeces):
        if jj % size == rank:
            indeces.append(ii)

    print(
        "Number of witness points to extract: {} in {} dimensional case on rank {}/{}".format(
            len(indeces), lc.ndim, rank, size
        )
    )

    for ii in indeces:
        #
        # Create point
        #
        point = PointType(
            index=ii, coords=lc.witnessDefinition[ii, :], filterSize=lc.filterSize[ii]
        )

        #
        # Extract data corresponding to this one point using the Alya script
        #
        point.extractSinglePointFiles(lc, rank=rank, overwrite=overwrite)

    #
    # Barrier
    #
    Barrier(noMPI=noMPI)


def processWitnessPoints(lc, noMPI=False):
    """
    Process the data of witness points.
    """

    #
    # Deal with MPI paralleleism
    #
    size, rank = getMpiData(noMPI=noMPI)

    #
    # Apply point filtering
    #
    allIndeces = lc.getWitnessIndcesFromLocData()

    #
    # Get local indeces per MPI
    #
    indeces = []
    for jj, ii in enumerate(allIndeces):
        if jj % size == rank:
            indeces.append(ii)

    print(
        "Number of witness points to process: {} in {} dimensional case on rank {}/{}".format(
            len(indeces), lc.ndim, rank, size
        )
    )

    for ii in indeces:
        #
        # Create point
        #
        point = PointType(
            index=ii, coords=lc.witnessDefinition[ii, :], filterSize=lc.filterSize[ii]
        )

        #
        # Process files of single point
        #
        point.processSinglePointFiles(lc)

    #
    # Barrier
    #
    Barrier(noMPI=noMPI)


def deleteIntermediateWitnessPointData(lc, noMPI=False):
    """
    Delete the intermediate data of witness points.
    """

    #
    # Deal with MPI paralleleism
    #
    size, rank = getMpiData(noMPI=noMPI)

    #
    # Apply point filtering
    #
    allIndeces = lc.getWitnessIndcesFromLocData()

    #
    # Get local indeces per MPI
    #
    indeces = []
    for jj, ii in enumerate(allIndeces):
        if jj % size == rank:
            indeces.append(ii)

    print(
        "Number of witness points to process: {} in {} dimensional case on rank {}/{}".format(
            len(indeces), lc.ndim, rank, size
        )
    )

    for ii in indeces:
        #
        # Create point
        #
        point = PointType(
            index=ii, coords=lc.witnessDefinition[ii, :], filterSize=lc.filterSize[ii]
        )

        #
        # Delete files of single point
        #
        point.deleteSinglePointFiles(lc)

    #
    # Barrier
    #
    Barrier(noMPI=noMPI)


def mergeWitnessPointData(lc):
    """
    Merge the data of individual witness point files.
    """

    if not lc.witnessDefinition is None:
        #
        # Try processing all points
        #
        indeces = range(lc.nwit)

        #
        # List of existing files
        #
        fnlist = []
        for ii in indeces:
            fnlist.append(None)

            #
            # Check existence
            #
            fn = os.path.join(
                lc.locdata["CasePath"], "witness_data_entity{}.json".format(ii + 1)
            )

            if os.path.isfile(fn):
                fnlist[-1] = fn

        #
        # Obtain the structure
        #
        indExist = np.argwhere(np.logical_not(np.array(fnlist) == None)).flatten()

        if len(indExist) > 0:
            with open(fnlist[indExist[0]], "r") as f:
                templateDict = json.load(f)
        else:
            templateDict = {}

        #
        # Create dictionary from template, with correct length
        #
        allData = {}
        allData = getExpandedDictionary(allData, templateDict, lc.nwit)

        #
        # Add points
        #
        for ii in indeces:
            if fnlist[ii] is None:
                continue

            #
            # Add data
            #
            with open(fnlist[ii], "r") as f:
                templateDict = json.load(f)

            allData = insertToExpandedDictionary(allData, templateDict, ii)

        #
        # Serialize data
        #
        outData = {}
        outData = serializeDictionary(outData, allData, elementLimit=np.inf)

        #####################
        # Write merged data #
        #####################
        fn = "witness_data_merged.json"
        with open(os.path.join(lc.locdata["CasePath"], fn), "w") as f:
            json.dump(outData, f, indent=2)
