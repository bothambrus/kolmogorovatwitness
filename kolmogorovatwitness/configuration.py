#################################################################################
#   KolmogorovAtWitness                                                         #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import json
import os
import subprocess

import numpy as np

from kolmogorovatwitness.witness import extractHeaderInfo

import kolmogorovatwitness.context  # isort:skip
from alyatab.table import AlyaTable  # isort:skip


class GlobalConfig:
    def __init__(
        self,
        json_path_glob=os.path.abspath(
            os.path.join(os.path.dirname(__file__), "..", "globalConfig.json")
        ),
    ):
        #
        # Initialize global data
        #
        self.globdata = {}
        self.json_path_glob = json_path_glob

        #
        # Read json
        #
        if os.path.isfile(self.json_path_glob):
            #
            # Read if exists
            #
            with open(self.json_path_glob, "r") as f:
                self.globdata = json.load(f)
        else:
            #
            # Write default if does not exist
            #
            self.writeDefaultGlob()

    def writeDefaultGlob(self):
        """
        Set current state to default, and write to given path
        """
        self.setToDefaultGlob()
        self.writeGlob()

    def writeGlob(self):
        """
        Write to given path
        """
        with open(self.json_path_glob, "w") as f:
            json.dump(self.globdata, f, sort_keys=True, indent=4)

    def setToDefaultGlob(self):
        """
        Set current state to default
        """
        self.globdata = {}
        self.globdata["AlyaPath"] = ""

        #
        # Try getting Alya path
        #
        try:
            output = subprocess.check_output("which alya-sets", shell=True).decode()
        except:
            output = ""

        if len(output) > 0:
            if "Utils" in output:
                self.globdata["AlyaPath"] = os.path.dirname(
                    os.path.dirname(os.path.dirname(output))
                )


class LocalConfig(GlobalConfig):
    def __init__(
        self,
        json_path_loc=os.path.abspath("KolmogorovAtWitnessInputFile.json"),
        json_path_glob=os.path.abspath(
            os.path.join(os.path.dirname(__file__), "..", "globalConfig.json")
        ),
    ):
        #
        # Initialize parent class
        #
        GlobalConfig.__init__(self, json_path_glob=json_path_glob)

        #
        # Initialize local data
        #
        self.locdata = {}
        self.json_path_loc = json_path_loc

        #
        # Read json
        #
        if os.path.isfile(self.json_path_loc):
            #
            # Read if exists
            #
            with open(self.json_path_loc, "r") as f:
                self.locdata = json.load(f)

        #
        # Initialize member data
        #
        self.witnessDefinition = None
        self.filterSize = None
        self.nwit = None
        self.ndim = None

    def writeLoc(self):
        """
        Write to given path
        """
        with open(self.json_path_loc, "w") as f:
            json.dump(self.locdata, f, sort_keys=True, indent=4)

    def getData(self, noTables=False):
        """
        Get all available data
        """
        self.getWitnessDefinition()
        self.getFilterSize()
        self.getPropertyRequirements()
        self.getWitnessOutputHeaders()
        if not noTables:
            self.getTables()
        self.processConfigInteractions(noTables=noTables)

    def getWitnessDefinition(self):
        """
        Read witness definition file
        """
        self.witnessDefinition = None
        self.nwit = None
        self.ndim = None
        if "WitnessDefinitionPath" in self.locdata.keys():
            #
            # Return if it is not a file
            #
            if not os.path.isfile(self.locdata["WitnessDefinitionPath"]):
                print("Cannot process: ".format(self.locdata["WitnessDefinitionPath"]))
                return

            #
            # Read witness definition
            #
            try:
                #
                # Porcess with Numpy if header line is not present
                #
                self.witnessDefinition = np.loadtxt(
                    self.locdata["WitnessDefinitionPath"], comments="$"
                )
            except:
                #
                # Otherwise process by hand
                #
                self.witnessDefinition = []
                with open(self.locdata["WitnessDefinitionPath"], "r") as f:
                    line = True
                    while line:
                        line = f.readline()
                        line = line.strip()

                        #
                        # Reasons to skip this line
                        #
                        if not line:
                            continue

                        if line[0] in ["$", "#"]:
                            continue

                        try:
                            float(line.split()[0])
                        except:
                            continue

                        if len(line.split()) < 2:
                            continue

                        #
                        # Now we should only have nice numbers
                        #
                        ndim = 3
                        coords = line.split()
                        for ic in range(len(coords)):
                            if ic < ndim:
                                coords[ic] = float(coords[ic])

                        ndim = min(len(coords), 3)
                        #
                        # Add to list
                        #
                        self.witnessDefinition.append(coords[0:ndim])

                self.witnessDefinition = np.array(self.witnessDefinition)

            self.ndim = self.witnessDefinition.shape[1]
            self.nwit = self.witnessDefinition.shape[0]

    def getFilterSize(self):
        """
        Decide filter size
        """
        if not (self.nwit is None):
            #
            # Should be executed only after reading the witness file
            #
            self.filterSize = np.empty([self.nwit])

            if not "FilterSizeMethod" in self.locdata.keys():
                return
            #
            # Methods
            #
            if self.locdata["FilterSizeMethod"] == "list_file":
                ii = 0
                with open(self.locdata["FilterSizePath"], "r") as f:
                    line = True
                    while line:
                        line = f.readline()
                        line = line.strip()

                        #
                        # Reasons to skip this line
                        #
                        if not line:
                            continue

                        if line[0] in ["$", "#"]:
                            continue

                        try:
                            float(line.split()[0])
                        except:
                            continue

                        if len(line.split()) < 1:
                            continue

                        if ii >= self.nwit:
                            break

                        #
                        # Read
                        #
                        self.filterSize[ii] = float(line.split()[0])
                        ii += 1

            elif self.locdata["FilterSizeMethod"] == "constant":
                #
                # Assign
                #
                self.filterSize = (
                    np.ones([self.nwit]) * self.locdata["FilterSizeConstant"]
                )

    def getPropertyRequirements(self):
        """
        Get properties that are required to execute the given tasks
        """
        #
        # Initialize
        #
        self.locdata["RequiredProperties"] = {}
        if "Modules" in self.locdata.keys():
            for m in self.locdata["Modules"]:
                self.locdata["RequiredProperties"][m] = []

        self.locdata["RequiredProperties"]["general"] = []

        if "Tasks" in self.locdata.keys():
            #
            # Get necessary properties for each task
            #
            if ("kolmogorov" in self.locdata["Tasks"]) or (
                "integral" in self.locdata["Tasks"]
            ):
                #
                # Minimum required properties to claculate kinetic energy dissipation rate
                #
                for k in ["time", "viscosity", "density", "sgs_viscosity"]:
                    if not k in self.locdata["RequiredProperties"]["general"]:
                        self.locdata["RequiredProperties"]["general"].append(k)

                #
                # Nastin
                #
                if "nsi" in self.locdata["Modules"]:
                    for k in [
                        "S11",
                        "S22",
                        "S12",
                        "S33",
                        "S13",
                        "S23",
                        "TURBU",
                    ]:
                        if not k in self.locdata["RequiredProperties"]["nsi"]:
                            self.locdata["RequiredProperties"]["nsi"].append(k)

            if "integral" in self.locdata["Tasks"]:
                #
                # Minimum required properties to claculate mean velocity fluctuations
                #
                for k in ["time", "viscosity", "density"]:
                    if not k in self.locdata["RequiredProperties"]["general"]:
                        self.locdata["RequiredProperties"]["general"].append(k)

                if "nsi" in self.locdata["Modules"]:
                    for k in [
                        "VELOX",
                        "VELOY",
                        "VELOZ",
                        "S11",
                        "S22",
                        "S12",
                        "S33",
                        "S13",
                        "S23",
                        "TURBU",
                    ]:
                        if not k in self.locdata["RequiredProperties"]["nsi"]:
                            self.locdata["RequiredProperties"]["nsi"].append(k)

            if "spectrum" in self.locdata["Tasks"]:
                #
                # If any raw things are required, request the reading of them
                #
                if ("spectrumProperties" in self.locdata.keys()) and (
                    "raw" in self.locdata["spectrumProperties"].keys()
                ):
                    for mod in self.locdata["spectrumProperties"]["raw"].keys():
                        #
                        # Add module to required ones
                        #
                        if not mod in self.locdata["RequiredProperties"].keys():
                            self.locdata["RequiredProperties"][mod] = []

                        #
                        # Add required properties of this module
                        #
                        for k in self.locdata["spectrumProperties"]["raw"][mod]:
                            if not k in self.locdata["RequiredProperties"][mod]:
                                self.locdata["RequiredProperties"][mod].append(k)

            if "chemical_scales" in self.locdata["Tasks"]:
                #
                # Minimum required properties to claculate Chemical scales
                #
                a = 1

            if "favre_error" in self.locdata["Tasks"]:
                #
                # If the error made by ensemble averaging the Favre-filtered velocities
                # is to be calculated, request "density_Fv"
                #
                for k in ["time", "density", "density_Fv"]:
                    if not k in self.locdata["RequiredProperties"]["general"]:
                        self.locdata["RequiredProperties"]["general"].append(k)

        if "PropertyMethod" in self.locdata.keys():
            #
            # Mathod to get density
            #
            if (
                ("density" in self.locdata["RequiredProperties"]["general"])
                and (self.locdata["PropertyMethod"] == "tabulated")
                and "PropertyDensityMethod" in self.locdata.keys()
                and ("Modules" in self.locdata.keys())
            ):
                if self.locdata["PropertyDensityMethod"] == "EoS_T_from_temper":
                    #
                    # Density needs TEMPE and lookup
                    #
                    m = "tem"
                    if m in self.locdata["Modules"]:
                        for k in [
                            "TEMPE",
                        ]:
                            if not k in self.locdata["RequiredProperties"][m]:
                                self.locdata["RequiredProperties"][m].append(k)

                    m = "chm"
                    if m in self.locdata["Modules"]:
                        for k in [
                            "SCONC",
                        ]:
                            if not k in self.locdata["RequiredProperties"][m]:
                                self.locdata["RequiredProperties"][m].append(k)

                elif (
                    self.locdata["PropertyDensityMethod"]
                    == "EoS_H_from_temper_to_NASA_poly"
                ):
                    #
                    # Density needs ENTHA and lookup
                    #
                    m = "tem"
                    if m in self.locdata["Modules"]:
                        for k in [
                            "ENTHA",
                        ]:
                            if not k in self.locdata["RequiredProperties"][m]:
                                self.locdata["RequiredProperties"][m].append(k)

                    m = "chm"
                    if m in self.locdata["Modules"]:
                        for k in [
                            "SCONC",
                        ]:
                            if not k in self.locdata["RequiredProperties"][m]:
                                self.locdata["RequiredProperties"][m].append(k)

                elif (
                    self.locdata["PropertyDensityMethod"]
                    == "EoS_H_from_SCONC_to_NASA_poly"
                ):
                    #
                    # Density needs only lookup
                    #
                    m = "chm"
                    if m in self.locdata["Modules"]:
                        for k in [
                            "SCONC",
                        ]:
                            if not k in self.locdata["RequiredProperties"][m]:
                                self.locdata["RequiredProperties"][m].append(k)

                elif self.locdata["PropertyDensityMethod"] == "from_chemic":
                    #
                    # Density is in .chm.wit
                    #
                    m = "chm"
                    if m in self.locdata["Modules"]:
                        for k in [
                            "DENSI",
                        ]:
                            if not k in self.locdata["RequiredProperties"][m]:
                                self.locdata["RequiredProperties"][m].append(k)

            if (
                ("viscosity" in self.locdata["RequiredProperties"]["general"])
                and (self.locdata["PropertyMethod"] == "tabulated")
                and ("Modules" in self.locdata.keys())
            ):
                #
                # Viscosity needs lookup
                #
                m = "chm"
                if m in self.locdata["Modules"]:
                    for k in [
                        "SCONC",
                    ]:
                        if not k in self.locdata["RequiredProperties"][m]:
                            self.locdata["RequiredProperties"][m].append(k)

            #
            # Switch SCONC to CONCE if necessary
            #
            if (
                "PropertyControlVariableMethod" in self.locdata.keys()
                and self.locdata["PropertyControlVariableMethod"][:5] == "CONCE"
                and "chm" in self.locdata["RequiredProperties"]
            ):
                if "SCONC" in self.locdata["RequiredProperties"]["chm"]:
                    isconc = self.locdata["RequiredProperties"]["chm"].index("SCONC")
                    self.locdata["RequiredProperties"]["chm"][isconc] = "CONCE"

    def getWitnessOutputHeaders(self):
        """
        Process headers of witness files, to know which properties are present
        """
        if ("Modules" in self.locdata.keys()) and ("CaseName" in self.locdata.keys()):
            #
            # Get list of files
            #
            key = "ModulesWitnessFileNames"
            self.locdata[key] = {}
            for m in self.locdata["Modules"]:
                self.locdata[key][m] = "{}.{}.wit".format(self.locdata["CaseName"], m)

            #
            # Process header of these files
            #
            if "CasePath" in self.locdata.keys():
                key = "ModulesWitnessHeaderData"
                self.locdata[key] = {}

                for m in self.locdata["Modules"]:
                    fn = self.locdata["ModulesWitnessFileNames"][m]

                    self.locdata[key][m] = extractHeaderInfo(
                        self.locdata["CasePath"], fn
                    )

    def getTables(self):
        """
        Read Alya tables.
        """
        #
        # Property table
        #
        if "PropertyLookupTable" in self.locdata.keys():
            self.locdata["PropertyLookupTableObject"] = AlyaTable()
            self.locdata["PropertyLookupTableObject"].readText(
                self.locdata["PropertyLookupTable"]
            )

        #
        # Enthalpy scaling table, if necessary
        #
        if "PropertyLookupEnthalpyScalingTable" in self.locdata.keys():
            self.locdata["PropertyLookupEnthalpyScalingTableObject"] = AlyaTable()
            self.locdata["PropertyLookupEnthalpyScalingTableObject"].readText(
                self.locdata["PropertyLookupEnthalpyScalingTable"]
            )

        #
        # Favre-filtered density table
        #
        if (
            "FavreDensityLookupTable" in self.locdata.keys()
            and "favre_error" in self.locdata["Tasks"]
        ):
            if (
                self.locdata["PropertyLookupTable"]
                == self.locdata["FavreDensityLookupTable"]
            ):
                self.locdata["FavreDensityLookupTableObject"] = self.locdata[
                    "PropertyLookupTableObject"
                ]
            else:
                self.locdata["FavreDensityLookupTableObject"] = AlyaTable()
                self.locdata["FavreDensityLookupTableObject"].readText(
                    self.locdata["FavreDensityLookupTable"]
                )

    def processConfigInteractions(self, noTables=False):
        """
        Process config interactions.
        E.g.:
        - Number of necessary scaled control variables
        """

        if ("PropertyLookupTableObject" in self.locdata.keys()) and (
            "Modules" in self.locdata.keys()
        ):
            #
            # Convert SCONC and CONCE in required properties to as many SCO** and CON** as the table needs
            #
            if "chm" in self.locdata["RequiredProperties"].keys():
                if "SCONC" in self.locdata["RequiredProperties"]["chm"]:
                    isconc = self.locdata["RequiredProperties"]["chm"].index("SCONC")

                    #
                    # Remove SCONC
                    #
                    del self.locdata["RequiredProperties"]["chm"][isconc]

                    if not noTables:
                        #
                        # Replace with SCO**
                        #
                        for ii, ck in enumerate(
                            self.locdata["PropertyLookupTableObject"].coordKeys
                        ):
                            newkey = "SCO{:02d}".format(ii + 1)
                            if not newkey in self.locdata["RequiredProperties"]["chm"]:
                                self.locdata["RequiredProperties"]["chm"].append(newkey)

                if "CONCE" in self.locdata["RequiredProperties"]["chm"]:
                    iconce = self.locdata["RequiredProperties"]["chm"].index("CONCE")

                    #
                    # Remove CONCE
                    #
                    del self.locdata["RequiredProperties"]["chm"][iconce]

                    if not noTables:
                        #
                        # Replace with CON**
                        #
                        for ii, ck in enumerate(
                            self.locdata["PropertyLookupTableObject"].coordKeys
                        ):
                            ii_unscaled = self.locdata[
                                "PropertyControlVariableSconcIndexToConceIndex"
                            ][ii]
                            newkey = "CON{:02d}".format(ii_unscaled)

                            if not newkey in self.locdata["RequiredProperties"]["chm"]:
                                self.locdata["RequiredProperties"]["chm"].append(newkey)

        else:
            if "chm" in self.locdata["RequiredProperties"].keys():
                if "SCONC" in self.locdata["RequiredProperties"]["chm"]:
                    isconc = self.locdata["RequiredProperties"]["chm"].index("SCONC")
                    #
                    # Remove SCONC
                    #
                    del self.locdata["RequiredProperties"]["chm"][isconc]

    def getWitnessIndces(
        self,
        xcrit="any",
        ycrit="any",
        zcrit="any",
        xhigh=1e10,
        xlow=-1e10,
        yhigh=1e10,
        ylow=-1e10,
        zhigh=1e10,
        zlow=-1e10,
        x=0.0,
        y=0.0,
        z=0.0,
    ):
        """
        Get indeces corresponding to given criteria.
        Criteria types:
        `any`:    this coordinate is not considered as a criteria.
        `equal`:  in conjunction with `x`, `y`, or `z`, equal to the given values
        `range`:  within range of [`xlow`,`xhigh`]

        e.g.: index_list = lc.getWitnessIndces(xcrit="any", ycrit="equal", y=0.1, zcrit="range", zlow=0.0, zhigh=0.05)
        """
        indeces = []
        if not self.witnessDefinition is None:
            #
            # Default is all points
            #
            indeces = range(self.nwit)

            #
            # Apply criteria
            #
            for ii, coord in enumerate(["x", "y", "z"]):
                crit = eval("{}crit".format(coord))

                if crit == "equal":
                    #
                    # Allow matching values
                    #
                    value = eval("{}".format(coord))
                    indeces_local = np.where(self.witnessDefinition[:, ii] == value)[0]
                    indeces = np.intersect1d(indeces_local, indeces)

                elif crit == "range":
                    #
                    # Allow range
                    #
                    low = eval("{}low".format(coord))
                    high = eval("{}high".format(coord))
                    indeces_local = np.where(self.witnessDefinition[:, ii] >= low)[0]
                    indeces = np.intersect1d(indeces_local, indeces)
                    indeces_local = np.where(self.witnessDefinition[:, ii] <= high)[0]
                    indeces = np.intersect1d(indeces_local, indeces)

        return indeces

    def getWitnessIndcesFromLocData(self):
        """
        Call self.getWitnessIndces() with the inputs from self.locdata()
        """
        #
        # Add specific value if present in configuration
        #
        for ii, coord in enumerate(["x", "y", "z"]):
            #
            # Defaults
            #
            crit = "any"
            val = 0.0
            low = -1e10
            high = 1e10

            #
            # Custom values from configuration data
            #
            key = "WitnessFilter{}crit".format(coord.upper())
            if key in self.locdata.keys():
                crit = self.locdata[key]

            key = "WitnessFilter{}value".format(coord.upper())
            if (crit == "equal") and (key in self.locdata.keys()):
                val = self.locdata[key]

            key = "WitnessFilter{}range".format(coord.upper())
            if (crit == "range") and (key in self.locdata.keys()):
                low = self.locdata[key][0]
                high = self.locdata[key][1]

            #
            # Give values to parameters
            #
            if coord == "x":
                xcrit = crit
                x = val
                xhigh = high
                xlow = low
            elif coord == "y":
                ycrit = crit
                y = val
                yhigh = high
                ylow = low
            elif coord == "z":
                zcrit = crit
                z = val
                zhigh = high
                zlow = low

        indeces = self.getWitnessIndces(
            xcrit=xcrit,
            ycrit=ycrit,
            zcrit=zcrit,
            xhigh=xhigh,
            xlow=xlow,
            yhigh=yhigh,
            ylow=ylow,
            zhigh=zhigh,
            zlow=zlow,
            x=x,
            y=y,
            z=z,
        )

        return indeces
