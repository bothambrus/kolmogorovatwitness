#################################################################################
#   KolmogorovAtWitness                                                         #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################

import copy
import glob
import json
import os

import numpy as np
from scipy import signal

from kolmogorovatwitness.nasapoly import h2Tcp
from kolmogorovatwitness.serialize import serializeDictionary
from kolmogorovatwitness.witness import (
    extractSinglePointFileWithAlyaUtils,
    getDataArrayFromAlyaTimeSetFile,
    getTimeArrayFromAlyaTimeSetFile,
)


class PointType:
    """
    Class to store and process the data of a specific point.
    """

    def __init__(
        self,
        index=None,
        coords=None,
        filterSize=None,
    ):
        #
        # Index of point in witness file
        #
        self.index = copy.deepcopy(index)

        #
        # Coordinates of this particular point
        #
        if coords is None:
            self.coords = np.array([np.nan, np.nan, np.nan])
        else:
            self.coords = np.array(coords)

        if filterSize is None:
            self.filterSize = np.nan
        else:
            self.filterSize = filterSize

        #
        # Initialize data structures
        #
        self.rawModuleData = {}
        self.processedData = {}
        self.averagedData = {}
        self.limitData = {}
        self.spectrumData = {}

    def extractSinglePointFiles(self, lc, rank=0, overwrite=False):
        """
        Extract data corresponding to this point based on the local configuration.
        """
        if (
            ("AlyaPath" in lc.globdata.keys())
            and ("Modules" in lc.locdata.keys())
            and ("ModulesWitnessFileNames" in lc.locdata.keys())
            and ("RequiredProperties" in lc.locdata.keys())
        ):
            #
            # Go through all requested modules
            #
            for m in lc.locdata["Modules"]:
                #
                # File names
                #
                fn = lc.locdata["ModulesWitnessFileNames"][m]
                entityIndex = self.index + 1

                for rp in lc.locdata["RequiredProperties"][m]:
                    #
                    # Column to extract
                    #
                    column = lc.locdata["ModulesWitnessHeaderData"][m]["VarCols"][rp]

                    newName = "tmp_{}_entity{}_var{}".format(fn, entityIndex, rp)

                    extractSinglePointFileWithAlyaUtils(
                        path=lc.locdata["CasePath"],
                        AlyaPath=lc.globdata["AlyaPath"],
                        timeSeriesFile=fn,
                        entityIndex=entityIndex,
                        column=column,
                        newName=newName,
                        rank=rank,
                        overwrite=overwrite,
                    )

    def deleteSinglePointFiles(self, lc):
        """
        Delete data files corresponding to this point.
        """
        if "CasePath" in lc.locdata.keys():
            #
            # Use regex to get all file names matching the pattern
            #
            entityIndex = self.index + 1
            for fn in glob.glob(
                os.path.join(
                    lc.locdata["CasePath"], "tmp_*_entity{}_var*".format(entityIndex)
                )
            ):
                os.remove(fn)

    def processSinglePointFiles(self, lc):
        """
        Read and process the raw data.
        """
        #
        # Read the necessary raw data
        #
        self.readRawDataFromSinglePointFiles(lc)

        #
        # Calculate material properties
        #
        self.getMaterialProperties(lc)

        #
        # Calculate chemical scales
        #
        self.getChemicalScales(lc)

        #
        # Calculate dissipation rate if needed
        #
        Tensors, Scalars = self.calculateKineticEnergyDissipationRate(lc)

        #
        # Calculate Kolmogorov scales
        #
        self.calculateKolmogorovScales(lc)

        #
        # Calculate cut-off scales
        #
        Tensors, Scalars = self.calculateCutoffScales(lc, Tensors, Scalars)

        #
        # Calculate turbulence integral scales
        #
        Tensors, Scalars = self.calculateTurbulenceIntegralScales(lc, Tensors, Scalars)

        #
        # Calculate error made by ensemble averaging the Favre-filtered velocities
        # favre_error
        #
        Tensors, Scalars = self.calculateFavreError(lc, Tensors, Scalars)

        #
        # Calculate limiting values of all time series
        #
        self.calculateLimitingValues(lc)

        #
        # Calculate spectrum of selected time series
        #
        Tensors, Scalars = self.calculateSpectra(lc, Tensors, Scalars)

        #
        # Save Averaged data
        #
        self.saveAveragedData(lc)

        #
        # Save Limit data
        #
        self.saveLimitData(lc)

        #
        # Save temporal data
        #
        self.saveTemporalData(lc)

        #
        # Save spectra data
        #
        self.saveSpectrumData(lc)

    def readRawDataFromSinglePointFiles(self, lc):
        """
        Read the data form the extracted data files corresponding to this point.
        """
        if (
            ("Modules" in lc.locdata.keys())
            and ("ModulesWitnessFileNames" in lc.locdata.keys())
            and ("RequiredProperties" in lc.locdata.keys())
        ):
            entityIndex = self.index + 1

            #
            # Initialize raw module data
            #
            for m in lc.locdata["Modules"]:
                if not m in self.rawModuleData.keys():
                    self.rawModuleData[m] = {}
            if not "general" in self.rawModuleData.keys():
                self.rawModuleData["general"] = {}

            #
            # Find any file to read for time processing
            #
            fn = None
            foundFile = False
            for m in lc.locdata["Modules"]:
                for rp in lc.locdata["RequiredProperties"][m]:
                    fn = "tmp_{}_entity{}_var{}".format(
                        lc.locdata["ModulesWitnessFileNames"][m], entityIndex, rp
                    )
                    foundFile = True
                    break
                if foundFile:
                    break

            #
            # Read time
            #
            if not fn is None:
                t_min = 0.0
                t_max = np.inf

                if "TimeInitial" in lc.locdata.keys():
                    t_min = lc.locdata["TimeInitial"]

                if "TimeFinal" in lc.locdata.keys():
                    t_max = lc.locdata["TimeFinal"]

                (
                    self.rawModuleData["general"]["time"],
                    self.rawModuleData["general"]["mask"],
                ) = getTimeArrayFromAlyaTimeSetFile(
                    path=lc.locdata["CasePath"],
                    extractedTimeSeriesFile=fn,
                    t_min=t_min,
                    t_max=t_max,
                )

                #
                # Get dt for averaging
                #
                self.rawModuleData["general"]["dt"] = np.diff(
                    self.rawModuleData["general"]["time"]
                )

            #
            # Read raw data
            #
            for m in lc.locdata["Modules"]:
                for rp in lc.locdata["RequiredProperties"][m]:
                    fn = "tmp_{}_entity{}_var{}".format(
                        lc.locdata["ModulesWitnessFileNames"][m], entityIndex, rp
                    )

                    self.rawModuleData[m][rp] = getDataArrayFromAlyaTimeSetFile(
                        path=lc.locdata["CasePath"],
                        extractedTimeSeriesFile=fn,
                        mask=self.rawModuleData["general"]["mask"],
                    )

    def getMaterialProperties(self, lc):
        """
        Get material properties corresponding to the local state.
        """
        if (
            "RequiredProperties" in lc.locdata.keys()
            and "PropertyMethod" in lc.locdata.keys()
        ):
            #
            # Constant
            #
            if lc.locdata["PropertyMethod"] == "constant":
                #
                # same structure as with variable properties
                #
                ntime = len(self.rawModuleData["general"]["time"])

                #
                # Viscosity
                #
                if "viscosity" in lc.locdata["RequiredProperties"]["general"]:
                    if "PropertyConstKinematicViscosity" in lc.locdata.keys():
                        self.processedData["nu"] = (
                            np.ones([ntime])
                            * lc.locdata["PropertyConstKinematicViscosity"]
                        )

                        if "PropertyConstDensity" in lc.locdata.keys():
                            self.processedData["mu"] = np.ones([ntime]) * (
                                lc.locdata["PropertyConstKinematicViscosity"]
                                * lc.locdata["PropertyConstDensity"]
                            )

                #
                # Density
                #
                if "density" in lc.locdata["RequiredProperties"]["general"]:
                    if "PropertyConstDensity" in lc.locdata.keys():
                        self.processedData["rho"] = (
                            np.ones([ntime]) * lc.locdata["PropertyConstDensity"]
                        )

                if "density_Fv" in lc.locdata["RequiredProperties"]["general"]:
                    if "PropertyConstDensity" in lc.locdata.keys():
                        self.processedData["rho_Fv"] = self.processedData["rho"]

            elif lc.locdata["PropertyMethod"] == "tabulated":
                #
                # Lookup
                #
                tab = lc.locdata["PropertyLookupTableObject"]

                #
                # Scale control variables if necessary
                #
                if (
                    "PropertyControlVariableMethod" in lc.locdata.keys()
                    and lc.locdata["PropertyControlVariableMethod"][:5] == "CONCE"
                ):
                    #
                    # Adiabatic premixed table
                    #
                    if (
                        lc.locdata["PropertyControlVariableMethod"]
                        == "CONCE_perfectly_premixed"
                    ):
                        #
                        # C
                        #
                        i_unsc = lc.locdata[
                            "PropertyControlVariableSconcIndexToConceIndex"
                        ][0]
                        self.rawModuleData["chm"]["SCO01"] = copy.copy(
                            self.rawModuleData["chm"]["CON{:02d}".format(i_unsc)]
                        )

                        #
                        # Cvar
                        #
                        i_unsc = lc.locdata[
                            "PropertyControlVariableSconcIndexToConceIndex"
                        ][1]
                        self.rawModuleData["chm"]["SCO02"] = copy.deepcopy(
                            self.rawModuleData["chm"]["CON{:02d}".format(i_unsc)]
                        )
                        self.rawModuleData["chm"]["SCO02"] /= (
                            np.where(
                                self.rawModuleData["chm"]["SCO01"] >= 0.0,
                                self.rawModuleData["chm"]["SCO01"],
                                0.0,
                            )
                            * (
                                1.0
                                - np.where(
                                    self.rawModuleData["chm"]["SCO01"] >= 0.0,
                                    self.rawModuleData["chm"]["SCO01"],
                                    0.0,
                                )
                            )
                            + 1e-12
                        )

                #
                # Assemble control variables
                #
                ntime = len(self.rawModuleData["general"]["time"])
                control = copy.deepcopy(
                    np.where(
                        self.rawModuleData["chm"]["SCO01"][0:ntime]
                        > tab.coords[tab.coordKeys[0]][-1],
                        tab.coords[tab.coordKeys[0]][-1],
                        np.where(
                            self.rawModuleData["chm"]["SCO01"][0:ntime]
                            < tab.coords[tab.coordKeys[0]][0],
                            tab.coords[tab.coordKeys[0]][0],
                            self.rawModuleData["chm"]["SCO01"][0:ntime],
                        ),
                    )
                ).reshape(ntime, 1)
                for ii, ck in enumerate(
                    lc.locdata["PropertyLookupTableObject"].coordKeys[1:]
                ):
                    control = np.append(
                        control,
                        np.where(
                            self.rawModuleData["chm"]["SCO{:02d}".format(ii + 2)][
                                0:ntime
                            ]
                            > tab.coords[tab.coordKeys[ii + 1]][-1],
                            tab.coords[tab.coordKeys[ii + 1]][-1],
                            np.where(
                                self.rawModuleData["chm"]["SCO{:02d}".format(ii + 2)][
                                    0:ntime
                                ]
                                < tab.coords[tab.coordKeys[ii + 1]][0],
                                tab.coords[tab.coordKeys[ii + 1]][0],
                                self.rawModuleData["chm"]["SCO{:02d}".format(ii + 2)][
                                    0:ntime
                                ],
                            ),
                        ).reshape(ntime, 1),
                        axis=1,
                    )

                #
                # Interpolate
                #
                data = tab.interpolateArray(control, key_list=tab.keys(), verbose=False)

                #
                # Determine molecular weight key
                #
                molecKey = None
                if "WStar" in tab.keys():
                    molecKey = "WStar"
                elif "W" in tab.keys():
                    molecKey = "W"

                #
                # Get Favre-filtered denisty
                #
                if "density_Fv" in lc.locdata["RequiredProperties"]["general"]:
                    rho_Fv_data = lc.locdata[
                        "FavreDensityLookupTableObject"
                    ].interpolateArray(control, key_list=["rho_Fv"], verbose=False)
                    self.processedData["rho_Fv"] = rho_Fv_data["rho_Fv"]

                #
                # Get density
                #
                if "density" in lc.locdata["RequiredProperties"]["general"]:
                    #
                    # Density methods
                    #
                    if lc.locdata["PropertyDensityMethod"] == "EoS_T_from_temper":
                        #
                        # Density from P/rho = Ru/W T
                        #
                        self.processedData["rho"] = (
                            lc.locdata["PropertyConstPressure"]
                            * data[molecKey]
                            / (8.31446261815324 * self.rawModuleData["tem"]["TEMPE"])
                        )

                    elif (
                        lc.locdata["PropertyDensityMethod"]
                        == "EoS_H_from_temper_to_NASA_poly"
                    ):
                        #
                        # Density from P/rho = Ru/W T
                        # Where T is computed from H from temper
                        # Using the NASA polynomials
                        #
                        h = self.rawModuleData["tem"]["ENTHA"]
                        cpLT = np.transpose(
                            np.array(
                                [
                                    data["CP1_1"],
                                    data["CP1_2"],
                                    data["CP1_3"],
                                    data["CP1_4"],
                                    data["CP1_5"],
                                    data["CP1_6"],
                                ]
                            )
                        )
                        cpHT = np.transpose(
                            np.array(
                                [
                                    data["CP2_1"],
                                    data["CP2_2"],
                                    data["CP2_3"],
                                    data["CP2_4"],
                                    data["CP2_5"],
                                    data["CP2_6"],
                                ]
                            )
                        )
                        T, cp = h2Tcp(cpLT, cpHT, h)
                        self.processedData["rho"] = (
                            lc.locdata["PropertyConstPressure"]
                            * data[molecKey]
                            / (8.31446261815324 * T)
                        )

                    elif (
                        lc.locdata["PropertyDensityMethod"]
                        == "EoS_H_from_SCONC_to_NASA_poly"
                    ):
                        #
                        # Lookup
                        #
                        htab = lc.locdata["PropertyLookupEnthalpyScalingTableObject"]

                        #
                        # Assemble control variables
                        #
                        ntime = len(self.rawModuleData["general"]["time"])
                        control = copy.deepcopy(
                            np.where(
                                self.rawModuleData["chm"]["SCO01"][0:ntime]
                                > tab.coords[tab.coordKeys[0]][-1],
                                tab.coords[tab.coordKeys[0]][-1],
                                np.where(
                                    self.rawModuleData["chm"]["SCO01"][0:ntime]
                                    < tab.coords[tab.coordKeys[0]][0],
                                    tab.coords[tab.coordKeys[0]][0],
                                    self.rawModuleData["chm"]["SCO01"][0:ntime],
                                ),
                            )
                        ).reshape(ntime, 1)
                        for ii, ck in enumerate(
                            lc.locdata["PropertyLookupTableObject"].coordKeys[1:-1]
                        ):
                            control = np.append(
                                control,
                                np.where(
                                    self.rawModuleData["chm"][
                                        "SCO{:02d}".format(ii + 2)
                                    ][0:ntime]
                                    > tab.coords[tab.coordKeys[ii + 1]][-1],
                                    tab.coords[tab.coordKeys[ii + 1]][-1],
                                    np.where(
                                        self.rawModuleData["chm"][
                                            "SCO{:02d}".format(ii + 2)
                                        ][0:ntime]
                                        < tab.coords[tab.coordKeys[ii + 1]][0],
                                        tab.coords[tab.coordKeys[ii + 1]][0],
                                        self.rawModuleData["chm"][
                                            "SCO{:02d}".format(ii + 2)
                                        ][0:ntime],
                                    ),
                                ).reshape(ntime, 1),
                                axis=1,
                            )

                        #
                        # Interpolate
                        #
                        hdata = htab.interpolateArray(
                            control, key_list=htab.keys(), verbose=False
                        )

                        sco_h = self.rawModuleData["chm"]["SCO{:02d}".format(tab.dims)][
                            0:ntime
                        ]
                        h = hdata["Hmin"] + sco_h * (hdata["Hmax"] - hdata["Hmin"])

                        cpLT = np.transpose(
                            np.array(
                                [
                                    data["CP1_1"],
                                    data["CP1_2"],
                                    data["CP1_3"],
                                    data["CP1_4"],
                                    data["CP1_5"],
                                    data["CP1_6"],
                                ]
                            )
                        )
                        cpHT = np.transpose(
                            np.array(
                                [
                                    data["CP2_1"],
                                    data["CP2_2"],
                                    data["CP2_3"],
                                    data["CP2_4"],
                                    data["CP2_5"],
                                    data["CP2_6"],
                                ]
                            )
                        )
                        T, cp = h2Tcp(cpLT, cpHT, h)
                        self.processedData["rho"] = (
                            lc.locdata["PropertyConstPressure"]
                            * data[molecKey]
                            / (8.31446261815324 * T)
                        )

                    elif lc.locdata["PropertyDensityMethod"] == "from_chemic":
                        #
                        # Density directly from chemic witness file
                        #
                        self.processedData["rho"] = copy.copy(
                            self.rawModuleData["chm"]["DENSI"]
                        )

                #
                # Get viscosity
                #
                if "viscosity" in lc.locdata["RequiredProperties"]["general"]:
                    self.processedData["mu"] = data["mu"]
                    self.processedData["nu"] = (
                        self.processedData["mu"] / self.processedData["rho"]
                    )

            #
            # Get sub-grid scale (SGS) viscosity
            #
            if "sgs_viscosity" in lc.locdata["RequiredProperties"]["general"]:
                self.processedData["mu_sgs"] = self.rawModuleData["nsi"]["TURBU"]
                self.processedData["nu_sgs"] = (
                    self.processedData["mu_sgs"] / self.processedData["rho"]
                )

            ###################
            # Mean properties #
            ###################
            for typ in ["Re", "Fv"]:
                for k in ["nu", "mu", "rho", "nu_sgs", "mu_sgs", "rho_Fv"]:
                    if k in self.processedData.keys():
                        self.addAveragedValues(
                            parentKey="processed",
                            key=k,
                            array=self.processedData[k],
                            typ=typ,
                            arrtyp="1D",
                        )

            ####################
            # Mean of raw data #
            ####################
            for m in lc.locdata["Modules"]:
                for rp in self.rawModuleData[m].keys():
                    for typ in ["Re", "Fv"]:
                        self.addAveragedValues(
                            parentKey=m,
                            key=rp,
                            array=self.rawModuleData[m][rp],
                            typ=typ,
                            arrtyp="1D",
                        )

    def getChemicalScales(self, lc):
        """
        Get chemical scales corresponding to the local state.
        """
        if (
            "ChemicalScalesList" in lc.locdata.keys()
            and "ChemicalScalesMethod" in lc.locdata.keys()
        ):
            #
            # Constant
            #
            if lc.locdata["ChemicalScalesMethod"] == "constant":
                #
                # same structure as with variable properties
                #
                ntime = len(self.rawModuleData["general"]["time"])
                #
                # Get constant properties
                #
                for prop in lc.locdata["ChemicalScalesList"]:
                    key = "ChemicalScalesConst_{}".format(prop)

                    if key in lc.locdata.keys():
                        self.processedData[prop] = np.ones([ntime]) * lc.locdata[key]

            #
            # Calculate third property from other two
            #
            if ("S_L" in lc.locdata["ChemicalScalesList"]) and (
                "delta_diff" in lc.locdata["ChemicalScalesList"]
            ):
                self.processedData["tau_c"] = (
                    self.processedData["delta_diff"] / self.processedData["S_L"]
                )
            elif ("tau_c" in lc.locdata["ChemicalScalesList"]) and (
                "delta_diff" in lc.locdata["ChemicalScalesList"]
            ):
                self.processedData["S_L"] = (
                    self.processedData["delta_diff"] / self.processedData["tau_c"]
                )
            elif ("tau_c" in lc.locdata["ChemicalScalesList"]) and (
                "S_L" in lc.locdata["ChemicalScalesList"]
            ):
                self.processedData["delta_diff"] = (
                    self.processedData["S_L"] * self.processedData["tau_c"]
                )

            #
            # Derived properties
            #
            self.processedData["freq_c"] = 1.0 / self.processedData["tau_c"]

            self.processedData["kappa_diff"] = (
                2.0 * np.pi / self.processedData["delta_diff"]
            )

            ###################
            # Mean properties #
            ###################
            for typ in ["Re", "Fv"]:
                for k in ["S_L", "tau_c", "delta_diff", "freq_c", "kappa_diff"]:
                    if k in self.processedData.keys():
                        self.addAveragedValues(
                            parentKey="processed",
                            key=k,
                            array=self.processedData[k],
                            typ=typ,
                            arrtyp="1D",
                        )

    def calculateKineticEnergyDissipationRate(self, lc):
        """
        Calculate local kinetic energy dissipation rate (resolved and subgrid.)
        """
        Tensors = {}
        Scalars = {}
        if "Tasks" in lc.locdata.keys():
            #
            # Check if kinetic energy dissipation is needed
            #
            if ("kolmogorov" in lc.locdata["Tasks"]) or (
                "integral" in lc.locdata["Tasks"]
            ):
                #
                # Auxiliary variables
                #
                ntime = len(self.rawModuleData["general"]["time"])

                ###############################################
                # Assemble instanteneous resolved strain rate #
                ###############################################
                #
                # Diagonal terms
                #
                Sij = copy.deepcopy(self.rawModuleData["nsi"]["S11"][0:ntime]).reshape(
                    ntime, 1
                )
                for ii in range(2):
                    Sij = np.append(
                        Sij,
                        self.rawModuleData["nsi"]["S{}{}".format(ii + 2, ii + 2)][
                            0:ntime
                        ].reshape(ntime, 1),
                        axis=1,
                    )

                #
                # Off-diagonal terms
                #
                for ii, jj in zip([1, 2, 1], [2, 3, 3]):
                    Sij = np.append(
                        Sij,
                        self.rawModuleData["nsi"]["S{}{}".format(ii, jj)][
                            0:ntime
                        ].reshape(ntime, 1),
                        axis=1,
                    )

                #######################
                # Velocity divergence #
                #######################
                divU = np.sum(Sij[:, 0:3], axis=1)

                #########################################################
                # Deviatoric part of instanteneous resolved strain rate #
                #########################################################
                SijD = copy.deepcopy(Sij)
                SijD[:, 0:3] = SijD[:, 0:3] - 1.0 / 3.0 * np.multiply.outer(
                    divU[:],
                    np.ones([3]),
                )

                ######################
                # Strain rate tensor #
                ######################
                Tau_resolved = (
                    2.0
                    * np.multiply.outer(self.processedData["mu"], np.ones([6]))
                    * SijD
                )
                TauD_sgs = (
                    2.0
                    * np.multiply.outer(self.processedData["mu_sgs"], np.ones([6]))
                    * SijD
                )
                Tau = Tau_resolved + TauD_sgs
                TauPerRho = Tau / np.multiply.outer(
                    self.processedData["rho"], np.ones([6])
                )

                #
                # Gather all these temporally changing tensors for easier handling
                #
                Tensors = dict(
                    Sij=Sij,
                    divU=divU,
                    SijD=SijD,
                    Tau_resolved=Tau_resolved,
                    TauD_sgs=TauD_sgs,
                    Tau=Tau,
                    TauPerRho=TauPerRho,
                )

                #####################
                # Mean strain rates #
                #####################
                for typ in ["Re", "Fv"]:
                    for key, arrtyp in zip(
                        [
                            "Sij",
                            "SijD",
                            "divU",
                            "Tau_resolved",
                            "TauD_sgs",
                            "Tau",
                            "TauPerRho",
                        ],
                        ["2D", "2D", "1D", "2D", "2D", "2D", "2D"],
                    ):
                        self.addAveragedValues(
                            parentKey="dissipationRate",
                            key=key,
                            array=Tensors[key],
                            typ=typ,
                            arrtyp=arrtyp,
                        )

                ###############################
                # Save mean dissipation rates #
                ###############################
                for typ in ["Re", "Fv"]:
                    #
                    # Left term is strain rate
                    #
                    left = self.averagedData[typ]["dissipationRate"]["Sij"]

                    #
                    # Right term is a stress tensor
                    #
                    for rkey, key in zip(
                        ["Tau_resolved", "TauD_sgs", "Tau"],
                        ["rho_eps_mean_resolved", "rho_eps_mean_sgs", "rho_eps_mean"],
                    ):
                        #
                        # Multiply
                        #
                        right = self.averagedData[typ]["dissipationRate"][rkey]
                        multip = left[0:6] * right[0:6]

                        self.averagedData[typ]["dissipationRate"][key] = np.sum(
                            multip[0:3]
                        ) + 2.0 * np.sum(multip[3:6])

                #############################################
                # Compute fluctuating components of tensors #
                #############################################
                for typ in ["Re", "Fv"]:
                    extra = "_f{}".format(typ)

                    for key in [
                        "Sij",
                        "SijD",
                        "Tau_resolved",
                        "TauD_sgs",
                        "Tau",
                        "TauPerRho",
                    ]:
                        newkey = key + extra

                        #
                        # Add fluctuations
                        #
                        Tensors[newkey] = Tensors[key] - np.multiply.outer(
                            np.ones([ntime]),
                            self.averagedData[typ]["dissipationRate"][key],
                        )

                ###########################################################
                # Compute instanteneous and average double inner products #
                ###########################################################
                lefts = []
                rights = []
                names = []
                Scalars = {}

                lefts.append("Sij"), rights.append("Sij"), names.append(
                    lefts[-1] + "_DOT_" + rights[-1]
                )
                lefts.append("Sij"), rights.append("SijD"), names.append(
                    lefts[-1] + "_DOT_" + rights[-1]
                )
                lefts.append("Sij"), rights.append("Tau_resolved"), names.append(
                    lefts[-1] + "_DOT_" + rights[-1]
                )
                lefts.append("Sij"), rights.append("TauD_sgs"), names.append(
                    lefts[-1] + "_DOT_" + rights[-1]
                )
                lefts.append("Sij"), rights.append("Tau"), names.append(
                    lefts[-1] + "_DOT_" + rights[-1]
                )
                lefts.append("Sij"), rights.append("TauPerRho"), names.append(
                    lefts[-1] + "_DOT_" + rights[-1]
                )
                for typ in ["Re", "Fv"]:
                    extra = "_f{}".format(typ)
                    lefts.append("Sij" + extra), rights.append(
                        "Sij" + extra
                    ), names.append(lefts[-1] + "_DOT_" + rights[-1])
                    lefts.append("Sij" + extra), rights.append(
                        "SijD" + extra
                    ), names.append(lefts[-1] + "_DOT_" + rights[-1])
                    lefts.append("Sij" + extra), rights.append(
                        "Tau_resolved" + extra
                    ), names.append(lefts[-1] + "_DOT_" + rights[-1])
                    lefts.append("Sij" + extra), rights.append(
                        "TauD_sgs" + extra
                    ), names.append(lefts[-1] + "_DOT_" + rights[-1])
                    lefts.append("Sij" + extra), rights.append(
                        "Tau" + extra
                    ), names.append(lefts[-1] + "_DOT_" + rights[-1])
                    lefts.append("Sij" + extra), rights.append(
                        "TauPerRho" + extra
                    ), names.append(lefts[-1] + "_DOT_" + rights[-1])

                for left, right, name in zip(lefts, rights, names):
                    multip = Tensors[left][:, 0:6] * Tensors[right][:, 0:6]

                    #
                    # Double inner product
                    #
                    Scalars[name] = np.sum(multip[:, 0:3], axis=1) + 2.0 * np.sum(
                        multip[:, 3:6], axis=1
                    )

                    #
                    # Average them
                    #
                    for typ in ["Re", "Fv"]:
                        self.addAveragedValues(
                            parentKey="dissipationRate",
                            key=name,
                            array=Scalars[name],
                            typ=typ,
                            arrtyp="1D",
                        )

                ############################
                # Save meaningful epsilons #
                ############################
                #
                # Dissipation of TKE
                #
                self.averagedData["Fv"]["dissipationRate"]["eps"] = (
                    self.averagedData["Re"]["dissipationRate"]["Sij_fRe_DOT_Tau_fRe"]
                    / self.averagedData["Re"]["processed"]["rho"]
                )
                self.averagedData["Fv"]["dissipationRate"]["eps_res"] = (
                    self.averagedData["Re"]["dissipationRate"][
                        "Sij_fRe_DOT_Tau_resolved_fRe"
                    ]
                    / self.averagedData["Re"]["processed"]["rho"]
                )
                self.averagedData["Fv"]["dissipationRate"]["eps_sgs"] = (
                    self.averagedData["Re"]["dissipationRate"][
                        "Sij_fRe_DOT_TauD_sgs_fRe"
                    ]
                    / self.averagedData["Re"]["processed"]["rho"]
                )

                #
                # Total dissipation of kinetic energy
                #
                self.averagedData["Fv"]["dissipationRate"]["eps_tot"] = (
                    self.averagedData["Re"]["dissipationRate"]["Sij_DOT_Tau"]
                    / self.averagedData["Re"]["processed"]["rho"]
                )
                self.averagedData["Fv"]["dissipationRate"]["eps_tot_res"] = (
                    self.averagedData["Re"]["dissipationRate"]["Sij_DOT_Tau_resolved"]
                    / self.averagedData["Re"]["processed"]["rho"]
                )
                self.averagedData["Fv"]["dissipationRate"]["eps_tot_sgs"] = (
                    self.averagedData["Re"]["dissipationRate"]["Sij_DOT_TauD_sgs"]
                    / self.averagedData["Re"]["processed"]["rho"]
                )

                #
                # Dissipation of mean field
                #
                self.averagedData["Fv"]["dissipationRate"]["eps_mean"] = (
                    self.averagedData["Re"]["dissipationRate"]["rho_eps_mean"]
                    / self.averagedData["Re"]["processed"]["rho"]
                )
                self.averagedData["Fv"]["dissipationRate"]["eps_mean_res"] = (
                    self.averagedData["Re"]["dissipationRate"]["rho_eps_mean_resolved"]
                    / self.averagedData["Re"]["processed"]["rho"]
                )
                self.averagedData["Fv"]["dissipationRate"]["eps_mean_sgs"] = (
                    self.averagedData["Re"]["dissipationRate"]["rho_eps_mean_sgs"]
                    / self.averagedData["Re"]["processed"]["rho"]
                )

                #
                # Incompressible limit
                #
                self.averagedData["Re"]["dissipationRate"]["eps"] = self.averagedData[
                    "Re"
                ]["dissipationRate"]["Sij_fRe_DOT_TauPerRho_fRe"]

        return Tensors, Scalars

    def calculateKolmogorovScales(self, lc):
        """
        Calculate Kolmogorov scale associated to witness point.
        """
        if "Tasks" in lc.locdata.keys():
            if "kolmogorov" in lc.locdata["Tasks"]:
                #
                # Add dictionary
                #
                if not "kolmogorov" in self.averagedData.keys():
                    self.averagedData["kolmogorov"] = {}

                #
                # Kolmogorov scales
                #
                self.averagedData["kolmogorov"]["l_K"] = (
                    (self.averagedData["Fv"]["processed"]["nu"]) ** 3.0
                    / self.averagedData["Fv"]["dissipationRate"]["eps"]
                ) ** 0.25
                self.averagedData["kolmogorov"]["t_K"] = (
                    self.averagedData["Fv"]["processed"]["nu"]
                    / self.averagedData["Fv"]["dissipationRate"]["eps"]
                ) ** 0.5
                self.averagedData["kolmogorov"]["v_K"] = (
                    self.averagedData["Fv"]["processed"]["nu"]
                    * self.averagedData["Fv"]["dissipationRate"]["eps"]
                ) ** 0.25

                #
                # Incompressible Kolmogorov scales
                #
                self.averagedData["kolmogorov"]["l_K_inco"] = (
                    (self.averagedData["Re"]["processed"]["nu"]) ** 3.0
                    / self.averagedData["Re"]["dissipationRate"]["eps"]
                ) ** 0.25
                self.averagedData["kolmogorov"]["t_K_inco"] = (
                    self.averagedData["Re"]["processed"]["nu"]
                    / self.averagedData["Re"]["dissipationRate"]["eps"]
                ) ** 0.5
                self.averagedData["kolmogorov"]["v_K_inco"] = (
                    self.averagedData["Re"]["processed"]["nu"]
                    * self.averagedData["Re"]["dissipationRate"]["eps"]
                ) ** 0.25

    def calculateCutoffScales(self, lc, Tensors=None, Scalars=None):
        """
        Calculate cut-off scales associated to witness point.
        """
        if Tensors is None:
            Tensors = {}
        if Scalars is None:
            Scalars = {}

        if "Tasks" in lc.locdata.keys():
            #
            # Check if kinetic energy dissipation is needed
            #
            if ("cutoff" in lc.locdata["Tasks"]) or ("integral" in lc.locdata["Tasks"]):
                #
                # Add dictionary
                #
                if not "cutoff" in self.averagedData.keys():
                    self.averagedData["cutoff"] = {}

                #
                # Calculate cut-off properties
                #
                self.averagedData["cutoff"]["l_filter"] = self.filterSize
                self.averagedData["cutoff"]["kappa_filter"] = (
                    2.0 * np.pi / self.filterSize
                )
                self.averagedData["cutoff"]["l_cutoff"] = 2.0 * self.filterSize
                self.averagedData["cutoff"]["kappa_cutoff"] = np.pi / self.filterSize

                #
                # Relate to Kolmogorov scales
                #
                if "kolmogorov" in self.averagedData.keys():
                    self.averagedData["cutoff"]["rati_l_filter_l_K"] = (
                        self.averagedData["cutoff"]["l_filter"]
                        / self.averagedData["kolmogorov"]["l_K"]
                    )
                    self.averagedData["cutoff"]["rati_l_cutoff_l_K"] = (
                        self.averagedData["cutoff"]["l_cutoff"]
                        / self.averagedData["kolmogorov"]["l_K"]
                    )

                #
                # Relate to Integral scales
                #
                if "integral" in self.averagedData.keys():
                    self.averagedData["cutoff"]["rati_l_filter_l_t"] = (
                        self.averagedData["cutoff"]["l_filter"]
                        / self.averagedData["integral"]["l_t"]
                    )
                    self.averagedData["cutoff"]["rati_l_cutoff_l_t"] = (
                        self.averagedData["cutoff"]["l_cutoff"]
                        / self.averagedData["integral"]["l_t"]
                    )

                #
                # Instantaneous SGS TKE
                #
                if "Sij_DOT_Sij" in Scalars.keys():
                    #
                    # According to Bogey and Bailly (2006)
                    #
                    CI = 0.01
                    Scalars["ek_sgs_Bogey"] = (
                        2 * CI * self.filterSize**2 * Scalars["Sij_DOT_Sij"]
                    )

                    #
                    # According to Yoshizawa (1994)
                    #
                    CI = 0.0886
                    Scalars["ek_sgs_Yoshizawa"] = (
                        2 * CI * self.filterSize**2 * Scalars["Sij_DOT_Sij"]
                    )

                    #
                    # Instantaneous SGS velocity scale
                    #
                    Scalars["v_sgs_Yoshizawa"] = np.sqrt(
                        2 * Scalars["ek_sgs_Yoshizawa"]
                    )

                    #
                    # Mean SGS TKE
                    #
                    for typ in ["Re", "Fv"]:
                        for arr, key in zip(
                            [
                                Scalars["ek_sgs_Bogey"],
                                Scalars["ek_sgs_Yoshizawa"],
                                Scalars["v_sgs_Yoshizawa"],
                            ],
                            ["ek_sgs_Bogey", "ek_sgs_Yoshizawa", "v_sgs_Yoshizawa"],
                        ):
                            self.addAveragedValues(
                                parentKey="cutoff",
                                key=key,
                                array=arr,
                                typ=typ,
                                arrtyp="1D",
                            )

        return Tensors, Scalars

    def calculateFavreError(self, lc, Tensors=None, Scalars=None):
        """
        Calculate error made by ensemble averaging the Favre-filtered velocities on the witness point.
        """
        if Tensors is None:
            Tensors = {}
        if Scalars is None:
            Scalars = {}

        if "Tasks" in lc.locdata.keys():
            #
            # Check if kinetic energy dissipation is needed
            #
            if "favre_error" in lc.locdata["Tasks"]:
                #
                # Add dictionary
                #
                if not "favre_error" in self.averagedData.keys():
                    self.averagedData["favre_error"] = {}

                if ("rho" in self.processedData.keys()) and (
                    "rho_Fv" in self.processedData.keys()
                ):
                    #
                    # Instantaneous sub-grid density variance
                    #
                    Scalars["rho_var_sgs"] = self.processedData["rho"] * (
                        self.processedData["rho_Fv"] - self.processedData["rho"]
                    )

                    #
                    # Mean SGS TKE
                    #
                    for typ in ["Re", "Fv"]:
                        for arr, key in zip(
                            [
                                Scalars["rho_var_sgs"],
                            ],
                            ["rho_var_sgs"],
                        ):
                            self.addAveragedValues(
                                parentKey="favre_error",
                                key=key,
                                array=arr,
                                typ=typ,
                                arrtyp="1D",
                            )

                    #
                    # Instantaneous Favre / non-density-weighted error
                    #
                    if "v_sgs_Yoshizawa" in Scalars.keys():
                        Scalars["error_veloc_Fv_Re_Yoshizawa"] = Scalars[
                            "v_sgs_Yoshizawa"
                        ] * (
                            self.processedData["rho_Fv"] / self.processedData["rho"]
                            - 1.0
                        )

                        #
                        # Mean SGS TKE
                        #
                        for typ in ["Re", "Fv"]:
                            for arr, key in zip(
                                [
                                    Scalars["error_veloc_Fv_Re_Yoshizawa"],
                                ],
                                ["error_veloc_Fv_Re_Yoshizawa"],
                            ):
                                self.addAveragedValues(
                                    parentKey="favre_error",
                                    key=key,
                                    array=arr,
                                    typ=typ,
                                    arrtyp="1D",
                                )
                        #
                        # Save error
                        #
                        self.averagedData["favre_error"][
                            "error_veloc_Fv_Re_Yoshizawa"
                        ] = self.averagedData["Re"]["favre_error"][
                            "error_veloc_Fv_Re_Yoshizawa"
                        ]

        return Tensors, Scalars

    def calculateTurbulenceIntegralScales(self, lc, Tensors=None, Scalars=None):
        """
        Calculate turbulence integral scales associated to witness point.
        Following Chapter 2 of Fox: Computational models for turbulent reating flows (2003)
        """
        if Tensors is None:
            Tensors = {}
        if Scalars is None:
            Scalars = {}

        if "Tasks" in lc.locdata.keys():
            if "integral" in lc.locdata["Tasks"]:
                #
                # Add dictionary
                #
                if not "integral" in self.averagedData.keys():
                    self.averagedData["integral"] = {}

                #
                # Auxiliary variables for temporal averaging
                #
                ntime = len(self.rawModuleData["general"]["time"])

                #
                # Assemble velocity vector
                #
                Tensors["veloc"] = copy.deepcopy(
                    self.rawModuleData["nsi"]["VELOX"][0:ntime]
                ).reshape(ntime, 1)
                for ax in ["Y", "Z"]:
                    Tensors["veloc"] = np.append(
                        Tensors["veloc"],
                        self.rawModuleData["nsi"]["VELO{}".format(ax)][0:ntime].reshape(
                            ntime, 1
                        ),
                        axis=1,
                    )

                #
                # Mean velocity
                #
                for typ in ["Re", "Fv"]:
                    self.addAveragedValues(
                        parentKey="integral",
                        key="veloc",
                        array=Tensors["veloc"],
                        typ=typ,
                        arrtyp="2D",
                    )

                #
                # Velocity magnitude
                #
                for typ in ["Re", "Fv"]:
                    self.averagedData[typ]["integral"]["mag_veloc"] = (
                        np.sum(
                            np.power(self.averagedData[typ]["integral"]["veloc"], 2.0)
                        )
                        ** 0.5
                    )

                #
                # Fluctuations
                #
                Tensors["veloc_fRe"] = Tensors["veloc"][:, 0:3] - np.multiply.outer(
                    np.ones([ntime]), self.averagedData["Re"]["integral"]["veloc"]
                )
                Tensors["veloc_fFv"] = Tensors["veloc"][:, 0:3] - np.multiply.outer(
                    np.ones([ntime]), self.averagedData["Fv"]["integral"]["veloc"]
                )

                #
                # Instantaneous kinetic energy and turbulent kinetic energy
                #
                ek_res_inst = 0.5 * np.sum(Tensors["veloc"] * Tensors["veloc"], axis=1)
                k_res_inst_fRe = 0.5 * np.sum(
                    Tensors["veloc_fRe"] * Tensors["veloc_fRe"], axis=1
                )
                k_res_inst_fFv = 0.5 * np.sum(
                    Tensors["veloc_fFv"] * Tensors["veloc_fFv"], axis=1
                )

                #
                # Mean resolved TKE
                #
                for typ in ["Re", "Fv"]:
                    for arr, key in zip(
                        [ek_res_inst, k_res_inst_fRe, k_res_inst_fFv],
                        ["ek_res", "k_res_fRe", "k_res_fFv"],
                    ):
                        self.addAveragedValues(
                            parentKey="integral",
                            key=key,
                            array=arr,
                            typ=typ,
                            arrtyp="1D",
                        )

                #
                # Integral lenght scales
                #
                self.averagedData["integral"]["v_t"] = (
                    self.averagedData["Fv"]["integral"]["k_res_fFv"]
                ) ** 0.5
                self.averagedData["integral"]["t_t"] = (
                    self.averagedData["Fv"]["integral"]["k_res_fFv"]
                    / self.averagedData["Fv"]["dissipationRate"]["eps"]
                )
                self.averagedData["integral"]["l_t"] = (
                    self.averagedData["integral"]["v_t"]
                    * self.averagedData["integral"]["t_t"]
                )
                self.averagedData["integral"]["Re_t"] = (
                    self.averagedData["integral"]["v_t"]
                    * self.averagedData["integral"]["l_t"]
                ) / self.averagedData["Fv"]["processed"]["nu"]

                #
                # Instantaneous SGS TKE
                #
                if "ek_sgs_Yoshizawa" in Scalars.keys():
                    ek = ek_res_inst + Scalars["ek_sgs_Yoshizawa"]
                    k_inst_fRe = k_res_inst_fRe + Scalars["ek_sgs_Yoshizawa"]
                    k_inst_fFv = k_res_inst_fFv + Scalars["ek_sgs_Yoshizawa"]

                    #
                    # Mean SGS TKE
                    #
                    for typ in ["Re", "Fv"]:
                        for arr, key in zip(
                            [ek, k_inst_fRe, k_inst_fFv],
                            ["ek", "k_fRe", "k_fFv"],
                        ):
                            self.addAveragedValues(
                                parentKey="integral",
                                key=key,
                                array=arr,
                                typ=typ,
                                arrtyp="1D",
                            )

                    #
                    # Integral lenght scales
                    #
                    self.averagedData["integral"]["v_t_+sgs"] = (
                        self.averagedData["Fv"]["integral"]["k_fFv"]
                    ) ** 0.5
                    self.averagedData["integral"]["t_t_+sgs"] = (
                        self.averagedData["Fv"]["integral"]["k_fFv"]
                        / self.averagedData["Fv"]["dissipationRate"]["eps"]
                    )
                    self.averagedData["integral"]["l_t_+sgs"] = (
                        self.averagedData["integral"]["v_t_+sgs"]
                        * self.averagedData["integral"]["t_t_+sgs"]
                    )
                    self.averagedData["integral"]["Re_t_+sgs"] = (
                        self.averagedData["integral"]["v_t_+sgs"]
                        * self.averagedData["integral"]["l_t_+sgs"]
                    ) / self.averagedData["Fv"]["processed"]["nu"]

                #
                # Relate cutoff scale to Integral scales
                #
                if "cutoff" in self.averagedData.keys():
                    self.averagedData["cutoff"]["rati_l_filter_l_t"] = (
                        self.averagedData["cutoff"]["l_filter"]
                        / self.averagedData["integral"]["l_t"]
                    )
                    self.averagedData["cutoff"]["rati_l_cutoff_l_t"] = (
                        self.averagedData["cutoff"]["l_cutoff"]
                        / self.averagedData["integral"]["l_t"]
                    )

        return Tensors, Scalars

    def calculateSpectra(self, lc, Tensors=None, Scalars=None):
        """
        Calculate spectra on witness point.
        """
        if Tensors is None:
            Tensors = {}
        if Scalars is None:
            Scalars = {}

        if "Tasks" in lc.locdata.keys():
            if "spectrum" in lc.locdata["Tasks"]:
                #
                # Add dictionary
                #
                if not "spectrum" in self.averagedData.keys():
                    self.averagedData["spectrum"] = {}

                #
                # Auxiliary variables for temporal averaging
                #
                ntime = len(self.rawModuleData["general"]["time"])

                #
                # Auxiliary variables for resampling
                #
                dt_min = self.limitData["min"]["general"]["dt"]
                t_min = self.limitData["min"]["general"]["time"]
                t_max = self.limitData["max"]["general"]["time"]
                nequi = int(np.ceil((t_max - t_min) / dt_min))

                equidist_dt = (t_max - t_min) / nequi
                equidist_time = np.linspace(t_min, t_max, nequi + 1)

                #
                # Save to structure
                #
                self.spectrumData["nequi"] = nequi
                self.spectrumData["equidist_dt"] = equidist_dt
                self.spectrumData["sampling_freq"] = 1.0 / equidist_dt

                freqs = np.fft.fftfreq(equidist_time.size, equidist_dt)

                self.spectrumData["frequencies"] = freqs

                #
                # Frequencies for Lomb-Scargle method
                #
                if "spectrumDoLombscargle" in lc.locdata.keys():
                    spectrumDoLombscargle = lc.locdata["spectrumDoLombscargle"]
                else:
                    spectrumDoLombscargle = False

                if spectrumDoLombscargle:
                    # self.spectrumData["lombscargle_angular_frequencies"] = np.logspace( np.log10(1/(t_max-t_min)), np.log10(1/dt_min), nequi )
                    self.spectrumData["lombscargle_angular_frequencies"] = np.linspace(
                        1 / (t_max - t_min), np.pi / dt_min, int(nequi // 2)
                    )
                    self.spectrumData["lombscargle_frequencies"] = self.spectrumData[
                        "lombscargle_angular_frequencies"
                    ] / (2.0 * np.pi)

                #
                # Process requested spectra
                #
                for mod in lc.locdata["spectrumProperties"]["raw"].keys():
                    if not mod in self.spectrumData.keys():
                        self.spectrumData[mod] = {}

                    for k in lc.locdata["spectrumProperties"]["raw"][mod]:
                        Scalars["{}_{}".format(mod, k)] = self.rawModuleData[mod][k]
                        Scalars["{}_{}_square".format(mod, k)] = (
                            self.rawModuleData[mod][k] ** 2
                        )
                        #
                        # Mean and mean energy
                        #
                        for typ in ["Re", "Fv"]:
                            for arr, key in zip(
                                [
                                    Scalars["{}_{}".format(mod, k)],
                                    Scalars["{}_{}_square".format(mod, k)],
                                ],
                                ["{}_{}".format(mod, k), "{}_{}_square".format(mod, k)],
                            ):
                                self.addAveragedValues(
                                    parentKey="spectrum",
                                    key=key,
                                    array=arr,
                                    typ=typ,
                                    arrtyp="1D",
                                )

                        #
                        # RMS
                        #
                        for typ in ["Re", "Fv"]:
                            self.averagedData["spectrum"][
                                "{}_{}_f{}_RMS".format(mod, k, typ)
                            ] = np.sqrt(
                                self.averagedData[typ]["spectrum"][
                                    "{}_{}_square".format(mod, k)
                                ]
                                - self.averagedData[typ]["spectrum"][
                                    "{}_{}".format(mod, k)
                                ]
                                ** 2
                            )

                        #
                        # Interpolate on equidistant time
                        #
                        equidist_data = np.interp(
                            equidist_time,
                            self.rawModuleData["general"]["time"],
                            Scalars["{}_{}".format(mod, k)],
                        )

                        #
                        # Take spectrum
                        #
                        thisfft = np.fft.fft(equidist_data)
                        self.spectrumData[mod][k] = (
                            np.abs(thisfft) ** 2
                            / len(equidist_time)
                            / self.spectrumData["sampling_freq"]
                        )

                        #
                        # Use Welch method
                        #
                        welch_frequencies, Pxx_spec = signal.welch(
                            equidist_data,
                            self.spectrumData["sampling_freq"],
                            window="flattop",
                            nperseg=nequi // 5,
                            scaling="density",
                            return_onesided=False,
                        )
                        self.spectrumData["welch_frequencies"] = welch_frequencies
                        self.spectrumData[mod]["welch_{}".format(k)] = Pxx_spec

                        if spectrumDoLombscargle:
                            #
                            # Use Lomb-Scargle method
                            #
                            self.spectrumData[mod][
                                "lombscargle_{}".format(k)
                            ] = signal.lombscargle(
                                self.rawModuleData["general"]["time"],
                                Scalars["{}_{}".format(mod, k)],
                                freqs=self.spectrumData[
                                    "lombscargle_angular_frequencies"
                                ],
                            )

        return Tensors, Scalars

    def saveAveragedData(self, lc):
        """
        Save scalar values from self.averagedData into json file.
        """
        #
        # Assemble data to save
        #
        data = {}

        #
        # Add coordinate
        #
        data["coords"] = [
            float(self.coords[0]),
            float(self.coords[1]),
            float(self.coords[2]),
        ]
        data["ntime"] = len(self.rawModuleData["general"]["time"])
        data["time_diff"] = (
            self.rawModuleData["general"]["time"][-1]
            - self.rawModuleData["general"]["time"][0]
        )

        #
        # Serialize data in self.averagedData
        #
        data = serializeDictionary(data, self.averagedData, elementLimit=10)

        #
        # Write single point data
        #
        fn = "witness_data_entity{}.json".format(self.index + 1)

        with open(os.path.join(lc.locdata["CasePath"], fn), "w") as f:
            json.dump(data, f, indent=2)

    def saveLimitData(self, lc):
        """
        Save values from self.limitData into json file.
        """
        #
        # Assemble data to save
        #
        data = {}

        #
        # Add coordinate
        #
        data["coords"] = [
            float(self.coords[0]),
            float(self.coords[1]),
            float(self.coords[2]),
        ]
        data["ntime"] = len(self.rawModuleData["general"]["time"])
        data["time_diff"] = (
            self.rawModuleData["general"]["time"][-1]
            - self.rawModuleData["general"]["time"][0]
        )

        #
        # Serialize data in self.limitData
        #
        data = serializeDictionary(data, self.limitData, elementLimit=10)

        #
        # Write single point data
        #
        fn = "witness_limiting_data_entity{}.json".format(self.index + 1)

        with open(os.path.join(lc.locdata["CasePath"], fn), "w") as f:
            json.dump(data, f, indent=2)

    def saveTemporalData(self, lc):
        """
        Save values from self.processedData into json file.
        """
        if "SaveTemporalData" in lc.locdata.keys():
            if lc.locdata["SaveTemporalData"]:
                #
                # Assemble data to save
                #
                data = {}

                #
                # Add coordinate
                #
                data["time"] = list(self.rawModuleData["general"]["time"])
                ntime = len(data["time"])

                #
                # Dictionaries to process
                #
                dicts = [self.processedData]
                dictnames = ["processed"]

                #
                # Raw module data
                #
                if "Modules" in lc.locdata.keys():
                    for m in lc.locdata["Modules"]:
                        dicts.append(self.rawModuleData[m])
                        dictnames.append(m)

                for dic, dn in zip(dicts, dictnames):
                    #
                    # Initialize structure
                    #
                    data[dn] = {}

                    #
                    # Serialize data in dictionaries
                    #
                    data[dn] = serializeDictionary(data[dn], dic, elementLimit=np.inf)

                #
                # Write single point data
                #
                fn = "witness_temporal_data_entity{}.json".format(self.index + 1)

                with open(os.path.join(lc.locdata["CasePath"], fn), "w") as f:
                    json.dump(data, f, indent=2)

    def saveSpectrumData(self, lc):
        """
        Save values from self.spectrumData into json file.
        """
        if "Tasks" in lc.locdata.keys():
            if "spectrum" in lc.locdata["Tasks"]:
                #
                # Serialize data in self.spectrumData
                #
                data = {}
                data["coords"] = [
                    float(self.coords[0]),
                    float(self.coords[1]),
                    float(self.coords[2]),
                ]
                data = serializeDictionary(data, self.spectrumData, elementLimit=np.inf)

                #
                # Write single point data
                #
                fn = "witness_spectrum_data_entity{}.json".format(self.index + 1)

                with open(os.path.join(lc.locdata["CasePath"], fn), "w") as f:
                    json.dump(data, f, indent=2)

    def calculateLimitingValues(self, lc):
        """
        Calculate limiting values of various time series.
        """
        if "Modules" in lc.locdata.keys():
            #
            # Get min/max of raw data
            #
            for m in lc.locdata["Modules"] + ["general"]:
                if not m in self.rawModuleData.keys():
                    continue

                for k in self.rawModuleData[m].keys():
                    if self.rawModuleData[m][k].dtype == float:
                        self.addLimitingValues(
                            parentKey=m, key=k, array=self.rawModuleData[m][k]
                        )

        #
        # Get min /max of processed data
        #
        for k in self.processedData.keys():
            if self.processedData[k].dtype == float:
                self.addLimitingValues(
                    parentKey="processed", key=k, array=self.processedData[k]
                )

    def addLimitingValues(self, parentKey, key, array):
        """
        Add a limiting value from a given time series to self.limitData.
        """
        #
        # Initialize structure
        #
        if not "min" in self.limitData.keys():
            self.limitData["min"] = {}
        if not "max" in self.limitData.keys():
            self.limitData["max"] = {}

        #
        # Add parent key if necessary
        #
        if not parentKey in self.limitData["min"].keys():
            self.limitData["min"][parentKey] = {}
        if not parentKey in self.limitData["max"].keys():
            self.limitData["max"][parentKey] = {}

        #
        # Add min/max
        #
        self.limitData["min"][parentKey][key] = np.min(array)
        self.limitData["max"][parentKey][key] = np.max(array)

    def addAveragedValues(self, parentKey, key, array, typ="Re", arrtyp="1D"):
        """
        Add the average of a given time series to self.averagedData.
        typ options:
        - Re:     Temporal average (Reynolds)
        - Fv:     Density weighted temporal average (Favre)
        arrtyp options:
        - 1D:     one dimensional array
        - 2D:     two dimensional array with the first diemnsion corresponding to a vector quantity
        """
        #
        # Return is time is misisng
        #
        if not "general" in self.rawModuleData.keys():
            return

        if not (
            "dt" in self.rawModuleData["general"].keys()
            and "time" in self.rawModuleData["general"].keys()
        ):
            return

        #
        # Has Reynolds averaged density
        #
        hasRhoBar = False
        if "Re" in self.averagedData.keys():
            if "processed" in self.averagedData["Re"].keys():
                if "rho" in self.averagedData["Re"]["processed"].keys():
                    hasRhoBar = True

        #
        # Return if density is missing
        #
        if typ == "Fv" and (not hasRhoBar) and "rho" in self.processedData.keys():
            return

        ########################
        # Initialize structure #
        ########################
        if not typ in self.averagedData.keys():
            self.averagedData[typ] = {}

        #
        # Add parent key if necessary
        #
        if not parentKey in self.averagedData[typ].keys():
            self.averagedData[typ][parentKey] = {}

        ###########
        # Average #
        ###########
        #
        # Auxiliary variables for temporal averaging
        #
        time_diff = (
            self.rawModuleData["general"]["time"][-1]
            - self.rawModuleData["general"]["time"][0]
        )

        #
        # Shape
        #
        if arrtyp == "2D":
            nelem = array.shape[1]

        #
        # Time step
        #
        if arrtyp == "1D":
            dt = self.rawModuleData["general"]["dt"]
        elif arrtyp == "2D":
            dt = np.multiply.outer(
                self.rawModuleData["general"]["dt"], np.ones([nelem])
            )

        #
        # Weight for weighted averages
        #
        if (typ == "Fv") and ("rho" in self.processedData.keys()):
            #
            # Density weighted
            #
            if arrtyp == "1D":
                weightL = self.processedData["rho"][:-1]
                weightR = self.processedData["rho"][1:]
            elif arrtyp == "2D":
                weightL = np.multiply.outer(
                    self.processedData["rho"][:-1], np.ones([nelem])
                )
                weightR = np.multiply.outer(
                    self.processedData["rho"][1:], np.ones([nelem])
                )

            weightDenom = self.averagedData["Re"]["processed"]["rho"]
        else:
            weightL = 1.0
            weightR = 1.0
            weightDenom = 1.0

        #
        # Add average
        #
        if arrtyp == "1D":
            self.averagedData[typ][parentKey][key] = np.sum(array[:-1] * weightL * dt)
            self.averagedData[typ][parentKey][key] += np.sum(array[1:] * weightR * dt)
            self.averagedData[typ][parentKey][key] /= 2.0 * time_diff * weightDenom

        elif arrtyp == "2D":
            self.averagedData[typ][parentKey][key] = np.sum(
                array[:-1, 0:nelem] * weightL * dt, axis=0
            )
            self.averagedData[typ][parentKey][key] += np.sum(
                array[1:, 0:nelem] * weightR * dt, axis=0
            )
            self.averagedData[typ][parentKey][key] /= 2.0 * time_diff * weightDenom
