#################################################################################
#   KolmogorovAtWitness                                                         #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################

import numpy as np


def getExpandedDictionary(dic, templateDic, nelem):
    """
    Return a dictionary identical to the template, but replacing floats with arrays, and arrays with matrices.
    """

    for k in templateDic.keys():
        entry = templateDic[k]

        #
        # Add all strings
        #
        if isinstance(entry, str):
            dic[k] = np.empty([nelem], dtype=str)

        #
        # Add all floats
        #
        elif isinstance(entry, float):
            dic[k] = np.empty([nelem], dtype=float) * np.nan

        #
        # Add lists
        #
        elif isinstance(entry, list):
            if len(entry) > 0:
                dtype = type(entry[0])
            else:
                dtype = float
            dic[k] = np.empty([nelem, len(entry)], dtype=dtype)

            if dtype == float:
                dic[k] *= np.nan
            elif dtype == int:
                dic[k] *= 0

        #
        # Add numpy arrays
        #
        elif isinstance(entry, np.ndarray):
            if len(entry.shape) == 1:
                #
                # 1D -> 2D array
                #
                dic[k] = np.empty([nelem, len(entry)], dtype=entry.dtype)

            elif len(entry.shape) == 2:
                #
                # 2D -> 3D array
                #
                dic[k] = np.empty(
                    [nelem, entry.shape[0], entry.shape[1]], dtype=entry.dtype
                )

            elif len(entry.shape) == 3:
                #
                # 3D -> 4D array
                #
                dic[k] = np.empty(
                    [nelem, entry.shape[0], entry.shape[1], entry.shape[2]],
                    dtype=entry.dtype,
                )

            #
            # Initialize
            #
            if entry.dtype == float:
                dic[k] *= np.nan
            elif entry.dtype == int:
                dic[k] *= 0

        #
        # Call recursively this function, if it's a dict
        #
        elif isinstance(entry, dict):
            dic[k] = {}
            dic[k] = getExpandedDictionary(dic[k], entry, nelem)

    return dic


def insertToExpandedDictionary(dic, insertDic, ielem):
    """
    Return a dictionary identical to the template, but replacing floats with arrays, and arrays with matrices.
    """

    for k in insertDic.keys():
        entry = insertDic[k]

        #
        # Add all strings
        #
        if isinstance(entry, str):
            dic[k][ielem] = entry

        #
        # Add all floats
        #
        elif isinstance(entry, float):
            dic[k][ielem] = entry

        #
        # Add lists
        #
        elif isinstance(entry, list):
            dic[k][ielem, :] = entry

        #
        # Add numpy arrays
        #
        elif isinstance(entry, np.ndarray):
            if len(entry.shape) == 1:
                #
                # 1D -> 2D array
                #
                dic[k][ielem, :] = entry

            elif len(entry.shape) == 2:
                #
                # 2D -> 3D array
                #
                dic[k][ielem, :, :] = entry

            elif len(entry.shape) == 3:
                #
                # 3D -> 4D array
                #
                dic[k][ielem, :, :, :] = entry

        #
        # Call recursively this function, if it's a dict
        #
        elif isinstance(entry, dict):
            dic[k] = insertToExpandedDictionary(dic[k], entry, ielem)

    return dic
