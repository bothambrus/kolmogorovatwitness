#################################################################################
#   KolmogorovAtWitness                                                         #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################


def getMpiData(noMPI):
    """
    Get MPI size and my rank.
    """
    #
    # MPI for python is not a compulsory requirement
    #
    if noMPI:
        rank = 0
        size = 1
    else:
        try:
            from mpi4py import MPI

            comm = MPI.COMM_WORLD
            rank = comm.Get_rank()
            size = comm.Get_size()
        except:
            rank = 0
            size = 1
    return size, rank


def Barrier(noMPI):
    """
    Impose MPI barrier.
    """
    if not noMPI:
        try:
            from mpi4py import MPI

            MPI.COMM_WORLD.Barrier()
        except:
            pass
