#################################################################################
#   KolmogorovAtWitness                                                         #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################

import numpy as np


def serializeDictionary(data, nonSerialData, elementLimit=10):
    """
    Return a dictionary identical to the non-serial one, replacing numpy arrays with lists.
    """

    for k in nonSerialData.keys():
        entry = nonSerialData[k]

        #
        # Add all floats
        #
        if isinstance(entry, float):
            data[k] = float(entry)

        #
        # Add lists containing floats
        #
        if isinstance(entry, list):
            data[k] = []
            for e in entry:
                if isinstance(e, float):
                    data[k].append(float(e))

        #
        # Add numpy arrays smaller than the limiting size
        #
        if isinstance(entry, np.ndarray):
            if np.prod(entry.shape) < elementLimit and entry.dtype == float:
                if len(entry.shape) == 1:
                    #
                    # 1D array
                    #
                    data[k] = list(entry)

                elif len(entry.shape) == 2:
                    #
                    # 2D array
                    #
                    data[k] = []
                    for ii in range(entry.shape[0]):
                        data[k].append(list(entry[ii, :]))

                elif len(entry.shape) == 3:
                    #
                    # 3D array
                    #
                    data[k] = []
                    for ii in range(entry.shape[0]):
                        data[k].append([])
                        for jj in range(entry.shape[1]):
                            data[k][ii].append(list(entry[ii, jj, :]))

        #
        # Call recursively this function, if it's a dict
        #
        if isinstance(entry, dict):
            data[k] = {}
            data[k] = serializeDictionary(data[k], entry, elementLimit=elementLimit)

    return data
