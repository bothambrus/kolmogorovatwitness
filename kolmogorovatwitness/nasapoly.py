#################################################################################
#   KolmogorovAtWitness                                                         #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import numpy as np


def T2hcp(cpcoeffsLT_in, cpcoeffsHT_in, T_in):
    """
    Evaluate enthalpy and specific heat at given temperature.
    """
    if isinstance(T_in, float):
        orishape = [1]
    else:
        orishape = T_in.shape
    flatshape = np.prod(orishape)

    cpcoeffsLT = cpcoeffsLT_in.reshape((flatshape, 6))
    cpcoeffsHT = cpcoeffsHT_in.reshape((flatshape, 6))
    if isinstance(T_in, float):
        T = np.array([T_in])
    else:
        T = T_in.flatten()

    #
    # Clip temperarure
    #
    T = np.where(T > 3000.0, 3000.0, np.where(T < 200.0, 200.0, T))

    #
    # Use low or high temperature poly
    #
    cpc = np.where(np.multiply.outer(T, np.ones([6])) < 1000.0, cpcoeffsLT, cpcoeffsHT)

    #
    # Get ordered polynomial coefficients for Cp and H where the H oens are multiplied with the correct factors
    #
    coeffs_cp = np.transpose(
        np.take_along_axis(
            cpc,
            np.multiply.outer(
                np.ones_like(T, dtype=int), np.array([0, 1, 2, 3, 4], dtype=int)
            ),
            axis=-1,
        )
    )
    coeffs_h = np.take_along_axis(
        cpc,
        np.multiply.outer(
            np.ones_like(T, dtype=int), np.array([5, 0, 1, 2, 3, 4], dtype=int)
        ),
        axis=-1,
    )
    coeffs_h = np.transpose(
        coeffs_h
        * np.multiply.outer(
            np.ones_like(T),
            np.array([1.0, 1.0, 1.0 / 2.0, 1.0 / 3.0, 1.0 / 4.0, 1.0 / 5.0]),
        )
    )

    #
    # Evaluate polynomials
    #
    cp = np.polynomial.polynomial.polyval(T, coeffs_cp, tensor=False)
    h = np.polynomial.polynomial.polyval(T, coeffs_h, tensor=False)

    cp = cp.reshape(orishape)
    h = h.reshape(orishape)

    return h, cp


def h2Tcp(cpcoeffsLT, cpcoeffsHT, h, Tini=273.15, tol=1e-7):
    """
    Evaluate temperature and specific heat at given enthalpy,
    using the Newton-Rhapson's method
    """
    #
    # Initialize
    #
    T = Tini * np.ones_like(h)
    err = np.inf * np.ones_like(h)
    maxit = 100
    it = 0

    #
    # Loop
    #
    while np.any(abs(err / h) > tol):
        h_guess, cp_guess = T2hcp(cpcoeffsLT, cpcoeffsHT, T)
        err = h_guess - h
        T -= err / cp_guess
        T = np.where(T > 5000.0, 5000.0, np.where(T < 200.0, 200.0, T))
        it += 1
        if it >= maxit:
            break

    h_fin, cp_fin = T2hcp(cpcoeffsLT, cpcoeffsHT, T)
    return T, cp_fin
